<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//static html pages
//Route::get('/contact', 'HomeController@contact');
//Route::get('/addcontacts', 'HomeController@settings');
//Route::get('/log', 'HomeController@log');
//Route::get('/addreminder', 'HomeController@addreminder')->name('addreminder');
//Route::get('/settings', 'HomeController@settings');




Route::get('/user/login', 'Auth\RegisterController@userlogin');

//add data routes
Route::get('/insertdata', 'HomeController@adddata');
Route::get('/syncLatestRemindersToServer', 'SendRequestToServer@syncLatestRemindersToServer');
Route::get('/getLatestReminderStatusFromCloud', 'SendRequestToServer@getLatestReminderStatusFromCloud');
Route::get('/getLatestAllReminderStatusFromCloud', 'SendRequestToServer@getLatestAllReminderStatusFromCloud');

//get latest twlio logs from aws
Route::get('/getLatestReminderTwilioUpdateFromCloud', 'SendRequestToServer@getLatestReminderTwilioUpdateFromCloud');
Route::get('/syncEmailSettings', 'SendRequestToServer@syncEmailSettings');

// bulk reminder upload
Route::get('syncBulkReminder', ['as' => 'bulk.excel', 'uses' => 'BulkReminderController@addBulkReminder']);
Route::get('/calaudit-to-sns', 'ReminderController@calauditsns');
Route::get('/remindernotify', 'SendRequestToServer@remindernotify');
Route::get('/sync-staff', 'ReminderController@syncStaffDetailsToUsers');
Route::get('/updateTwilioStatusFromTheServer/{reminderId}/{twilioStatus}/{confirmdate}', 'ReminderController@updateTwilioStatusFromTheServer');
Route::get('/fetch-default-templates', 'ReminderController@fetchDefaultTemplates');

Route::get('contacts/migratecontact', 'ContactController@syncContactFromWindowsToWeb');
Route::get('/lrsreminder-to-snsreminder', 'ReminderController@syncReminderFromWindowsToWeb');
Route::get('/lrsremindertiming-to-snsremindertiming', 'ReminderController@syncReminderTimingFromWindowsToWeb');

Auth::routes();

Route::group(['middleware' => ['auth:web']], function() {

    Route::resource('users', 'UserController', ['only' => ['index', 'create', 'store', 'edit']]);
    Route::patch('users/update/{id}', ['uses' => 'UserController@update', 'as' => 'users.update']);
    Route::delete('users/delete/{id}', ['uses' => 'UserController@destroy', 'as' => 'users.destroy']);
    //contacts routes
    Route::get('contacts/getlist', ['uses' => 'ContactController@getlist', 'as' => 'datatables.data']);
    Route::resource('contacts', 'ContactController');
    Route::get('contacts/searchContact/{queryString}/{extraType?}', 'ContactController@searchForContact');
    Route::get('searchcaseno',array('as'=>'searchcaseno','uses'=>'ContactController@ajaxSearchForCaseNo'));
    Route::post('quickContactSave',array('as'=>'quickcontactsave','uses'=>'ContactController@quickContactStore'));


    //templates routes
    Route::get('mastertemplates/getlist', ['uses' => 'MasterTemplatesController@getlist', 'as' => 'templates.data']);
    Route::resource('mastertemplates', 'MasterTemplatesController');


    //reminder routes
    Route::post('/reminderAdd', ['uses' => 'ReminderController@store', 'as' => 'addReminder']);
    Route::post('/isdone', ['uses' => 'ReminderController@isdone', 'as' => 'isdone']);
    Route::post('/removecontacts', ['uses' => 'ContactController@removecontacts', 'as' => 'removecontacts']);
    Route::get('{reminderId}/reminderEdit', ['uses' => 'ReminderController@edit', 'as' => 'editReminder']);
    Route::get('{reminderId}/reminderEdit/view', ['uses' => 'ReminderController@edit', 'as' => 'editReminder']);
    Route::post('/updateReminder', ['uses' => 'ReminderController@update', 'as' => 'updateReminder']);
    Route::get('/getReminderDashboard', 'ReminderController@getRemindersList');
    Route::get('/deleteReminder', 'ReminderController@deleteReminders');
    Route::get('/addReminder/{strUserSearch?}', ['uses' => 'ReminderController@create', 'as' => 'addReminderPage']);
    Route::resource('/', 'ReminderController');



    //logs rotes
    Route::get('logs', 'TwiloLogController@index');
    Route::get('log/getlist', ['uses' => 'TwiloLogController@getlist', 'as' => 'log.data']);
    Route::get('log/addtwiliodate/{reminderId}', ['uses' => 'TwiloLogController@addtwiliodate', 'as' => 'log.addtwiliodata']);



//settings routes
    Route::get('/settings', 'SettingController@index');
    Route::post('/settings/updateLogColors', 'SettingController@updateLogColors');
    Route::post('/settings/updateTimeZone', 'SettingController@updateTimeZone');
    Route::post('/settings/updateRecallSettings', 'SettingController@updateRecallSettings');
    Route::post('/settings/updateCompanyLogoAndEmailSignature', 'SettingController@updateCompanyLogoAndEmailSignature')->name('updateCompanyLogoAndSignature');
    Route::patch('/settings/updateEmailSettings', ['uses' => 'SettingController@updateEmailSettings', 'as' => 'setting.updateemail']);
    Route::put('/settings/updateCsvSettings', 'SettingController@updateCsvSettings');
    Route::post('/settings/updateDefaultLanguageSettings', 'SettingController@updateDefaultLanguageSettings');
    Route::post('/settings/updateparalegalhandling', 'SettingController@updateparalegalhandling');
    Route::post('/settings/updatesalutation', 'SettingController@updatesalutation');
    // added by sourabh for salutation
    //Route::post('/settings/updatesyna1law', 'SettingController@updatesyna1law');
    Route::post('/settings/updatesyncsnstoa1law', 'SettingController@updatesyncsnstoa1law');    // to sync flag for updating sns reminder activity to a1law if enable.
//import csv contacts
    Route::post('import-csv', ['as' => 'import-csv-excel', 'uses' => 'ContactController@importFileIntoDB']);
//export csv contacts
    Route::get('downloadsamplefile', ['as' => 'excel.samplefile', 'uses' => 'ContactController@downloadSampleExcelFile']);
    Route::get('downloadexcelfile', ['as' => 'excel.file', 'uses' => 'ContactController@downloadExcelFile']);
//add promotional message
    Route::post('/settings/addPromotionalMessage', ['as' => 'add.promotional', 'uses' => 'SettingController@addPromotionalMessage']);
//export route
    Route::post('/settings/exportReport', ['as' => 'export.report', 'uses' => 'SettingController@exportReport']);
    Route::get('/settings/exportReport', ['uses' => 'SettingController@exportReport']);

    //users routes
    Route::get('users/getlist', ['uses' => 'UserController@getlist', 'as' => 'datatables.ur.data']);
//    Route::get('users/searchContact/{queryString}', 'UserController@searchForContact');
    
    
    // added on 23rd April 2018
    //logs rotes
    Route::get('failurelog', 'FailureController@index');
    Route::get('failurelog/getlist', ['uses' => 'FailureController@getlist', 'as' => 'failurelog.data']);
    Route::get('failurelog/addtwiliodate/{reminderId}', ['uses' => 'FailureController@addtwiliodate', 'as' => 'failurelog.addtwiliodata']);
    
    // for addressbook created by nilay
    Route::get('/addressbook', 'AddressBookController@index');
    Route::get('/addContact/{addid}/{id}', 'AddressBookController@addContact');
    Route::get('/addressbookContact/{id}', 'AddressBookController@addressbookContact');
    Route::get('/deleteContact/{addid}/{cardid}', 'AddressBookController@deleteContact');
    Route::post('/addNewBook', 'AddressBookController@addNewAddressBook');
    Route::get('/allAddressBook', 'AddressBookController@allAddressBook');
    Route::get('/deleteAddressBook/{id}', 'AddressBookController@deleteAddressBook');
    Route::get('/addressBookContactInReminder/{id}', 'AddressBookController@addressBookContactInReminder');

});
