@extends('layouts.app')
@section('title', 'SNS|Settings')

@section('content')


<div class="col-lg-12">
    <div class="">
        <div class="row setting-page-inner-tab">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#appointment_page" aria-expanded="false">Appointment page</a></li>
                    <li class=""><a data-toggle="tab" href="#email_call_sms_setting" aria-expanded="false">Email/Call/SMS Setting</a></li>
                    <li class=""><a data-toggle="tab" href="#other_setting" aria-expanded="false">Other Setting</a></li>
                    <li class=""><a data-toggle="tab" href="#reports" aria-expanded="true">Reports</a></li>
                </ul>
                <div class="tab-content">
                    <div id="appointment_page" class="tab-pane active">

                        @include("mastertemplates/index")

                    </div>
                    <div id="email_call_sms_setting" class="tab-pane">
                        @include("settings/emailsettings",['arrSettings'=>$arrSettings])

                    </div>
                    <div id="other_setting" class="tab-pane">
                        <div class="panel-body">  
                            <div class="row">    
                                <div class="col-sm-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Color Setting</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="close-link">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <strong>Appointment Status Color</strong>

                                                    @if(COUNT($appointmentColor) > 0)
                                                    <ul class="reminders-private m-t-10">
                                                        @foreach($appointmentColor AS $key => $appointmentColorInfo)
                                                        <li>
                                                            <span class="parentColorStatusChange" style="background-color: {{$appointmentColorInfo->color}};" data-colorId="{{$appointmentColorInfo->recid}}"></span>
                                                            {{$appointmentColorInfo->statusvalue}}
                                                            <a href="javaScript:;">
                                                                <i class="fa fa-pencil colorStatusChange" aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif

                                                </div>
                                                <div class="col-sm-6">
                                                    <strong>Log Status Color</strong>

                                                    @if(COUNT($twilioLogColor) > 0)
                                                    <ul class="reminders-private m-t-10">
                                                        @foreach($twilioLogColor AS $key => $twilioLogColorInfo)
                                                        <li>
                                                            <span class="parentColorStatusChange" style="background-color: {{$twilioLogColorInfo->color}};" data-colorId="{{$twilioLogColorInfo->recid}}"></span>
                                                            {{$twilioLogColorInfo->statusvalue}}
                                                            <a href="javaScript:;">
                                                                <i class="fa fa-pencil colorStatusChange" aria-hidden="true"></i>
                                                            </a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox float-e-margins upload-company-logo">
                                        <div class="ibox-title">
                                            <h5>Upload</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="close-link">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                {{-- {{ Form::open(array('url' => 'foo/bar')) }} --}}
                                                <form name="frmUploadCompanyLogoAndPic" id="frmUploadCompanyLogoAndPic" action="{{Route('updateCompanyLogoAndSignature')}}" mentod="POST" enctype="multipart/form-data">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-12">
                                                            <div class="upload-signature">
                                                                <strong>Company Name</strong>
                                                                <div class="form-group">
                                                                    <input class="form-control" name="companyname" value="{{ $arrAdminSettings->companyname}}" id="companyname" type="text" />
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        
                                                        <div class="col-sm-6">
                                                            <div class="clearfix">
                                                                <strong>Upload Company logo</strong>
                                                            </div>                                                                        
                                                            <div class="form-group">
                                                                <div class="input-group filetypeplainbox">
                                                                    <input type="file" id="flCompanyLogo" class="form-control" name="flCompanyLogo">
                                                                </div>
                                                                <img class="img-preview img-preview-sm" id="companyLogoPreview" src="{{$arrAdminSettings->companylogoFullPath}}">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-6">
                                                            <div class="upload-signature">
                                                                <strong>Upload Signature</strong>
                                                                <div class="form-group">
                                                                    <textarea class="form-control emailsignature" name="taEmailSignature" id="taEmailSignature">{{ $arrAdminSettings->emailsignature}}</textarea>
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary pull-right btn-xs">Update</button>
                                                        </div>
                                                    </div>
                                                </form>

                                                <div id="emailsignuploadresponase">
                                                </div>
                                                {{-- {{ Form::close() }} --}}
                                            </div>
                                        </div>
                                    </div> 
                                </div>          
                                <div class="col-sm-6">
                                    <div class="ibox float-e-margins hidden"> 
                                        <div class="ibox-title">
                                            <h5>Change Password</h5>  
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="close-link">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Old Password</label>
                                                        <input type="Password" class="form-control">
                                                    </div>  
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>New Password</label>
                                                        <input type="Password" class="form-control">
                                                    </div>  
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Confirm Password</label>
                                                        <input type="Password" class="form-control">
                                                    </div> 
                                                </div>
                                                <div class="col-sm-12">
                                                    <button type="button" class="btn btn-primary pull-right btn-xs">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                        
                                    <div class="ibox float-e-margins m-b-0 other-settings-box"> 
                                        <div class="ibox-title">
                                            <h5>Other Settings</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="close-link">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row m-0">
                                                <div class="clearfix m-b-10">
                                                    <strong>Default Language</strong>
                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <?php
                                                            $langId = array_keys($twilioDefaultLanguage);
                                                            $lstLanguage = isset($twilioDefaultLanguage) ? ((isset($langId) && !empty($langId)) ? $langId[0] : "") : "";
                                                            ?>
                                                            {{ Form::label('lstLanguage', 'Language :') }}
                                                            {{ Form::select('lstLanguage',[null=>'Select Language'] + $twilioLanguage, $lstLanguage, ['class'=>'form-control'])}}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-xs-6">
                                                            <button type="button" class="btn btn-primary btn-xs" id="btnUpdateDefaultLanguageSettings">Update</button>
                                                        </div>

                                                </div>
                                                <hr>
                                                <div class="clearfix m-b-10">
                                                    <strong>Time Zone</strong>
                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <select class="form-control" name="lstTimeZone" id="lstTimeZone">
                                                                @foreach ($arrTimeZoneList AS $key => $value)
                                                                <option value="{{$key}}" 

                                                                        @if ($arrAdminSettings->systemtimezone == $key)
                                                                        selected="selected"
                                                                        @endif

                                                                        >{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-6">
                                                            <button type="button" class="btn btn-primary btn-xs" id="btnUpdateTimeZone">Change Time Zone</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <!-- <div class="clearfix m-b-10">
                                                    <strong>A1-LAW Sync ON/Off</strong>
                                                    <div class="i-checks">
                                                        <label> <input type="checkbox" value="{{$arrAdminSettings->synca1law}}" id="synca1law"
                                                        @if($arrAdminSettings->synca1law == "1")
                                                                   checked="checked"
                                                                   @endif
                                                        >Sync A1Law events to Synergy Notification System</label>
                                                        <button type="button" id="btnsynca1law" class="btn btn-primary btn-xs">Update</button>
                                                    </div>
                                                </div>
                                                <hr> -->
                                                <div class="clearfix m-b-10">
                                                    <strong>Sync to A1-LAW ON/Off</strong>
                                                    <div class="i-checks">
                                                        <label> <input type="checkbox" value="{{$arrAdminSettings->onoffsynca1law}}" id="onoffsynca1law" value="1"
                                                        @if($arrAdminSettings->onoffsynca1law == "1")
                                                                   checked="checked"
                                                                   @endif
                                                        >Sync SNS reminder activity to A1Law</label>
                                                        <button type="button" id="btnonoffsynca1law" class="btn btn-primary btn-xs">Update</button>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="clearfix m-b-10">
                                                    <strong>Recall Settings</strong>
                                                    <div class="i-checks">
                                                        <label for="chkRecallEnable">
                                                            <input type="checkbox" name="chkRecallEnable" id="chkRecallEnable" value="1"


                                                                   @if($arrAdminSettings->flagrecall == "1")
                                                                   checked="checked"
                                                                   @endif

                                                                   >Recall enable/disable
                                                        </label>
                                                        <button type="button" class="btn btn-primary btn-xs" id="btnUpdateRecallSettings">Update</button>
                                                    </div>
                                                    <div class="row 

                                                         @if($arrAdminSettings->flagrecall == '0')
                                                         hidden
                                                         @endif

                                                         " id="divRecallOptions">
                                                        <div class="col-sm-4 col-xs-6">
                                                            <label>How many times to call</label>
                                                            <select class="form-control" name="lstRecallHowManyTimes" id="lstRecallHowManyTimes">

                                                                @foreach ($arrRecallTimes AS $key => $value)
                                                                <option value="{{$key}}" 

                                                                        @if ($arrAdminSettings->recalltimes == $key)
                                                                        selected="selected"
                                                                        @endif

                                                                        >{{$value}}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-6">
                                                            <label>Timming interval for recall</label>
                                                            <select class="form-control" name="lstTimingInterval" id="lstTimingInterval">
                                                                @foreach ($arrRecalIntervals AS $key => $value)
                                                                <option value="{{$key}}" 

                                                                        @if ($arrAdminSettings->recallinterval == $key)
                                                                        selected="selected"
                                                                        @endif

                                                                        >{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-6">

                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="clearfix m-b-10">
                                                    <strong>*.CSV Auto Upload Settings</strong>
                                                    <div class="i-checks">
                                                        <label for="chkCdvEnable">
                                                            <input type="checkbox" name="chkCsvEnable" id="chkCsvEnable" value="1"   @if($arrAdminSettings->flagcsv == "1")
                                                                   checked="checked"
                                                                   @endif> *.CSV file Autoupload Enable/Disable</label>
                                                        <button type="button" class="btn btn-primary btn-xs" id="btnUpdateCsvSettings">Update</button>
                                                    </div>
                                                </div>

                                                {!! Form::open(['route' => 'add.promotional','method'=>'POST','files'=>'true','id'=>'importpromotional']) !!}
                                                {{ csrf_field() }}
                                                <div class="clearfix">
                                                    <strong>Upload *.CSV File</strong>                                                                        
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="file" id="imgInp"  class="form-control" name="flagcsv">
                                                            <span class="help-block" id="imagecsv"></span>
                                                        </div>

                                                    </div>
                                                    {!! Form::submit('Update',['class'=>'btn btn-primary pull-right btn-xs']) !!}

                                                </div> 
                                                <hr>
                                                <div class="clearfix">
                                                    <strong>ParaLegal Handling </strong>                                                                        
                                                    <div class="i-checks">
                                                        <label for="chkCdvEnable">
                                                            <input type="checkbox" name="chkCsvEnable" id="onoffparalegal" value="{{$arrAdminSettings->onoffparalegal}}"   @if($arrAdminSettings->onoffparalegal == "1")
                                                                   checked="checked"
                                                                   @endif>  Enable/Disable</label>
                                                    <!-- added by sourabh for salutation  -->
                                                    </div>
                                                    <strong>Salutation </strong>                                                                        
                                                        <div class="i-checks">
                                                            <label for="chkCdvEnable">
                                                                <input type="checkbox" name="chkSalutationEnable" id="onoffsalutation" value="{{$arrAdminSettings->onoffsalutation}}"   @if($arrAdminSettings->onoffsalutation == "1")
                                                                    checked="checked"
                                                                    @endif>  Enable/Disable</label>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="settingsid" name="settingsid" value="{{$arrAdminSettings->recid}} ">
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div id="reports" class="tab-pane">
                        @include("settings/report",['arrSettings'=>$arrSettings])  

                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>


@endsection

@section('scripts')


<script type="text/javascript">
    var url = "{{ route('templates.data') }}";
</script>
<script src="{{ asset('js/webjs/mastertemplates.js') }}"></script>

<script src="{{ asset('js/jquery.form.js') }}"></script>
<script src="{{ asset('js/webjs/settings.js') }}"></script>
<script src="{{ asset('js/webjs/emailsettings.js') }}"></script>
@endsection