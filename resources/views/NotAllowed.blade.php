@extends('layouts.app')
@section('title', 'SNS|Settings')

@section('content')
<div style="margin-top: 20%;text-align: center;">
    <h1 style="color:darkred;">
        Permission denied
    </h1>
    <h3> 
        It appears the page you were looking for is forbidden and not accessible. 
        <br>If the problem persists, please contact web Administrator.
    </h3>
</div>
@endsection