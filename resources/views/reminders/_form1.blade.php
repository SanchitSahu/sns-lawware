<?php

$hidReminderId = "";    
if(isset($reminderId) && is_numeric($reminderId) && $reminderId > 0) {
    $hidReminderId = $reminderId;
}
?>
<style>
    .ui-helper-hidden-accessible { display:none !important;}
</style>
<div class="ibox-content add-new-reminder-box">
    <div class="">
        <!-- Impromptu section start here -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-sx-12">
                <!--<div class="form-group">-->
                    <div class="i-checks">
                        <label><?php

                        $isprivate = false;
                        if((isset($arrReminderDetail) && isset($arrReminderDetail['isprivate']) && $arrReminderDetail['isprivate'] == "Y") || (isset($arrReminderDetail) && isset($arrReminderDetail['contacttype']) && $arrReminderDetail['contacttype'] == "LawwareOTS")) {
                            $isprivate = true;
                        }

                        ?>{{ Form::checkbox('chkImpromptu','Y', $isprivate, ['id'  =>  'chkImpromptu', 'class' => 'chkImpromptu']) }}
                            <span>Impromptu</span>
                        </label>
                    </div>
                <!--</div>-->
            </div>
        </div>
        
        <div class="row i-reminder-create" style="display:none;">
            <div class="col-md-4 col-sm-6 col-sx-6">
                {{ Form::label('txtCaseNo', 'Case No. :') }}
                <!--{!! Form::text('txtCaseNo', null, array('placeholder' => 'Search Caseno','class' => 'form-control','id'=>'txtCaseNo')) !!}-->
                {{ Form::text('txtCaseNo', null, array('placeholder' => 'Search Caseno','class' => 'form-control','id'=>'txtCaseNo', 'data_callback' => 'fetchUserData')) }}
                
                <span style="display:none;" class="caseValueSelected">Remove</span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4 col-sm-6 col-sx-6">
                {{ Form::label('txtContactName', 'First Name :') }}
                {{ Form::text('txtContactName', '', array( 'class'=>'form-control myOptionalFields' )) }}
                {{ Form::hidden('txtContactMName', '', ['id' => 'txtContactMName']) }}
            </div>
            <div class="col-md-4 col-sm-6 col-sx-6">
                {{ Form::label('txtContactLName', 'Last Name :') }}
                {{ Form::text('txtContactLName', '', array( 'class'=>'form-control myOptionalFields' )) }}
            </div>
            <div class="col-md-4 col-sm-6 col-sx-6">
                {{ Form::label('txtEmailAddress', 'Email :') }}
                {{ Form::text('txtEmailAddress', '', array( 'class'=>'form-control myOptionalFields' )) }}
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                {{ Form::label('country_id', 'Country',['class'=>"control-label"]) }}  
                {{ Form::select('country_id',[""=>"Please select country code"]+$countrycode,null,['class'=>'form-control myOptionalFields'])}}
            </div>
            <div class="col-md-6 col-sm-6 col-sx-6">
                {{ Form::label('txtMobileNumber', 'Phone Number :') }}
                {{ Form::text('txtMobileNumber', '', array( 'class'=>'form-control myOptionalFields', 'onkeypress' => 'return isNumberic(event);', 'maxlength' => '10', 'minlength' => '10' )) }}
                <span class="col-md-12 col-sm-12 col-xs-12">&nbsp;
                    {{ Form::hidden('phonetype', '0', ['id' => 'phonetype'] ) }}
                </span>
                <button type="button" class="btn btn-success pull-right btn-xs myOptionalFields" id="btnSaveNewContact"> Continue</button>
            </div>
        </div>
        <!-- Impromptu section ends here -->
        <div class="row"><hr></div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="form-group {{ $errors->has('txtReminderTitle') ? ' has-error' : '' }}"><?php

                    $title = "";
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['title'])) {
                        $title = $arrReminderDetail['title'];
                    }

                    ?>{{ Form::label('txtReminderTitle', 'Reminder Title :') }}
                    {{ Form::text('txtReminderTitle', $title, array( 'class'=>'form-control' )) }}
                    @if ($errors->has('txtReminderTitle'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txtReminderTitle') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="form-group {{ $errors->has('txtDate') ? ' has-error' : '' }}"><?php

//                    $reminderDate = date("m/d/Y");
                    $reminderDate = date(config('app.APP_DEFAULT_VIEW_DATE_FORMAT'));
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['reminderDate'])) {
                        $reminderDate = $arrReminderDetail['reminderDate'];
                    }

                    ?>{{ Form::label('txtDate', 'Date :') }}
                    <div id="input-group-datepicker" class="input-group date">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        {{ Form::text('txtDate', $reminderDate, array( 'class'=>'form-control' )) }}
                    </div>
                    @if ($errors->has('txtDate'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txtDate') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="form-group {{ $errors->has('txtTime') ? ' has-error' : '' }}"><?php

                    $reminderTime = date(config('app.APP_DEFAULT_TIME_FORMAT'));
//                    $reminderTime = date("H:i");
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['reminderTime'])) {
                        $reminderTime = date(config('app.APP_DEFAULT_TIME_FORMAT'), strtotime($arrReminderDetail['reminderTime']));
                    }

                    ?>{{ Form::label('txtTime', 'Time :') }}
                    <div class="input-group clockpicker" data-autoclose="true">
                        {{ Form::text('txtTime', $reminderTime, array( 'class'=>'form-control' )) }}
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                    @if ($errors->has('txtTime'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txtTime') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
           
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="form-group"><?php

                    $lstTemplates = "";
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['templateid'])) {
                        $lstTemplates = $arrReminderDetail['templateid'];
                    }

                    ?>{{ Form::label('lstTemplates', 'Event Template :') }}
                    @if(COUNT($arrTemplate) > 0) 
                        <select class="form-control" name="lstTemplates" id="lstTemplates">
                            <option value="" data-templateBody="" data-templateTitle="">--Select Appointment Template--</option>
                            @foreach($arrTemplate AS $key => $arrTemaplteInfo)
                                <option value="{{$arrTemaplteInfo->recid}}" 
                                    data-templateBody="{{ base64_encode($arrTemaplteInfo->bodyemail) }}" 
                                    data-templateTitle="{{ base64_encode($arrTemaplteInfo->purpose) }}" 
                                    data-languageSelected="{{$arrTemaplteInfo->language}}" 
                                    data-isReschedule="{{$arrTemaplteInfo->isreschedule}}"
                                    data-isdisable="{{$arrTemaplteInfo->isdisable}}"
                                    data-iscallback="{{$arrTemaplteInfo->iscallback}}"
                                    data-flagSMS="{{$arrTemaplteInfo->flagsms}}"
                                    data-flagSMSTimingId="{{implode(',', $arrTemaplteInfo->smstimingid)}}"
                                    data-flagCall="{{$arrTemaplteInfo->flagcall}}"
                                    data-flagCallTimingId="{{implode(',', $arrTemaplteInfo->calltimingid)}}"
                                    data-flagEmail="{{$arrTemaplteInfo->flagemail}}"
                                    data-flagEmailTimingId="{{implode(',', $arrTemaplteInfo->emailtimingid)}}"
                                    
                                    @if($lstTemplates == $arrTemaplteInfo->recid)
                                        selected="selected"
                                    @endif

                                >{{$arrTemaplteInfo->title}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="form-group {{ $errors->has('txtAppointmentType') ? ' has-error' : '' }}"><?php

                    $purpose = "";
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['purpose'])) {
                        $purpose = $arrReminderDetail['purpose'];
                    }

                    ?>{{ Form::label('txtAppointmentType', 'Appointment Type :') }}
                    {{ Form::text('txtAppointmentType', $purpose, array( 'class'=>'form-control' )) }}
                    @if ($errors->has('txtAppointmentType'))
                        <span class="help-block">
                            <strong>{{ $errors->first('txtAppointmentType') }}</strong>
                        </span>
                    @endif
                    
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="form-group"><?php
                
                    $key = array_keys($twilioDefaultLanguage);
                    $lstLanguage = (isset($key) && !empty($key)) ? $key[0] : "";
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['languageid'])) {
                        $lstLanguage = $arrReminderDetail['languageid'];
                    }

                    ?>{{ Form::label('lstLanguage', 'Language :') }}
                    {{ Form::select('lstLanguage',$language, $lstLanguage, ['class'=>'form-control'])}}
                </div>
            </div>
             
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group note-editable-group {{ $errors->has('taAppoinmentTemplateBody') ? ' has-error' : '' }}"><?php

                    $taAppoinmentTemplateBody = "";
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['bodyemail'])) {
                        $taAppoinmentTemplateBody = $arrReminderDetail['bodyemail'];
                    }

                    ?>{{ Form::label('taAppoinmentTemplateBody', 'Event Body :') }}
                    <div class="summernote1">
                        {{ Form::textarea('taAppoinmentTemplateBody', $taAppoinmentTemplateBody, array( 'class'=>'form-control' )) }}
                    </div>
                    @if ($errors->has('taAppoinmentTemplateBody'))
                        <span class="help-block">
                            <strong>{{ $errors->first('taAppoinmentTemplateBody') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row add-new-checkbox">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                    {{ Form::label('lstInsertFields', 'Insert Field :') }}
                    {{ Form::select('lstInsertFields',array_merge([""=>'Please select fields'],$fields),null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="m-b-20"></div>
                <div class="i-checks">
                    <label><?php

                    $isprivate = false;
                    if(isset($arrReminderDetail) && isset($arrReminderDetail['isprivate']) && $arrReminderDetail['isprivate'] == "Y") {
                        $isprivate = true;
                    }

                    ?>{{ Form::checkbox('chkMakePrivate','Y', $isprivate, ['id'  =>  'chkMakePrivate']) }}
                        <span>Make this reminder private.</span>
                    </label>
                </div>
                <div class="i-checks">
                    <label><?php

                        $isreschedule = false;
                        if(isset($arrReminderDetail) && isset($arrReminderDetail['isreschedule']) && $arrReminderDetail['isreschedule'] == "Y") {
                            $isreschedule = true;
                        }

                        ?>{{ Form::checkbox('chkIsReschedule','Y', $isreschedule, ['id'  =>  'chkIsReschedule']) }}
                        <span>Disable Reschedule.</span>
                    </label>
                </div>
                <div class="i-checks">
                    <label><?php

                        $isdisable = false;
                        if(isset($arrReminderDetail) && isset($arrReminderDetail['isdisable']) && $arrReminderDetail['isdisable'] == "Y") {
                            $isdisable = true;
                        }

                        ?>{{ Form::checkbox('chkIsDisable','Y', $isdisable, ['id'  =>  'chkIsDisable']) }}
                        <span>Disable Cancel.</span>
                    </label>
                </div>                
                <div class="i-checks">
                    <label><?php
                    //echo '<pre>';print_r($arrReminderDetail);
                        $isCallback = false;
                        if(isset($arrReminderDetail) && isset($arrReminderDetail['iscallback']) && $arrReminderDetail['iscallback'] == "Y") {
                            $isCallback = true;
                        }

                        ?>{{ Form::checkbox('chkIsCallback','Y', $isCallback, ['id'  =>  'chkIsCallback']) }}
                        <span>Disable Callback.</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row add-new-checkbox reminder-types-table">
            <div class="col-md-12 {{ $errors->has('chkFlagTimeSlot') ? ' has-error timeslot' : '' }}">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 120px">Reminder Types</th>
                                @if(COUNT($flagcolumn) > 0)
                                    @foreach($flagcolumn AS $key => $reminderType)
                                        <th>
                                            <div class="i-checks">
                                                <label><?php

                                                    $flagMainCheckboxCheck = false;
                                                    $varName = strtolower('flag' . $reminderType);

                                                    if(isset($arrReminderDetail[$varName]) && $arrReminderDetail[$varName] == "Y") {
                                                        $flagMainCheckboxCheck = true;
                                                    }


                                                    ?>{{ Form::checkbox('chkFlag' . $reminderType,'Y', $flagMainCheckboxCheck, ['id'  =>  'chkFlag' . $reminderType, 'class' => 'flagHeadTimingChecks']) }}
                                                    <span>{{$reminderType}}</span>
                                                </label>
                                            </div>
                                        </th>
                                    @endforeach
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td>
                                    <div class="i-checks">
                                        <label>
                                            {{ Form::checkbox('chkFlagCheckAllReminderSlot','Y', false, ['id'  =>  'chkFlagCheckAllReminderSlot0', 'class' => 'flagCheckReminderSlots', 'data-reminderId' => 0 ]) }}
                                        </label>
                                    </div>
                                </td>

                                @if(COUNT($flagcolumn) > 0)
                                    @foreach($flagcolumn AS $key => $reminderType)
                                        <td class="tdBoxReminderCheckbox0">
                                            <div class="i-checks">
                                                <label>
                                                    {{ Form::checkbox('chkFlagTimeSlot[' . $reminderType . '][]','0', false, ['id'  =>  'chkFlagTimeSlot_' . $reminderType . '_0', 'data-reminderSlotId' => 0, 'data-reminderType' => $reminderType, 'class' => 'clickReminderSlotCheckBox' ]) }}
                                                    <span>Immediate</span>
                                                </label>
                                            </div>
                                        </td>
                                    @endforeach
                                @endif
                            </tr> --}}
                            
                            @if(!empty($arrSlots))
                                @foreach($arrSlots AS $key => $arrSlotInfo)
                                    <tr>
                                        <td>
                                            <div class="i-checks">
                                                <label>
                                                    <?php $flagSideCheckBoxCheck = true; ?>

                                                    @foreach($flagcolumn AS $key => $reminderType)
                                                        <?php
                                                            $varname = strtolower($reminderType . 'timingid');
                                                            if(isset($arrReminderDetail) && isset($arrReminderDetail[$varname]) && !empty($arrReminderDetail[$varname])) {

                                                                if(!in_array($arrSlotInfo['slotId'], explode(',', $arrReminderDetail[$varname]))) {
                                                                    $flagSideCheckBoxCheck = $flagSideCheckBoxCheck && false;
                                                                }
                                                                else {
                                                                    $flagSideCheckBoxCheck = $flagSideCheckBoxCheck && true;
                                                                }
                                                            }
                                                            else {
                                                                $flagSideCheckBoxCheck = false;
                                                            }

                                                        ?>
                                                    @endforeach        

                                                    {{ Form::checkbox('chkFlagCheckAllReminderSlot' . $arrSlotInfo["slotId"],'Y', $flagSideCheckBoxCheck, ['id'  =>  'chkFlagCheckAllReminderSlot' . $arrSlotInfo["slotId"], 'class' => 'flagCheckReminderSlots', 'data-reminderId' => $arrSlotInfo["slotId"]]) }}
                                                </label>
                                            </div>
                                        </td>

                                        @if(COUNT($flagcolumn) > 0)
                                            @foreach($flagcolumn AS $key => $reminderType)
                                                <td class="tdBoxReminderCheckbox{{ $arrSlotInfo['slotId'] }}">
                                                    <div class="i-checks">
                                                        <label><?php

                                                            $varname = strtolower($reminderType . 'timingid');

                                                            $flagCheckBoxSlot = false;
                                                            if(isset($arrReminderDetail) && isset($arrReminderDetail[$varname])) {
                                                                if(in_array($arrSlotInfo['slotId'], explode(',', $arrReminderDetail[$varname]))) {
                                                                    $flagCheckBoxSlot = true;
                                                                }
                                                            }

                                                            ?>
                                                            <?php /* @if($arrSlotInfo['slots'] != 'Immediate' || $reminderType != 'CALL')*/?>
                                                            {{ Form::checkbox('chkFlagTimeSlot[' . $reminderType . '][]',$arrSlotInfo["slotId"], $flagCheckBoxSlot, ['id'  =>  'chkFlagTimeSlot_' . $reminderType . '_' . $arrSlotInfo["slotId"], 'data-reminderSlotId' => $arrSlotInfo["slotId"], 'data-reminderType' => $reminderType, 'class' => 'clickReminderSlotCheckBox' ]) }}
                                                            <span>{{ $arrSlotInfo['slots'] }}</span>
                                                            <?php /* @endif */?>
                                                        </label>
                                                    </div>
                                                </td>
                                            @endforeach
                                        @endif
                                    </tr>
                                @endforeach
                            @endIf
                        </tbody>
                    </table>
                </div>
                @if ($errors->has('chkFlagTimeSlot'))
                    <span class="help-block">
                        <strong>{{ $errors->first('chkFlagTimeSlot') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Participant Details <small>Reminders Details</small></h5>
                {{-- <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div> --}}
            </div>
            
            
            <div class="ibox-content add-new-reminder-box">
                <div class="participant-details-box"><?php

                    if($hidReminderId == 0) {
                        ?><div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <div class="input-group m-b">
                                    <span class="input-group-addon" id="btnSearchForCustomers">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </span>
                                    {{ Form::text('txtSearchForCustomers', $strUserSearch, array( 'class'=>'form-control', 'placeholder' => 'Search', 'id' => 'txtSearchForCustomers' )) }}
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-7">
                                <select class="form-control" onchange="AddressBookContact(this, false)" style="width: 150px;">
                                    <option value=""></option>
                                        @foreach($addressbook as $row)
                                        <option value="{{$row->recid}}">{{$row->name}}</option>
                                        @endforeach
                                </select>
                                <a href="{{ URL::to('contacts/create') }}"  class="btn btn-primary pull-right btn-xs" target="_balnk">Add Contact</a>
<!--                                <button type="button" class="btn btn-primary import-appointment-btn pull-right btn-xs">Import Appointment</button>
                           -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">                   
                                    <table class="table table-striped table-bordered table-hover" id="searchTable" >
                                        <thead>
                                            <tr>
                                                <th style="width: 1px"></th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone Number</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">                   
                                <div class="clearfix add-selected-btn">
                                    <button type="button" class="btn btn-success pull-right btn-xs" id="btnAddSelect"><i class="fa fa-plus" aria-hidden="true"></i> Add Selected</button>
                                </div>
                            </div>
                        </div><?php
                    }
                    ?><div class="row">                                        
                        <div class="col-md-12">
                            <div class="participant-details-contact m-b">
                                <div class="table-responsive {{ $errors->has('selectedUsersData') ? ' has-error selecteduser' : '' }}">                  
                                    <table class="table table-striped table-bordered table-hover" id="selectedUsersTable" >
                                        <thead>
                                            <tr>    
                                                <th></th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone Number</th>
                                            </tr>
                                        </thead>

                                        @if(isset($arrSelectedUserInfo) && COUNT($arrSelectedUserInfo) > 0)
                                            <tbody>
                                                <tr>
                                                    <th style="width: 1px">
                                                        <a class="text-warning btnRemoveUserFromSelectedContact" href="javaScript:;" data-contactType="{{$arrSelectedUserInfo['contactType']}}" data-cardCode="{{$arrSelectedUserInfo['caseNo']}}">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </a>
                                                    </th>
                                                    <th>
                                                        <span class="editableClass editableFullName">{{$arrSelectedUserInfo['fullName']}}</span>
                                                    </th>
                                                    <th>
                                                        <span class="editableClass editableEmail">{{$arrSelectedUserInfo['email']}}</span>
                                                    </th>
                                                    <th>
                                                        <span class="editableClass editablePhoneNumber">{{$arrSelectedUserInfo['phoneNumber']}}</span>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        @endif
                                    </table>
                                    @if ($errors->has('selectedUsersData'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('selectedUsersData') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 submit-event-btn">   
                            <a href="{{url('/')}}" class="btn btn-danger btn-sm"><i class="fa fa-close" aria-hidden="true"></i> Cancel</a>                
                        
                             @if($view==0)
                            <button type="submit" class="btn btn-primary submit-event btn-sm saveEventValidation"><i class="fa fa-check" aria-hidden="true"></i> Submit Event</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><?php

    $selectedUsers = "";    
    if(isset($jsonSelectedUser) && $jsonSelectedUser != '') {
        $selectedUsers = $jsonSelectedUser;
    }

    ?>{{ Form::hidden('hidCheckBoxType', implode(',', $flagcolumn), ['id' => 'hidCheckBoxType'] ) }}


    {{ Form::hidden('selectedUsersData', '', ['id' => 'selectedUsersData'] ) }}

    {{ Form::hidden('hidReminderId', $hidReminderId, ['id' => 'hidReminderId'] ) }}
    {{ Form::hidden('selectedUsersDataFromEdit', $selectedUsers, ['id' => 'selectedUsersDataFromEdit'] ) }}
    
    
    <script type="text/javascript">
    $(window).bind("load", function() {
//        $("#txtCaseNo").keyAutocomplete();
    });
    $(document).ready(function() {
        contactSrc = "{{ route('quickcontactsave') }}";
        src = "{{ route('searchcaseno') }}";
         $("#txtCaseNo").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term : request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var valueSet = ui.item.value;
//                console.log(ui);
//                console.log(ui.item.value);
                $("#txtSearchForCustomers").val(valueSet);
                clearParticipantSelection();
                searchCustomersFromDatabase('case', triggerEvents);
                $(".myOptionalFields").val("").attr("disabled", 'disabled');
                $('.caseValueSelected').show();
            },
        });
        
        // remove disabled class if removed css is applied
        $(document).on('click', '.caseValueSelected', function(){
            $(this).parents().find('.ui-autocomplete-input').val("");
            $('.caseValueSelected').hide();
            $(".myOptionalFields").val("").removeAttr("disabled");
        });
        
    });
    
    function triggerEvents() {
//        $('.addThisContactInList').trigger('click');
    }
    
    function triggerAddButton() {
        //$('#btnAddSelect').triggerHandler('click');
    }
    </script>