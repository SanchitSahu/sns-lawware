@extends('layouts.app')
@section('title', 'SNS|Reminder')
@section('content')
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add New Reminder <small>Reminders Details</small></h5>
                {{-- <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div> --}}
            </div>

            {{ Form::open(['route' => 'updateReminder', 'id' => 'reminder-form', 'onSubmit' => 'return gatherAllFormInfo();']) }}
                
                @include("reminders/_form1")
            
            {{ Form::close() }}


@endsection


@section('styles')
<link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endsection

@section('scripts')

<!-- Clock picker -->
<script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>

<script src="{{ asset('js/webjs/addreminder.js') }}"></script>


@endsection