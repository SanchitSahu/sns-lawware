@extends('layouts.app')
@section('title', 'SNS|Reminder')
@section('content')

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Reminders table</h5>
                    {{-- <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div> --}}
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group m-b">
                                <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                <input type="text" placeholder="Search" class="form-control" name="txtSearchForReminderUser" id="txtSearchForReminderUser" />
                            </div>
                        </div>
                        <div class="col-sm-6">                                        
                            <div class="add-Ref-Rem-btn">
                                <a class="btn btn-success btn-xs" href="{{ route('addReminderPage')}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Add Reminder</span></a>
                                <a class="btn btn-info btn-xs" id="btnRefreshDatbleContent"><i class="fa fa-refresh"></i> Refresh</a>
                                <a href="#" class="btn btn-danger btn-xs" id="btnDeleteReminder"><i class="fa fa-trash"></i> <span class="bold">Remove</span></a>
                                <button class="btn btn-danger btn-xs" id="multidone"> 
                                    <span class="bold">Done</span>
                                    <i class="fa fa-question"></i> 
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row m-0">
                            <div class="col-sm-4">
                                <h4>Filter</h4>
                            </div>
                            <div class="col-sm-8">
                                @if(COUNT($arrAppointmentColor) > 0)
                                    <ul class="reminders-private m-t-xs">
                                        @foreach($arrAppointmentColor AS $key => $arrAppointmentColorInfo)
                                            <li>
                                                <span style="background-color: {{$arrAppointmentColorInfo->color}} !important"></span>{{$arrAppointmentColorInfo->statusvalue}}
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-7 col-sm-8">
                                <div class="row">
                                    <div class="col-sm-3 m-b-xs">
                                        {{ Form::select('lastDateRangeFilter',$arrDateRange,null,['id' => 'lastDateRangeFilter', 'class'=>'input-sm form-control input-s-sm inline'])}}
                                    </div>
                                    <div class="col-sm-3 m-b-xs hidden" id="divStartDate">
                                        <div id="input-group-datepicker" class="input-group date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            {{ Form::text('txtStartDate', '', array( 'class'=>'form-control', 'id' => 'txtStartDate', 'readonly' => 'readonly' )) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-3 m-b-xs hidden" id="divEndDate">
                                        <div id="input-group-datepicker" class="input-group date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            {{ Form::text('txtEndDate', '', array( 'class'=>'form-control', 'id' => 'txtEndDate', 'readonly' => 'readonly' )) }}
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-sm btn-primary btn-xs" id="btnSearchFilters">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 pull-right">
                               {{ Form::checkbox('selectall','',false,['class'=>'i-checks','id'=>'selectall']) }}
                                Select All 
                            </div>
                            <div class="col-md-3 col-sm-4 pull-right">
                                
                                {{ Form::checkbox('isprivate','Y',false,['class'=>'i-checks','id'=>'selectPcheck']) }}
                                Show only private Reminders 
                            </div>
                            
                            <input type="hidden" name="checkforparalegal" value="{{ $arrAdminSettingsparalegal }}" id='checkforparalegal'>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover " id="reminderDashboardTable">
                            <thead>
                                <tr>
                                    <th>Reminder Id</th>
                                    <th>Appointment Datetime</th>
                                    <th>Appointment Type</th>
                                    <th>Name</th>
                                    <th>Initial</th>
                                    <th>Case No</th>
                                    <th>Paralegal Handling</th>
                                    <th>Created By</th>
                                    <th>Confirm Datetime</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

   

@endsection


@section('scripts')


<script src="{{ asset('js/webjs/reminderhome.js') }}"></script>
@endsection