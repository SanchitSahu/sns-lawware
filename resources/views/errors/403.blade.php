@extends('layouts.error')
@section('content')

    <div class="middle-box text-center animated fadeInDown">
        <h1>{{ $exception->getStatusCode() }}</h1>
        <h3 class="font-bold">Permission denied.</h3>

        <div class="error-desc">
           sorry you have not got permission
           
        </div>
    </div>

   @endsection

