@extends('layouts.error')
@section('title', 'SNS|Reminder')
@section('content')

<div class="middle-box text-center animated fadeInDown">
    <h1>{{ $exception->getStatusCode() }}</h1>
    <h3 class="font-bold">Page Not Found</h3>

    <div class="error-desc">
        The server encountered something unexpected that didn't allow it to complete the request. We apologize.

    </div>
</div>

@endsection