@extends('layouts.app')
@section('title', 'SNS|Contacts')
@section('content')   

<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Contacts<small></small></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>                            
        <div class="ibox-content contactindex">
            <div class="row">
<!--                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissable alertcontact" style="display: none">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <div class="import_error_message"></div>
                </div> 
                    <div class="m-t-20">
                        <button type="button" class="btn btn-primary btn-xs m-r-10 pull-right" data-toggle="modal" data-id="" data-title=""  data-target="#address-book">
                            Add New Address book
                        </button>
                        <button type="button" class="btn btn-danger btn-xs m-r-10 pull-right" id="remove-contacts" >Remove Address book</button>
                       

                    </div>
                </div>-->
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="newbookname">
                            <input type="hidden" id="addressbookid">
                        </div>
                        <div class="col-md-3 pull-right">
                            <button type="button" onclick="addNewAddressBook()" class="btn btn-warning btn-xs pull-right">+ Add</button>
                        </div>
                    </div>
                    <div class="table-responsive contacts-table" style="margin-top: 10px">
                        <table class="table table-striped table-bordered table-hover" i>
                            <thead>
                                <tr>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody id="addressbooklist">
                                @foreach($data_list as $row)
                                <tr onclick="setaddressbookid({{$row->recid}})" id="addressBookList{{$row->recid}}" class="addressBookListing">
                                    <td>{{$row->name}}<span class="pull-right" onclick="deleteAddressBook({{$row->recid}})" style="color:red;cursor: pointer;"><i class="fa fa-trash" aria-hidden="true"></i></span></td>
                                </tr>
                                    @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="table-responsive contacts-table" style="margin-top: 10px">
                        <table class="table table-striped table-bordered table-hover" i>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody id="contact_list">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="col-md-6">

                    <div class="table-responsive contacts-table">
                        <table class="table table-striped table-bordered table-hover" id="contactsdatatable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Case No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
<!--                                    <th style="width: 90px">Edit</th>
                                    <th style="width: 90px">Remove</th>-->
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var url = "{{ route('datatables.data') }}";
</script>
<script src="{{ asset('js/webjs/addressbook.js') }}"></script>

@endsection


