 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-sm-3{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                    {{ Form::label('firstname', 'First Name',['class'=>"control-label"]) }}
                                    {{ Form::text('firstname', old('firstname'), array('class' => 'form-control')) }}

                                    @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="col-md-3 col-sm-3{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                    {{ Form::label('lastname', 'Last Name',['class'=>"control-label"]) }}
                                    {{ Form::text('lastname', old('lastname'),['class' => 'form-control']) }}
                                    @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-8 col-sm-8{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {{ Form::label('email', 'Email',['class'=>"control-label"]) }}
                                    {{ Form::text('email',null, array('class' => 'form-control')) }}

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6{{ $errors->has('username') ? ' has-error' : '' }}">
                        {{ Form::label('username', 'User Name') }}  
                          {{ Form::text('username',null, ['class' => 'form-control','maxlength' => 15]) }}
                        @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                <div class="col-md-6 col-sm-6{{ $errors->has('password') ? ' has-error' : '' }}">
                        {{ Form::label('password', 'Password') }}  
                          <input type="password" name="password" value="{{$users->password ?? ''}}" class="form-control" maxlength="15">

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>

                </div>
               
                <div class="clearfix">
                    {!! Form::submit('Save changes', ['class' => 'btn btn-primary pull-right btn-xs m-l-10']) !!}
                   <a class="btn btn-white pull-right btn-xs" href="{{{ URL::route('users.index') }}}">Cancel</a>

                </div>