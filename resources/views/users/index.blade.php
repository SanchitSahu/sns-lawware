@extends('layouts.app')
@section('title', 'SNS|Users')
@section('content')   

<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Users<small></small></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>                            
        <div class="ibox-content userindex">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissable alertuser" style="display: none">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <div class="import_error_message"></div>
                </div> 
                    <div class="m-t-20">
                        <a href="#" class="btn btn-primary pull-right btn-xs refreshstable"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                      
                        <a href="{{ URL::to('users/create') }}" class="btn btn-primary pull-right m-r-10 btn-xs">Add Users</a>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive contacts-table">
                        <table class="table table-striped table-bordered table-hover" id="usersdatatable">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <!--<th>Phone Number</th>-->
                                    <th style="width: 90px">Edit</th>
                                    <th style="width: 90px">Remove</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var url = "{{ route('datatables.ur.data') }}";
</script>
<script src="{{ asset('js/webjs/users.js') }}"></script>

@endsection


