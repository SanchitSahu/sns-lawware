<div class="modal inmodal fade" id="address-book" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            {!! Form::open(['route' => 'import-csv-excel','method'=>'POST','files'=>'true','id'=>'importcontactform']) !!}
            {{ csrf_field() }}




            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>

                <h4 class="modal-title">Select Csv file to be uploaded</h4>
            </div>

            <div class="modal-body add-address-book">
                <div class="clearfix">
                    <div class="form-group">
                        <div class="alert alert-danger alert-dismissable" style="display: none">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <div class="import_error_message"></div>
                        </div> 
                  
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                         {{ Form::file('import_contact', ['class' => 'field' ,'id'=>'imgInp']) }}
                         
                          
                        </div>

                    </div>
                    <a href="{{ URL::to('downloadsamplefile') }}" target="_blank" class="downlaodsadmplecsv" >Download sample link </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" id="formclose" data-dismiss="modal">Close</button>

                {!! Form::submit('Upload files',['class'=>'btn btn-primary']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>