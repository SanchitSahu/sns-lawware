@extends('layouts.app')
@section('title', 'SNS|Logs')
@section('content')   

<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Log</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <h4>Filter</h4>
                </div>
                <div class="col-sm-8 col-xs-8">
                    @if(COUNT($twiliologcolor) > 0)
                    <ul class="reminders-private reminders-private-log m-t-xs">
                        @foreach($twiliologcolor as $key => $value)
                        <li>
                            <span style="background-color: {{$value}} !important"></span>{{$key}}
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </div>

                <div class="clearfix">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-sm-3 m-b-xs">
                                {{ Form::select('dateRangeFilter',$daterange,null,['id' => 'dateRangeFilter', 'class'=>'input-sm form-control input-s-sm inline'])}}
                            </div>
                            <div class="col-sm-3 m-b-xs hidden" id="divStartDate">
                                <div id="input-group-datepicker" class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    {{ Form::text('txtStartDate', '', array( 'class'=>'form-control', 'id' => 'txtStartDate', 'readonly' => 'readonly' )) }}
                                </div>
                            </div>

                            <div class="col-sm-3 m-b-xs hidden" id="divEndDate">
                                <div id="input-group-datepicker" class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    {{ Form::text('txtEndDate', '', array( 'class'=>'form-control', 'id' => 'txtEndDate', 'readonly' => 'readonly' )) }}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <button type="button" class="btn btn-sm btn-primary btn-xs" id="btnSearchFilters">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="twilologs">
                <thead>
                    <tr>
                <th style="width: 50px"></th>
                 <th style="width: 50px"></th>
                        <th>No</th>
                        <th>To</th>
                        <th>Status</th>
                        <th>Duration</th>
                        <th>Start Date</th>
                        <th>Type</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Case no</th>
                        <th>Reminder Id</th>
                    </tr>
                </thead>

            </table>

        </div>
    </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    var url = "{{ route('log.data') }}";
</script>
<script src="{{ asset('js/webjs/twilologs.js') }}"></script>

@endsection

