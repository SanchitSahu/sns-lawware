@extends('layouts.app')
@section('title', 'SNS | Add Templates')
@section('content')   

<div class="wrapper wrapper-content animated fadeInRight add-template">
    <div class="row">
        
                {{ Form::open(['route' => 'mastertemplates.store', 'id' => 'contact-form']) }}
                {{ csrf_field() }}
                     @include("mastertemplates/_form")
                <!-- https://laracast.blogspot.in/2016/06/laravel-ajax-crud-search-sort-and.html  -->
               
                {{ Form::close() }}

  

    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    var url = "{{ route('templates.data') }}";
</script>
<script src="{{ asset('js/webjs/mastertemplates.js') }}"></script>
@endsection



