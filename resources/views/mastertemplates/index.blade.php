
@section('title', 'SNS|Templates')

<div class="panel-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins m-0">
                <div class="ibox-title">
                    <h5>Templates<small></small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>                            
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <a href="#" class="btn btn-primary pull-right btn-xs refreshstable"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                <a href="{{ URL::to('mastertemplates/create') }}" class="btn btn-primary pull-right m-r-10 btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Add New Template</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive contacts-table">
                                <table class="table table-striped table-bordered table-hover" id="mastertemplates">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Title</th>
                                            <th style="width: 150px">Created Date</th>
                                            <th style="width: 148px">BODY</th>
                                            <th>Appointment Type</th>
                                            <th style="width: 50px">Edit</th>
                                            <th style="width: 50px">Remove</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


