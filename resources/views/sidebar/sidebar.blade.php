   <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="profile-element profile-element-name">
                            <h2>Reminder</h2>                            
                        </div>
                        <div class="logo-element">
                            RE+
                        </div>
                    </li>
       

                    <li @if(\Request::path()=='/') class= "active" @endif >
                        <a href="{{ url('/') }}"  title="Reminder"><i class="fa fa-bell"  ></i> <span class="nav-label"   >Reminders</span> </a>
                    </li>
                   <li @if(\Request::path()=='contacts' || \Request::route()->getName()=='contacts.create' || \Request::route()->getName()=='contacts.edit') class= "active" @endif >
                        <a href="{{ url('/contacts') }}" title="Contacts"><i class="fa fa-compress"></i> <span class="nav-label">Contacts</span></a>
                    </li>
                 <li @if(\Request::path()=='logs') class= "active" @endif >
                        <a href="{{ url('/logs') }}" title="Log"><i class="fa fa-file-text"></i> <span class="nav-label">Logs</span></a>
                    </li>
                     @if(Auth::user()->role =='admin')
                   <li @if(\Request::path()=='settings' || \Request::route()->getName()=='mastertemplates.create' ||  \Request::route()->getName()=='mastertemplates.edit') class= "active" @endif >
                        <a href="{{ url('/settings') }}" title="Settings"><i class="fa fa-cog"></i> <span class="nav-label">Settings</span></a>
                    </li>
                   
                    <li @if(\Request::path()=='users' || \Request::route()->getName()=='users.create' ||  \Request::route()->getName()=='users.edit') class= "active" @endif >
                        <a href="{{ url('/users') }}" title="Users"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a>
                    </li>
                    
                    <li @if(\Request::path()=='addressbook') class= "active" @endif >
                        <a href="{{ url('/addressbook') }}" title="Addressbook"><i class="fa fa-book" aria-hidden="true"></i> <span class="nav-label">Address Book</span></a>
                    </li>
                    @endif

                    <!--<li>
                        <a data-toggle="modal" data-target="#address-book" href="#"><i class="fa fa-book"></i> <span class="nav-label">Address Book</span>  </a>
                    </li>-->
                </ul>
            </div>
        </nav>