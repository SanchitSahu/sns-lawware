@extends('layouts.app')
@section('title', 'SNS|Contacts')
@section('content')   

<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Contacts<small></small></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>                            
        <div class="ibox-content contactindex">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissable alertcontact" style="display: none">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <div class="import_error_message"></div>
                </div> 
                    <div class="m-t-20">
                        <a href="#" class="btn btn-primary pull-right btn-xs refreshstable"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                      
                        <a href="{{ URL::to('contacts/create') }}" class="btn btn-primary pull-right m-r-10 btn-xs">Add Contacts</a>
                       

                        <button type="button" class="btn btn-primary btn-xs m-r-10 pull-right" data-toggle="modal" data-id="" data-title=""  data-target="#address-book">
                            Import Contacts
                        </button>
                        <button type="button" class="btn btn-danger btn-xs m-r-10 pull-right" id="remove-contacts" >Remove Contacts</button>
                       

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="clear-fix col-md-3">
                        <input type="checkbox" id="contact-selectall">
                        <label for="contact-selectall">Select All</label>
                    </div>
                <div class="col-md-12">

                    <div class="table-responsive contacts-table">
                        <table class="table table-striped table-bordered table-hover" id="contactsdatatable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Company Name</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th style="width: 90px">Edit</th>
                                    <th style="width: 90px">Remove</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td class="non_searchable"></td>
                                    <td class="non_searchable"></td>
                                    <td class="non_searchable"></td>
                                    <td class="non_searchable" style="width: 90px"></td>
                                    <td class="non_searchable" style="width: 90px"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var url = "{{ route('datatables.data') }}";
</script>
<script src="{{ asset('js/webjs/contacts.js') }}"></script>

@endsection


