<div class="panel-body">
    <div class="ibox float-e-margins m-0">
        <div class="ibox-title">
            <h5>Reports</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        {!! Form::model($arrSettings, [
        'method' => 'POST',
        'route' => ['export.report', $arrSettings->recid],
        'id' => 'settingsreport-form'

        ]) !!}

        {{ csrf_field() }}
        <div class="ibox-content">
            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">    
                        <label>From Date :</label>
                        <div id="input-group-fromdate" class="input-group date ">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span> {{ Form::text('fromdate',old('fromdate'), ['class' => 'form-control','id'=>'fromdate']) }}
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">    
                        <label>To Date :</label>
                        <div id="input-group-todate" class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span> {{ Form::text('todate',old('todate'), ['class' => 'form-control','id'=>'todate']) }}
                           
                        </div>
                         <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-sm-12 m-t-10">
                     {!! Form::submit('Export Report', ['class' => 'btn btn-primary pull-right btn-xs']) !!}
     
             
             
                </div>
            </div>
        </div>
           
        {{ Form::close() }}
    </div>
</div>