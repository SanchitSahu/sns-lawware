<div class="panel-body">                                                
    <div class="row m-0">
        {!! Form::model($arrSettings, [
        'method' => 'PATCH',
        'route' => ['setting.updateemail', $arrSettings->recid],
        'id' => 'settingsemail-form'

        ]) !!}

        {{ csrf_field() }}
        <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="ibox float-e-margins m-0">
                <div class="ibox-title">
                    <h5>Email and SMS<small></small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row m-0">

                        <div class="form-group{{ $errors->has('emailfrom') ? ' has-error' : '' }}">
                            {{ Form::label('emailfrom', 'Email From') }}  
                            {{ Form::text('emailfrom', null, array('class' => 'form-control')) }}  
                            <span class="help-block" id="emailfrom"></span>
                        </div>
                        <div class="form-group{{ $errors->has('emailsubject') ? ' has-error' : '' }}">
                            {{ Form::label('emailsubject', 'Email Subject') }}  
                            {{ Form::text('emailsubject', null, array('class' => 'form-control')) }}  
                            <span class="help-block" id="emailsubject"></span>
                        </div>

                        <div class="form-group{{ $errors->has('twiliosmsnumber') ? ' has-error' : '' }}">
                            {{ Form::label('twiliosmsnumber', 'SMS Sender') }}  
                            {{ Form::text('twiliosmsnumber', null, array('class' => 'form-control')) }}  
                            <span class="help-block"  id ="twiliosmsnumber"></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-6 col-sm-6">
            <div class="ibox float-e-margins m-0">
                <div class="ibox-title">
                    <h5>Call and Product Key<small></small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row m-0">

                        <div class="form-group{{ $errors->has('twiliocallnumber') ? ' has-error' : '' }}">
                            {{ Form::label('twiliocallnumber', 'Call Number') }}  
                            {{ Form::text('twiliocallnumber', null, array('class' => 'form-control')) }}  
                            <span class="help-block twiliocallnumber"></span>
                        </div>

                        <div class="form-group{{ $errors->has('twilioaccountsid') ? ' has-error' : '' }}">
                            {{ Form::label('twilioaccountsid', 'Twilio Accountsid') }}  
                            {{ Form::text('twilioaccountsid', null, array('class' => 'form-control')) }}  
                            <span class="help-block twiliocallnumber"></span>
                        </div>

                        <div class="form-group{{ $errors->has('twilioauthtoken') ? ' has-error' : '' }}">
                            {{ Form::label('twilioauthtoken', 'Twilio Authtoken') }}  
                            {{ Form::text('twilioauthtoken', null, array('class' => 'form-control')) }}  
                            <span class="help-block twilioauthtoken"></span>
                        </div>

                        <div class="form-group{{ $errors->has('clientkey') ? ' has-error' : '' }}">
                            {{ Form::label('clientkey', 'Product Key') }}  
                            {{ Form::text('clientkey', null, array('class' => 'form-control')) }}  
                            <span class="help-block clientkey"></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 m-t-20">
            {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right btn-xs updatesetting']) !!}
            <input type="hidden" name="_token" value="{{ Session::token() }}" >
        </div>
        {{ Form::close() }}
    </div>
</div>