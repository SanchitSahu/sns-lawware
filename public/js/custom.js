$(document).ready(function () {

    jQuery.fn.keyAutocomplete = function(objValue) {
        var selector = $(this);
        data = [];
        var resultData = window[selector.attr("data_callback")];
        console.log(resultData);
        $(selector).autocomplete({
            source: resultData,
            delay: 0,
            minLength: 0,
            autoFocus: true,
            autoFill: true,
            search: function(oEvent, oUi) {
                // get current input value
                var sValue = $(oEvent.target).val();
                // init new search array
                var aSearch = [];
                //var aSearch1 = [];
                // for each element in the main array ...
                $(resultData).each(function(iIndex, sElement) {
                    // ... if element starts with input value ...
                    if (sElement.search_keys.search(/sValue/i)) {
                        displaText = sElement.label;
                        if (sElement.value != undefined) {
                            displaText = displaText + ' (' + sElement.value + ')';
                        }
                        //
                        data = {label: displaText, value: sElement.value};
                        aSearch.push(data);
                        //
                        //aSearch.push(displaText);
                    }
                    /*for (key in sElement) {
                     if(sElement[key].search(/sValue/i)) {
                     aSearch.push(sElement[key]);
                     }
                     }*/
                });
                //console.log(aSearch1);
                //console.log(aSearch);
                // change search array
                selector.autocomplete('option', 'source', aSearch);
            },
            select: function(event, ui) {
                return;
                var label = ui.item.label;
                var value = ui.item.value;
                //console.log(document.valueSelectedForAutocomplete);
                //document.valueSelectedForAutocomplete = value 
                //$(".searchIcon").trigger("click");
                //console.log(selector.attr('data-onSelect'));
                selector.val(value);
                window[selector.attr('data-onSelect')]();
            }
        }).on("autocompleteselect", function(e, ui) {
            var label = ui.item.label;
            var value = ui.item.value;
            selector.val(value);
            window[selector.attr('data-onSelect')]();
        });
    }

});

setInterval(function () {
    console.log('test');
    var title = "New Reminder(s) Added";
    var desc = "Click to open";
    var url = hidAbsUrl;
    $.ajax({
        url: hidAbsUrl + '/remindernotify',
        method: "GET",
        success: function (result) {
            var data = $.parseJSON(result);
            var cnt = data.length;
            if (cnt > 0) {
                notifyBrowser('(' + cnt + ')' + title, desc, url);
            } else {
                return false;
            }
        }
    });
}, 70000);

document.addEventListener('DOMContentLoaded', function ()
{

    if (Notification.permission !== "granted")
    {
        Notification.requestPermission();
    }

    /*document.querySelector("#notificationButton").addEventListener("click", function(e)
     {
     var x = Math.floor((Math.random() * 10) + 1);
     var title=articles[x][0];
     var desc='Most popular article.';
     var url=articles[x][1];
     notifyBrowser(title,desc,url);
     e.preventDefault();
     });*/

});

function notifyBrowser(title, desc, url)
{
    if (!Notification) {
        console.log('Desktop notifications not available in your browser..');
        return;
    }
    if (Notification.permission !== "granted")
    {
        Notification.requestPermission();
    } else {
        var notification = new Notification(title, {
//            icon: 'https://lh3.googleusercontent.com/-aCFiK4baXX4/VjmGJojsQ_I/AAAAAAAANJg/h-sLVX1M5zA/s48-Ic42/eggsmall.png',
            icon: 'storage/app/public/SNS.ico',
            body: desc,
        });

// Remove the notification from Notification Center when clicked.
        notification.onclick = function () {
            window.open(url);
            Notification.close();
        };

// Callback function when the notification is closed.
        notification.onclose = function () {
            console.log('Notification closed');
        };

    }
}

