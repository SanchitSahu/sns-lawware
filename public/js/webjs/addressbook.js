
function setaddressbookid($id){
    $('#addressbookid').val($id);
    $('#contact_list').html('');
    var addressId = $('#addressbookid').val();
    var hidAbsUrl = $('#hidAbsUrl').val();
    $.ajax({
        url: hidAbsUrl + "/addressbookContact/"+ $id,
        cache: false,
        success: function(response){
            $('.addressBookListing').css('background-color','');
            $('#addressBookList'+addressId).css('background-color','#f8ac59');
            if(response.length > 0){
                var i = 0;
                for(i = 0;i < response.length; i++){//console.log(response[i]);
                    var id = "'"+response[i].cardCode+"'";
                    $('#contact_list').append('<tr><td>'+ response[i].fullName +'</td><td>'+ response[i].email +'</td><td><span class="pull-right" onclick="deleteContactToBook('+id+')" style="color:red;cursor: pointer;"><i class="fa fa-trash" aria-hidden="true"></i></span></td></tr>');
                }
            }
            else{
                $('#contact_list').append('<tr><td colspan="3" align="center">No data found</td></tr>');
            }
    }
      });
}

function addContactToBook(contactId){
    var addressId = $('#addressbookid').val();
    var hidAbsUrl = $('#hidAbsUrl').val();
    $.ajax({
        url: hidAbsUrl + "/addContact/"+ addressId + '/' + contactId,
        cache: false,
         beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function(html){
            setaddressbookid($('#addressbookid').val());
        }
      });
}

function deleteContactToBook(cardCode){
    var hidAbsUrl = $('#hidAbsUrl').val();
    var addressId = $('#addressbookid').val();
    $.ajax({
        url: hidAbsUrl + "/deleteContact/"+addressId+'/'+ cardCode,
        cache: false,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function(html){
            setaddressbookid($('#addressbookid').val());
        }
      });
}

function allAddressBook(){
    $('#addressbooklist').html('');
    var hidAbsUrl = $('#hidAbsUrl').val();
    var name = $('#newbookname').val();
    $.ajax({
        url: hidAbsUrl + "/allAddressBook",
        cache: false,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function(response){
            console.log(response);
            var i = 0;
            for(i = 0;i < response.length; i++){
                $('#addressbooklist').append('<tr onclick="setaddressbookid('+response[i].recid+')" id="addressBookList'+response[i].recid+'" class="addressBookListing"><td>'+response[i].name+'<span class="pull-right" onclick="deleteAddressBook('+response[i].recid+')" style="color:red;cursor: pointer;"><i class="fa fa-trash" aria-hidden="true"></i></span></td></tr>');
            }
        }
      });
}

function deleteAddressBook(addid){
    var hidAbsUrl = $('#hidAbsUrl').val();
    $.ajax({
        url: hidAbsUrl + "/deleteAddressBook/"+addid,
        cache: false,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function(response){
            console.log(response); 
            allAddressBook();
        }
      });
}

function addNewAddressBook(cardCode){
    var hidAbsUrl = $('#hidAbsUrl').val();
    var name = $('#newbookname').val();
    $.ajax({
        url: hidAbsUrl + "/addNewBook",
        type: 'POST',
        data: {'name': name},
        cache: false,
         beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function(html){
            $('#newbookname').val('');
            setaddressbookid($('#addressbookid').val());
            allAddressBook();
        }
      });
}

$(document).ready(function () {

    $('#contactsdatatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
         
        oLanguage: { sEmptyTable: "No records found" },
        columns: [

            //{data: 'check', name: 'check'},
            {
                mRender: function (data, type, row) {
                    return '<button type="button" onclick="addContactToBook(`'+ row.cardCode+'`)" class="btn btn-warning btn-xs m-r-10 pull-right">+ Add</button>';
                },
                targets: 0
            },
            {data: 'caseNo', name: 'caseNo'},
            {data: 'fullName', name: 'fullName'},
            {data: 'email', name: 'email'},
            {data: 'phoneNumber', name: 'phoneNumber'},
//            {
//                mRender: function (data, type, row) {
//                    hidAbsUrl = $('#hidAbsUrl').val();
//
//                    return '<a class="text-info" href=' + hidAbsUrl + '/contacts/' + row.cardCode + '/edit><i class="fa fa-pencil" aria-hidden="true"></i></a>';
//                },
//                orderable: false,
//                searchable: false,
//                className: "center",
//            },
//
//            {
//                mRender: function (data, type, row) {
//                   if(!$.isNumeric(row.cardCode)) {    
//                    return '<a class="text-warning editor_remove" href="" data-key=' + row.cardCode + '><i class="fa fa-times" aria-hidden="true"></i></a>';
//                   }
//                   return '';
//                    },
//                className: "center",
//                orderable: false,
//                searchable: false
//            },
        ],
        "columnDefs": [
            { "searchable": false, "targets": 0 }
          ]

    });

//
//    // Delete a record
//    $('#contactsdatatable').on('click', 'a.editor_remove', function (e) {
//
//        e.preventDefault();
//        if (confirm("Are you sure you want to delete contact "))
//        {
//            var table = $('#contactsdatatable').DataTable();
//            var parent = $(this).attr('data-key');
//            $.ajax({
//                url: hidAbsUrl + "/contacts/" + parent,
//                type: 'DELETE',
//            }).done(function (result_data) {
//                        var objErrorToastNotication = {
//                                    message: "Contacts deleted successfully"
//                                };
//                                setToastNotification(objErrorToastNotication);
//                table.row($(this).parents('tr')).remove().draw(false);
//
//            });
//            return false;
//        }
//    });
//    
   

});
$(document).on('click','#contact-selectall',function(){
    $('.contact-check:checkbox').prop('checked', this.checked); 
});
$(document).on('click','#remove-contacts',function(e){
e.preventDefault();
            var checkids = [];
            $.each($('.contact-check:checkbox'), function (index, item) {
                checkids.push($(item).val());
            });
        if (confirm("Are you sure you want to Remove contact(s)?")) {
            if (checkids.length > 0)
            {
                $.ajax({
                    url: hidAbsUrl + '/removecontacts',
                    data: {id:'0', checkids:checkids},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        if (data > 0) {
                            $('#contactsdatatable').DataTable().draw();
                            return false;
                        } else {
                            return false;
                        }
                    }
                });
            }
    } else {
        return false;
    }
});



    