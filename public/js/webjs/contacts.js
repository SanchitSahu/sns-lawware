
$(document).ready(function () {
    
var year = (new Date).getFullYear();
      $('#input-group-datepicker').datepicker({
      
        endDate: new Date(year, 0, 0),

        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        dateFormat: 'yy-mm-dd',
        autoclose: true,     
       
    });
    
    // Have DataTables throw errors rather than alert() them
    $.fn.dataTable.ext.errMode = 'throw';
    
    var tableX = $('#contactsdatatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
         
        oLanguage: { sEmptyTable: "No records found" },
        columns: [

            //{data: 'check', name: 'check'},
            {
                mRender: function (data, type, row) {
                    return "<input type='checkbox' class='contact-check' value='"+ row.cardCode + "' />";
                },
                targets: 0
            },
            {data: 'notes', name: 'notes'},
            {data: 'fullName', name: 'fullName'},
            {data: 'email', name: 'email'},
            {data: 'phoneNumber', name: 'phoneNumber'},
            {
                mRender: function (data, type, row) {
                    hidAbsUrl = $('#hidAbsUrl').val();

                    return '<a class="text-info" href=' + hidAbsUrl + '/contacts/' + row.cardCode + '/edit><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "center",
            },

            {
                mRender: function (data, type, row) {
                   if(!$.isNumeric(row.cardCode)) {    
                    return '<a class="text-warning editor_remove" href="" data-key=' + row.cardCode + '><i class="fa fa-times" aria-hidden="true"></i></a>';
                   }
                   return '';
                    },
                className: "center",
                orderable: false,
                searchable: false
            },
        ],

        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [1,2,3,4]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [1,2,3,4]
                }
            },
            
        ],

        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],

        initComplete: function () {
            var i = 0;
            this.api().columns().every(function () {
                var column = this;
               
                /// 1 for case no column and 4 for phone number column
                if(i == 1 || i == 4) {
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                }
                i++;
            });
        }

    });

    tableX.button(0).nodes().css('background', '#87ceeb');
    tableX.button(0).nodes().css('margin-left', '20px');
    tableX.button(0).nodes().css('padding', '2px 10px 2px 10px');
    tableX.button(1).nodes().css('background', '#00FFFF');
    tableX.button(1).nodes().css('padding', '2px 10px 2px 10px');

    // Delete a record
    $('#contactsdatatable').on('click', 'a.editor_remove', function (e) {

        e.preventDefault();
        if (confirm("Are you sure you want to delete contact "))
        {
            var table = $('#contactsdatatable').DataTable();
            var parent = $(this).attr('data-key');
            $.ajax({
                url: hidAbsUrl + "/contacts/" + parent,
                type: 'DELETE',
            }).done(function (result_data) {
                        var objErrorToastNotication = {
                                    message: "Contacts deleted successfully"
                                };
                                setToastNotification(objErrorToastNotication);
                table.row($(this).parents('tr')).remove().draw(false);

            });
            return false;
        }
    });
    
       
  $('#address-book').on('hidden.bs.modal', function (e) {
       $(".add-address-book").find('.alert-danger').hide();
       $(".add-address-book").find(".import_error_message").html("");
       $('#importcontactform')[0].reset();
       
    
    });

    $('form#importcontactform').submit(function (e) {

        e.preventDefault();

        var formData = new FormData($(this)[0]);
        $.ajax({
            url: hidAbsUrl + "/import-csv",
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,

            success: function (data) {
        
                  $('#contactsdatatable').DataTable().draw();
                $(".add-address-book").find('.alert-danger').hide();
                $(".import_error_message").html("");
                if (data.error == 2) {
                    var errors = data.errormessage;

                    $(".add-address-book").find('.alert-danger').show();
                    $(".import_error_message").html(errors);


                } else if (data.error == 1) {
                    var errors = data.errormessage;
                 
                    $(".contactindex").find('.alertcontact').show();
                    $(".import_error_message").html(errors);
                 $('#formclose').trigger('click');
                } else if (data.error == 0) {
                    $('#formclose').trigger('click');
                      $(".contactindex").find('.alertcontact').hide();
                    var objErrorToastNotication = {
                        message: data.message
                    };
                    setToastNotification(objErrorToastNotication);
                }
            },
        });
    })

  

});
$(document).on('click','#contact-selectall',function(){
    $('.contact-check:checkbox').prop('checked', this.checked); 
});
$(document).on('click','#remove-contacts',function(e){
e.preventDefault();
            var checkids = [];
            $.each($('.contact-check:checkbox'), function (index, item) {
                checkids.push($(item).val());
            });
        if (confirm("Are you sure you want to Remove contact(s)?")) {
            if (checkids.length > 0)
            {
                $.ajax({
                    url: hidAbsUrl + '/removecontacts',
                    data: {id:'0', checkids:checkids},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        if (data > 0) {
                            $('#contactsdatatable').DataTable().draw();
                            return false;
                        } else {
                            return false;
                        }
                    }
                });
            }
    } else {
        return false;
    }
});



    