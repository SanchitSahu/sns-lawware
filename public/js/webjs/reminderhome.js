var hidAbsUrl = "";
var arrDeleteReminderId = [];


$(document).ready(function () {
    hidAbsUrl = $('#hidAbsUrl').val();

    // Have DataTables throw errors rather than alert() them
    $.fn.dataTable.ext.errMode = 'throw';

    /* Init DataTables */
    reminderDashboardTable = $('#reminderDashboardTable').DataTable({
        stateSave: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: hidAbsUrl + '/getReminderDashboard',
            type: "GET",
            data: function (d) {
                d.dateRangeFilter = $('#lastDateRangeFilter').val(),
                d.startDate = $('#txtStartDate').val();
                d.endDate = $('#txtEndDate').val();
                d.isPrivate = $('#selectPcheck').is(':checked');
                d.searchText = $("#reminderDashboardTable_filter input").val();
                //d.localdata = localStorage.getItem("DataTables_reminderDashboardTable_/snsweb/");

            }
        },
        columns: [
            {data: 'recid', name: 'recid'},
            {data: 'reminderDateTime', name: 'reminderDateTime'},
            {data: 'purpose', name: 'purpose'},
            {data: 'fullname', name: 'fullname'},
            {data: 'snsusersunqid', name: 'snsusersunqid'},
            {data: 'caseno', name: 'caseno'},
            {data: 'para_hand', name: 'para_hand'},
            {data: 'snsUserName', name: 'snsUserName'},
            {data: 'confirmDate', name: 'confirmDate'}
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return row.recid;
                },
                width: "10%",
                targets: 0
            },
            {
                render: function (data, type, row) {
                    return row.reminderDateTime;
                },
                width: "10%",
                targets: 1
            },
            {
                render: function (data, type, row) {
                    return row.purpose;
                },
                width: "10%",
                targets: 2
            },
            {
                render: function (data, type, row) {
                    return row.fullname;
                },
                width: "10%",
                targets: 3
            },
            {
                render: function (data, type, row) {
                    return row.snsusersunqid;
                },
                width: "10%",
                targets: 4
            },
            {
                render: function (data, type, row) {
                    return row.caseno;
                },
                width: "10%",
                targets: 5
            },
            {
                render: function (data, type, row) {
                    return row.para_hand;
                },
                width: "20%",
                targets: 6
            },
            {
                render: function (data, type, row) {
                    return row.snsUserName;
                },
                width: "12%",
                targets: 7
            },
            {
                render: function (data, type, row) {
                    return row.confirmDate;
                },
                width: "10%",
                targets: 8
            },
            {
                render: function (data, type, row) {

                    var twilioStatus = row.twiliostatus;
                    var reminderId = row.recid;
                    var isdone = row.isdone;

                    var url = '<input type="checkbox" name="chkDeleteReminder" class="form-control deleteReminder" data-reminderId="' + reminderId + '" value="' + reminderId + '"/>&nbsp;&nbsp;';

//                    if (twilioStatus == null) {
                        url += '<a class="text-danger" href="' + hidAbsUrl + '/' + reminderId + '/reminderEdit" ><i class="fa fa fa-pencil-square-o" aria-hidden="true"></i></a>';
//                    }
                    if (isdone == 0) {
                        url += '<button id="done_reminder" data-id="' + reminderId + '" class="pull-right btn btn-sm btn-danger btn-xs" style="margin: 3px;">Done?</button>';
                    } else {
                        url += '<span class="pull-right" style="font-weight:700;margin: 3px 5px 0 0;">Done</span>';
                    }
                    return url;
                },
                width: "10%",
                targets: 9
            }
        ],

        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [0,1,2,3,4,6,7,8]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [0,1,2,3,4,6,7,8],
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            
        ],

        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],

        

        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        },
        createdRow: function (row, data, index) {
            $(row).css('background-color', data.twilioResponseColor);
        }

    });

    reminderDashboardTable.button(0).nodes().css('background', '#87ceeb');
    reminderDashboardTable.button(0).nodes().css('margin-left', '20px');
    reminderDashboardTable.button(0).nodes().css('padding', '2px 10px 2px 10px');
    reminderDashboardTable.button(1).nodes().css('background', '#00FFFF');
    reminderDashboardTable.button(1).nodes().css('padding', '2px 10px 2px 10px');
    
//    console.log(reminderDashboardTable.state());
    var checkforparalegal = $("#checkforparalegal").val() 
        if(checkforparalegal == 0)
            {
                //var column = reminderDashboardTable.column(4);
                reminderDashboardTable.columns([5]).visible( false );
                reminderDashboardTable.columns([4]).visible( true );
            }
            else{
                reminderDashboardTable.columns([4]).visible( true );
                reminderDashboardTable.columns([5]).visible( true );
            }

    $('#reminderDashboardTable tbody').on('dblclick', 'tr td:not(:last-child)', function () {
        var table = $('#reminderDashboardTable').DataTable();

        var data = $(this).parent('tr').find('.deleteReminder').attr('data-reminderid');
//        window.location.replace(data + "/reminderEdit/view");
        window.location.replace(data + "/reminderEdit");
    });
    $(document.body).delegate('.deleteReminder', 'click', function (e) {
        var thisObj = $(this);

        var flagChecked = thisObj.prop('checked');
        var reminderId = thisObj.val();

        if (flagChecked) {
            arrDeleteReminderId = [...arrDeleteReminderId, reminderId];
        } else {
            _.pull(arrDeleteReminderId, reminderId);
        }
    });

    $('#btnDeleteReminder').on('click', function () {
        if (_.isEmpty(arrDeleteReminderId)) {
            var objDataParams = {
                message: "Please select atleast one reminder to delete"
            };
            setToastNotification(objDataParams);
        } else {
            if (confirm("Are you sure you want to Delete this Reminder(s)?")) {
                var urlToGetData = hidAbsUrl + '/deleteReminder';
                var objDataParams = {
                    reminderIds: arrDeleteReminderId
                }

                $.ajax({
                    url: urlToGetData,
                    method: "GET",
                    data: objDataParams,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {

                        if (response['data'] == 'undefined' || response['flagMsg'] == 'undefined' || response['error'] == 'undefined') {
                            var objErrorToastNotication = {
                                message: "Some Error Occured"
                            };
                            setToastNotification(objErrorToastNotication);
                            return false;
                        }

                        var error = response['error'];
                        var data = response['data'];
                        var flagMsg = response['flagMsg'];
                        var message = "";

                        if (typeof response['message'] != 'undefined') {
                            message = response['message'];
                        }

                        if (error == "1") {
                            switch (flagMsg) {
                                default :
                                    var objErrorToastNotication = {
                                        message: "Some Error Occured"
                                    };
                                    setToastNotification(objErrorToastNotication);
                                    break;
                            }
                        } else {
                            switch (flagMsg) {
                                case "EMPTY":
                                    var objErrorToastNotication = {
                                        message: message
                                    };
                                    setToastNotification(objErrorToastNotication);
                                    break;
                                case "UPDATE":
                                    var objErrorToastNotication = {
                                        message: message
                                    };
                                    setToastNotification(objErrorToastNotication);
                                    reminderDashboardTable.draw();

                                    break;
                            }
                        }
                    }
                });
            }
            else{
                return false;
            }
        }
    });

    $('#btnRefreshDatbleContent').on('click', function (e) {
        e.preventDefault();
        reminderDashboardTable.draw();
    });

    $('#txtStartDate').datepicker();
    $('#txtEndDate').datepicker();

    $('#lastDateRangeFilter').on('change', function () {
        var thisValue = $(this).val();

        $('#txtStartDate').val('');
        $('#txtEndDate').val('');

        $('#divStartDate').addClass('hidden');
        $('#divEndDate').addClass('hidden');

        if (thisValue == "SD") {
            $('#divStartDate').removeClass('hidden');
            $('#divEndDate').addClass('hidden');
        } else if (thisValue == "CR") {
            $('#divStartDate').removeClass('hidden');
            $('#divEndDate').removeClass('hidden');
        }
    });
//refresh table when checked
    $('#btnSearchFilters').on('click', function (e) {
        e.preventDefault();

        reminderDashboardTable.draw();
    });

    $('#txtSearchForReminderUser').on('keypress', function (e) {
        if (e.keyCode == 13) {
            redirectToAddReminder($(this).val());
        }
    });
    /** 
     * trigger refresh event on interval 
     * 
     * @returns {Boolean}
     */
    setInterval(function () {
        $('#btnRefreshDatbleContent').trigger('click');
    }, 60000);
});

$('#selectPcheck').on('click', function (e) {
    reminderDashboardTable.draw();
});
$(document).on('click', '#done_reminder', function () {
    var id = $(this).attr('data-id');
    if (confirm("Are you sure you want to Done this Reminder?")) {
        if (id != 0 && id != '')
        {

            $.ajax({
                url: hidAbsUrl + '/isdone',
                data: {id: id},
                method: "POST",
                success: function (result) {
                    var data = $.parseJSON(result);
                    console.log(data);
                    if (data == 1) {
                        reminderDashboardTable.draw();
                        return false;
                    } else {
                        return false;
                    }
                }
            });
        }
    } else {
        return false;
    }
});
function redirectToAddReminder(strSearchUser) {

    if (!_.isEmpty(strSearchUser)) {
        var urlToRedirect = hidAbsUrl + '/addReminder/' + strSearchUser;
        window.location = urlToRedirect;
    } else {
        var objErrorToastNotication = {
            message: "Please enter search keyword"
        };
        setToastNotification(objErrorToastNotication);
        $('#txtSearchForReminderUser').focus();
    }
}
$(document).on('change', '#lastDateRangeFilter', function () {
    var thisval = $(this).val();
    if (thisval != '')
    {
        $('#btnRefreshDatbleContent').trigger('click');
    }
    return false;
});
$(document).on('click','#selectall',function(){
    $('input[name=chkDeleteReminder]:checkbox').prop('checked', this.checked); 
});
$(document).on('click','#multidone',function(e){
e.preventDefault();
            var checkids = [];
            $.each($('input[name=chkDeleteReminder]:checked'), function (index, item) {
                checkids.push($(item).val());
            });
        if (confirm("Are you sure you want to Done Reminder(s)?")) {
            if (checkids.length > 0)
            {
                $.ajax({
                    url: hidAbsUrl + '/isdone',
                    data: {id:'0', checkids:checkids},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        console.log(data);
                        if (data > 0) {
                            reminderDashboardTable.draw();
                            return false;
                        } else {
                            return false;
                        }
                    }
                });
            }
    } else {
        return false;
    }
});
