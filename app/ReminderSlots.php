<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderSlots extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_reminder_slots';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function scopeActive($query) {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }

    public static function getslots() {
//        $fields= ReminderSlots::select([\DB::raw('recid slotId, concat(value," ",unit) as slots')])->active()->get()->toArray(); 
        $fields = ReminderSlots::select([\DB::raw('recid slotId, IF(value = 0 and LEFT(unit, 5) = "immed", unit, concat(value," ",unit)) as slots')])->active()->get()->toArray();
        return $fields;
    }

    public static function getSlotsAllInformation() {
        $reminderSlots = ReminderSlots::all()->toArray();
        return $reminderSlots;
    }

}
