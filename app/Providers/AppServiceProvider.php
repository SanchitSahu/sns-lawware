<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // adding custom validation for sms reminder timeslot check 
        Validator::extend('reminderTimingSlot', function ($attribute, $value, $parameters, $validator) {

            $flagReturn = false;

            if(isset($value['CALL']) && !empty($value['CALL'])) {
                $flagReturn = true;
            }
            if(isset($value['SMS']) && !empty($value['SMS'])) {
                $flagReturn = true;
            }
            if(isset($value['EMAIL']) && !empty($value['EMAIL'])) {
                $flagReturn = true;
            }

            return $flagReturn;
        });

        // adding custom validation for selected user when add/update reminder
        Validator::extend('selectedGuestForReminder', function ($attribute, $value, $parameters, $validator) {

            $flagReturn = false;

            if(!empty($value)) {
                $arrGuest = json_decode($value);
                if(count($arrGuest) > 0) {
                    $flagReturn = true;
                }
            }

            return $flagReturn;
        });
        
        
        // adding custom validation for selected user when add/update reminder
        Validator::extend('empty_with', function ($attribute, $value, $parameters, $validator) {

$parametervalue=array_get($validator->getData(), $parameters[0]); 

            if(empty($value) && empty($parametervalue)){
              return false;
            }
            return true;
        });
        
        
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
