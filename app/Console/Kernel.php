<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        // $schedule->command('inspire')
        //          ->hourly();

        /* $schedule->call('\App\Http\Controllers\SendRequestToServer@syncLatestRemindersToServer')->everyMinute();
          $schedule->call('\App\Http\Controllers\SendRequestToServer@getLatestReminderStatusFromCloud')->everyMinute();
          $schedule->call('\App\Http\Controllers\SendRequestToServer@getLatestReminderTwilioUpdateFromCloud')->everyMinute();
          $schedule->call('\App\Http\Controllers\SendRequestToServer@syncEmailSettings')->everyMinute();
          $schedule->call('\App\Http\Controllers\SendRequestToServer@getLatestAllReminderStatusFromCloud')->everyMinute();
         */
        $schedule->call('\App\Http\Controllers\ReminderController@calauditsns')->everyMinute()->name('task7');
        $schedule->call('\App\Http\Controllers\ReminderController@syncStaffDetailsToUsers')->everyMinute()->name('task8');

        $schedule->call('\App\Http\Controllers\SendRequestToServer@syncLatestRemindersToServer')->everyMinute()->name('task1');//->withoutOverlapping();
        $schedule->call('\App\Http\Controllers\SendRequestToServer@getLatestReminderStatusFromCloud')->everyMinute()->name('task2');//->withoutOverlapping();
        $schedule->call('\App\Http\Controllers\SendRequestToServer@getLatestReminderTwilioUpdateFromCloud')->everyMinute()->name('task3');//->withoutOverlapping();
        $schedule->call('\App\Http\Controllers\SendRequestToServer@syncEmailSettings')->everyMinute()->name('task4');//->withoutOverlapping();
        $schedule->call('\App\Http\Controllers\SendRequestToServer@getLatestAllReminderStatusFromCloud')->everyMinute()->name('task5');//->withoutOverlapping();
        
        
        $schedule->call('\App\Http\Controllers\ContactController@syncContactFromWindowsToWeb')->hourly()->name('contactmigrate')->withoutOverlapping();

        // $schedule->call('\App\Http\Controllers\ReminderController@calauditsns')->everyFiveMinutes()->name('task7') ->withoutOverlapping(); 
        //bulk reminder api $setting = \App\AdminSetting::find(1); if($setting->flagcsv==1){ $schedule->call('\App\Http\Controllers\BulkReminderController@addBulkReminder')->hourly()->name('task6') ->withoutOverlapping();
        //bulk reminder api
        $setting = \App\AdminSetting::find(1);
        if ($setting->flagcsv == 1) {
            //$schedule->call('\App\Http\Controllers\BulkReminderController@addBulkReminder')->hourly();
            $schedule->call('\App\Http\Controllers\BulkReminderController@addBulkReminder')->hourly()->name('task6')->withoutOverlapping();
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        require base_path('routes/console.php');
    }

}
