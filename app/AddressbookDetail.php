<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use App\Http\Controllers\SendRequestToServer;

class AddressbookDetail extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'address_book_detail';
    protected $primaryKey = 'recid';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    static function getContact($id){
        $data = AddressbookDetail::where('addid', $id)->get();
        return $data;
    }
    
    static function deleteContact($addid, $cardcode){
        $data  = AddressbookDetail::where('addid', $addid)->where('cardcode',$cardcode)->delete();
        return $data;
    }

}
