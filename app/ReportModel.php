<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ReportModel extends Model
{
    
    
   public static function getExportData($fromdate,$todate){
       
     return  ReminderSync::select(\DB::raw('CONCAT_WS(" ",sns_users.firstname,sns_users.lastname) as createdusers'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.remindertype="CALL" THEN sns_reminder_sync.recid ELSE NULL END) totalcalls'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.remindertype="SMS" THEN sns_reminder_sync.recid ELSE NULL END) totalsms'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.remindertype="EMAIL" THEN sns_reminder_sync.recid ELSE NULL END) totalemail'),
             \DB::raw('COUNT(sns_reminder_sync.snsusersid) as totalreminders'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.flagdisablecancelreschedule="Y" THEN sns_reminder_sync.recid ELSE NULL END) cancellable'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder.isprivate="Y" THEN sns_reminder_sync.recid ELSE NULL END) private'),
               \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.twiliostatus=2 THEN sns_reminder_sync.recid ELSE NULL END) tcanclebyuser'),
              \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.twiliostatus=1 THEN sns_reminder_sync.recid ELSE NULL END) tconfirmbyuser'),
             \DB::raw(' ROUND(COUNT(DISTINCT CASE WHEN sns_reminder_sync.twiliostatus=1 THEN sns_reminder_sync.recid ELSE NULL END) * 100 / COUNT(sns_reminder_sync.snsusersid) ) percentage')
             )->leftjoin('reminder', 'reminder.recid', '=', 'reminder_sync.reminderid')
             ->leftjoin('users', 'users.recid', '=', 'reminder_sync.snsusersid')
             ->whereBetween(\DB::raw('date_format(sns_reminder_sync.remindereventdatetime,"%Y-%m-%d")'),[$fromdate,$todate])          
             ->groupby('reminder_sync.snsusersid')
             ->get()->toArray(); 
       
   }
   
   public static function getTotalData($fromdate,$todate){
       
              
     return  ReminderSync::select(
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.remindertype="CALL" THEN sns_reminder_sync.recid ELSE NULL END) totalcalls'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.remindertype="SMS" THEN sns_reminder_sync.recid ELSE NULL END) totalsms'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.remindertype="EMAIL" THEN sns_reminder_sync.recid ELSE NULL END) totalemail'),
             \DB::raw('COUNT(sns_reminder_sync.snsusersid) as totalreminders'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.flagdisablecancelreschedule="Y" THEN sns_reminder_sync.recid ELSE NULL END) cancellable'),
             \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder.isprivate="Y" THEN sns_reminder_sync.recid ELSE NULL END) private'),
               \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.twiliostatus=2 THEN sns_reminder_sync.recid ELSE NULL END) tcanclebyuser'),
              \DB::raw('COUNT(DISTINCT CASE WHEN sns_reminder_sync.twiliostatus=1 THEN sns_reminder_sync.recid ELSE NULL END) tconfirmbyuser'),
             \DB::raw(' ROUND(COUNT(DISTINCT CASE WHEN sns_reminder_sync.twiliostatus=1 THEN sns_reminder_sync.recid ELSE NULL END) * 100 / COUNT(sns_reminder_sync.snsusersid) ) percentage')
             )->whereBetween(\DB::raw('date_format(sns_reminder_sync.remindereventdatetime,"%Y-%m-%d")'),[$fromdate,$todate])
             ->leftjoin('reminder', 'reminder.recid', '=', 'reminder_sync.reminderid')
             ->leftjoin('users', 'users.recid', '=', 'reminder_sync.snsusersid')           
             ->get()->toArray(); 
 
       
   }
    
}
