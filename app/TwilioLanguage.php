<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwilioLanguage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_twilio_language';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function scopeActive($query) {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }

    public static function getlanguage() {

        $language = TwilioLanguage::active()->pluck('name', 'recid')->toArray();

        return $language;
    }

    public static function getDefaultLanguage() {
        $language = TwilioLanguage::where('isDefaultLang', '1')->active()->pluck('name', 'recid')->toArray();
        return $language;
    }

    public static function updateDefaultLanguage($arrParams) {
        $languageId = $arrParams['languageId'];

        // delete other default flag where previously selected 
        $twilioDefaultLangUpdate = TwilioLanguage::where('isdefaultLang', '1')->first();
        if (!empty($twilioDefaultLangUpdate)) {
            $twilioDefaultLangUpdate->isDefaultLang = '0';
            $twilioDefaultLangUpdate->save();
        }

        // save new  language which is updated as default value.
        $twilioDefaultLang = TwilioLanguage::find($languageId);
        if (!empty($twilioDefaultLang)) {
            $twilioDefaultLang->isDefaultLang = '1';
            $twilioDefaultLang->save();
        }

        return true;
    }

}
