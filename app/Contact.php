<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DB;
use Excel;
use URL;

class Contact extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_list';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';
    const IS_DELETED = 1;
    const IS_NOT_DELETED = 0;
    const IS_ACTIVE = 'A';
    const IS_NOT_ACTIVE = 'I';

    protected $fillable = ['firstname', 'lastname', 'middlename', 'case', 'phonenumber', 'zipcode',
        'notes', 'email', 'phonetype', 'salutation', 'emailtype', 'created_at', 'updated_at', 'dob',
        'firmcode', 'cardcode', 'country_id', 'win_con_id'];

    public function setFirstNameAttribute($value) {
        $this->attributes['firstname'] = ucwords($value);
    }

    public function setLastNameAttribute($value) {
        $this->attributes['lastname'] = ucwords($value);
    }

    public function setDobAttribute($value) {
        $this->attributes['dob'] = Carbon::parse($value)->format('Y-m-d');
    }
    
      

    public function getDobAttribute($value) {
        return Carbon::parse($value)->format('m/d/Y');
    }

    public function scopeActive($query) {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }

    public static function phonetype() {
        return ['0' => 'Mobile', '1' => 'Home', '2' => 'Office', '3' => 'Personal', '4' => 'Other'];
    }

    public static function emailtype() {
        return ['0' => 'Home', '1' => 'Work', '2' => 'Other'];
    }

    public static function salutationtype() {
        return ['Mr.' => 'Mr.', 'Ms.' => 'Ms.', 'Miss' => 'Miss', 'Mrs.' => 'Mrs.', 'Dr.' => 'Dr.', 'Prof.' => 'Prof.'];
    }
    
    public static function editcontacts($id){
             try {
           
           $result = \DB::connection('mysql3')->select(' SELECT c.cardcode,DATE_FORMAT(c.birth_date, "%m/%d/%Y") dob,c2.zip zipcode,c.comments notes, cc.caseno,'
                        . 'c.first firstname, c.middle middlename, c.last lastname, '
                        . ' c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phonenumber,'
                        . ' salutation'
                        . '  FROM card c, card2 c2, casecard cc '
                        . ' WHERE c.firmcode = c2.firmcode  '
                        . ' AND cc.cardcode = c.cardcode '
                       . ' AND c.cardcode ='.$id
                        );
           $contact=collect($result)->first();
           
        } catch (\Exception $e) {
            $contact = Contact::where('cardcode',$id)->first();
        }
        return $contact;
    }
     
    
        public static function listcontacts(){


        $lawwareContacts = DB::connection('mysql3')->select('SELECT cardCode, caseNo, fullName, email, '
            . 'phoneNumber,salutation,notes '
            . ' FROM (SELECT c.cardcode cardCode, cc.caseno caseNo,'
            . ' CONCAT_WS(" ", c.salutation,c.first, c.middle, c.last) fullName, '
            . ' c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,'
            . ' c.salutation salutation,'
            . ' c.comments notes'
            . '  FROM card c, card2 c2, casecard cc '
            . ' WHERE c.firmcode = c2.firmcode  '
            . ' AND cc.cardcode = c.cardcode '
            . ' GROUP BY c.cardcode HAVING (fullName <> "" AND (email <> "" OR phoneNumber <> "") )) AS t2'
        );

        $snsContacts = DB::select('SELECT cardCode, caseNo, fullName, email, '
            . 'phoneNumber,salutation,notes '
            . ' FROM (SELECT cl.cardcode cardCode, cl.case caseNo, '
            . '         CONCAT_WS(" ", cl.salutation,cl.firstname, cl.middlename, cl.lastname) fullName,'
            . '        cl.email email, IF(smc.rec_id < "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,'
            . ' cl.salutation salutation,'
            . ' cl.notes notes'
            . ' FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON'
            . ' cl.country_id = smc.rec_id AND smc.isactive = "A" AND smc.isdeleted = 0 where cl.isactive = "A" AND cl.isdeleted = 0'
            . ' ) AS t2'
        );

        $result = array_merge($lawwareContacts, $snsContacts);
    
    //  $result = \DB::select('SELECT cardCode, caseNo, fullName, email, '
    //                     . 'phoneNumber,salutation '
    //                     . ' FROM ( (  SELECT c.cardcode cardCode, cc.caseno caseNo,'
    //                     . ' CONCAT_WS(" ", c.salutation,c.first, c.middle, c.last) fullName, '
    //                     . ' c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,'
    //                     . ' c.salutation salutation'
    //                     . '  FROM card c, card2 c2, casecard cc '
    //                     . ' WHERE c.firmcode = c2.firmcode  '
    //                     . ' AND cc.cardcode = c.cardcode '
    //                     . ' GROUP BY c.cardcode HAVING (fullName <> "" AND (email <> "" OR phoneNumber <> "") ))  '
    //                     . 'UNION   '
    //                     . ' (SELECT cl.cardcode cardCode, cl.case caseNo, '
    //                     . '         CONCAT_WS(" ", cl.salutation,cl.firstname, cl.middlename, cl.lastname) fullName,'
    //                     . '        cl.email email, IF(smc.rec_id < "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,'
    //                     . ' cl.salutation salutation'
    //                     . ' FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON'
    //                     . ' cl.country_id = smc.rec_id AND smc.isactive = "A" AND smc.isdeleted = 0 where cl.isactive = "A" AND cl.isdeleted = 0'
    //                     . ' )) AS t2 '
    //                     );
     return $result;
     
        }
        
         /**
     * Get a validator for an incoming fb registeration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validatorcreate(array $data) {

        return Validator::make($data, [
            'phonenumber' => 'empty_with:email',
                
        ]);
    }
        
        public static function addCsvUpload($data){
              $errors = [];
              DB::beginTransaction();
                    foreach ($data as $key => $value) {

                        $countryCode = $value['countrycode'];
                        if(substr($countryCode, 0 ,1) != "+"){
                            $countryCode = "+".$countryCode;
                        }

                        $countryId = \DB::select('SELECT rec_id FROM sns_master_countries WHERE countrycode = '.$countryCode);

                        $arr = ['salutation' => $value['salutations'],
                            'firstname' => $value['fname'],
                            'lastname' => $value['lname'],
                            'middlename' => $value['mname'],
                            'phonenumber' => $value['phone'],
                            'email' => $value['email'],
                            'zipcode' => $value['zipcode'],
                            'notes' => $value['note'],
                            'phonetype' => $value['phonetype'],
                            'country_id' => isset($countryId) ? $countryId[0]->rec_id : 0,
                        ];

                        $validator = self::validatorcreate($arr);
                        //send error message

                        if ($validator->fails()) {
                            $validation = $validator->errors()->first();
                            $errors[$key] = $value;
                            $errors[$key]['errormessage'] = ucwords($validation);
                        } else {

                            $contacts = Contact::create($arr);

                            if ($contacts) {
                                $lastinsertedid = "RS" . $contacts->recid;
                                $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
                                $contacts->update($caseId);
                            }
                        }
                    }
                    DB::commit();

                    DB::rollback();
                    
                    if (!empty($errors)) {
                        $products = $errors;
                        Excel::create('synergynotification', function($excel) use ($products) {
                            $excel->sheet('sns1', function($sheet) use ($products) {
                                $sheet->fromArray($products);
                                $sheet->row(1, ['Salutations', 'FirstName', 'Lastname', 'Middlename', 'Countrycode', 'Phone', 'Email', 'Zipcode', 'Note', 'Phonetype', 'Message']);
                            });
                        })->store('csv');
                        $url=URL::to("downloadexcelfile");
                        return ['error' => 1, 'errormessage' => 'there is some error in your contacts file.please  <a href="'.$url.'"> click here </a>for error logs'];
                    }

                    return ['error' => 0, 'message' => 'contacts imported Successfully'];
        }

}
