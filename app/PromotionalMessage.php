<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class PromotionalMessage extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bulk_promotinal_message';
    protected $primaryKey = 'recid';
    
    
    
   public static function addPromotionalData($data){
    
      // remove null value
                $c = function($v) {
                    return array_filter($v) != array();
                };
                $data = (array_values(array_filter($data, $c)));
               $arr=[];
                foreach ($data as $key => $value) {
                    $apptime=Carbon::parse($value[7])->format('H:i:s');
                    if($apptime<='08:00'){
                    $appt_time=Carbon::parse($value[7])->addHours(12)->format('H:i:s');
                    }else{
                    $appt_time=$apptime;
                    }
                    $arr[$key] = [
                        'account' => $value[0],
                        'last_name' => $value[1],
                        'first_name' => $value[2],
                        'home' => $value[3],
                        'work' => $value[4],
                        'appointment' => $value[5],
                        'phone_number'=>($value[3]==0?($value[4]==0)?$value[4]:$value[5]:$value[3]),
                        'appt_date' => Carbon::parse($value[6])->format('Y-m-d'),
                        'appt_time' => $appt_time,
                        'appt_doctor' => $value[8],
                        'appt_room' => $value[9],
                        'room_no'=> (($value[8]!=0)?$value[8]:$value[9]),
                        'appt_room_location' => $value[10],
                        'appt_room_class' => $value[11],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                    
                    if($value[1]=='CLOSED' || $value[2]=='CLOSED' || $arr[$key]['phone_number']==0 ){
                        unset($arr[$key]);
                    }
                } 
           //  (new Http\Controllers\BulkReminderController)->addContact($arr);
             $insertpromotional = PromotionalMessage::insert($arr);
             self::syncBulkPromotionalData(array_values($arr));
            
   }
   
      public static function syncBulkPromotionalData($arr){
     

        $addinsert = [];
        $addcontact = [];
        foreach ($arr as $key => $value) {
                     
                $addinsert[] = $value;
                $addinsert[$key]['created_at'] = Carbon::now();
                $addinsert[$key]['updated_at'] = Carbon::now();
                //add contact if its new
                $contact_fetch = \App\Contact::select(['phonenumber'])
                        ->where('phonenumber', $value['phone_number'])
                        ->first();
                if (empty($contact_fetch)) {
                    $addcontact[$key]['firstname'] = $value['first_name'];
                    $addcontact[$key]['lastname'] = $value['last_name'];
                    $addcontact[$key]['phonenumber'] = $value['phone_number'];
                    $addcontact[$key]['country_id'] = 93;
                    $addcontact[$key]['createddate'] = Carbon::now();
                    $addcontact[$key]['updateddate'] = Carbon::now();
                
              }
        } 
    
        //update contact id
       (new Http\Controllers\BulkReminderController)->addContact($addcontact);
       (new Http\Controllers\BulkReminderController)->syncBulkReminder($addinsert,1,Auth::id());
     
      }
      
    
   
}
