<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;
use App\ReminderSlots;
use App\MasterTemplates;
use App\TwilioLanguage;
use App\Http\Requests\ReminderForm;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\MasterReminderResponseColor;
use Datatables;
use Auth;
use App\AdminSetting;
use App\calaudit1;
use App\User;

class ReminderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $arrDateRange = ReminderController::getDateRangeArray();
        $arrAppointmentColor = MasterReminderResponseColor::getResponseColors();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();
        $arrAdminSettings = AdminSetting::getInfo();
        $arrResponseToSend = array(
            'arrDateRange' => $arrDateRange,
            'arrAppointmentColor' => $arrAppointmentColor,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'arrAdminSettingsparalegal' => $arrAdminSettings->onoffparalegal
        );

        return view('reminders/home')->with($arrResponseToSend);
    }

    public static function getDateRangeArray() {
        return array(
            'UP' => 'Upcoming',
            'TD' => 'Today',
            'TO' => 'Tomorrow',
            'PW' => 'Past Week',
            'SD' => 'Specific Date',
            'CR' => 'Custom Range'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($strUserSearch = "") {
        $view = 0;
        $arrSlots = ReminderSlots::getslots();
        $arrTemplate = MasterTemplates::all();
        $flagcolumn = ['SMS', 'CALL', 'EMAIL'];
        $language = \App\TwilioLanguage::getlanguage();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();
        $fields = \App\ReminderFields::getfields();
        $slots = \App\ReminderSlots::getslots();
        $column = [0, 1, 2, 3];
        $countrycode = \App\MasterCountry::getcountry();

        $arrResponseToSend = array(
            'arrSlots' => $arrSlots,
            'arrTemplate' => $arrTemplate,
            'flagcolumn' => $flagcolumn,
            'language' => $language,
            'fields' => $fields,
            'slots' => $slots,
            'column' => $column,
            'strUserSearch' => $strUserSearch,
            'view' => $view,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'countrycode' => $countrycode
        );

        return view('reminders/addreminder')->with($arrResponseToSend);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReminderForm $request) {
        return ReminderController::addUpdateReminderDetails($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function edit($reminderId) {
        $view = 0;
        //view data in reminder
        $parts = (\Request::path());
        $ex = explode("/", $parts);

        if (isset($ex[2])) {
            $view = 1;
        }
        $arrSlots = ReminderSlots::getslots();
        $arrTemplate = MasterTemplates::all();
        $flagcolumn = ['SMS', 'CALL', 'EMAIL'];
        $language = \App\TwilioLanguage::getlanguage();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();
        $fields = \App\ReminderFields::getfields();
        $slots = \App\ReminderSlots::getslots();
        $column = [0, 1, 2, 3];
        $countrycode = \App\MasterCountry::getcountry();

        $arrReminderDetail = Reminder::getReminderDetails($reminderId);

        $arrSelectedUser = array();

        $arrSelectedUserInfo = array();
        $arrSelectedUserInfo['cardCode'] = $arrReminderDetail['cardcode'];
        $arrSelectedUserInfo['caseNo'] = $arrReminderDetail['caseno'];
        $arrSelectedUserInfo['contactType'] = $arrReminderDetail['contacttype'];
        $arrSelectedUserInfo['salutation'] = $arrReminderDetail['salutation'];
        $arrSelectedUserInfo['firstName'] = $arrReminderDetail['firstname'];
        $arrSelectedUserInfo['lastName'] = $arrReminderDetail['lastname'];
        $arrSelectedUserInfo['fullName'] = $arrReminderDetail['fullName'];
        $arrSelectedUserInfo['email'] = $arrReminderDetail['email'];
        $arrSelectedUserInfo['phoneNumber'] = $arrReminderDetail['phoneno'];

        array_push($arrSelectedUser, $arrSelectedUserInfo);

        $jsonSelectedUser = json_encode($arrSelectedUser);

        $arrResponseToSend = array(
            'arrSlots' => $arrSlots,
            'arrTemplate' => $arrTemplate,
            'flagcolumn' => $flagcolumn,
            'language' => $language,
            'fields' => $fields,
            'slots' => $slots,
            'column' => $column,
            'arrReminderDetail' => $arrReminderDetail,
            'reminderId' => $reminderId,
            'arrSelectedUserInfo' => $arrSelectedUserInfo,
            'jsonSelectedUser' => $jsonSelectedUser,
            'view' => $view,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'countrycode' => $countrycode
        );

        return view('reminders/editreminder')->with($arrResponseToSend);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reminder $reminder) {
        return ReminderController::addUpdateReminderDetails($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $reminder) {
        //
    }
    
    public static function checkImpromptuValues($request) {
        $chkImpromptu = $request->input('chkImpromptu');
        $txtCaseNo = $request->input('txtCaseNo');
        $flagSyncA1Law = 0;
        if((isset($chkImpromptu) && !empty($chkImpromptu)) && (isset($txtCaseNo) && !empty($txtCaseNo))) {
            $flagSyncA1Law = 1;
        }
//        echo  $flagSyncA1Law;exit;  
        echo "<pre>";print_r($request->all());exit;
    }

    public static function addUpdateReminderDetails($request) {

        DB::beginTransaction();

//        self::checkImpromptuValues($request);
//echo "<pre>";print_r($request->all());exit;
        
        $reminderClass = new ReminderController();

        try {

            $strResponseMessage = "Reminder Added Succesfully";
            $txtReminderTitle = $request->input('txtReminderTitle') ?? $request->input('txtAppointmentType');
            $txtDate = $request->input('txtDate');
            $txtTime = $request->input('txtTime');
//            $reminderDateTimeFrm = $txtDate . " " . $txtTime . ":00";
            $reminderDateTimeFrm = $txtDate . " " . $txtTime;
            $finalDate = Carbon::parse($reminderDateTimeFrm);
            $reminderDateTime = (string) $finalDate;

            $lstTemplates = $request->input('lstTemplates');
            $txtAppointmentType = $request->input('txtAppointmentType');
            $lstLanguage = $request->input('lstLanguage');
            $taAppoinmentTemplateBody = $request->input('taAppoinmentTemplateBody');
            $chkIsReschedule = $request->input('chkIsReschedule');
            $chkMakePrivate = $request->input('chkMakePrivate');
            $arrTimeSlot = $request->input('chkFlagTimeSlot');
            $selectedUsersData = $request->input('selectedUsersData');

            $reminderId = $request->input('hidReminderId');

            if (!is_numeric($reminderId) || $reminderId <= 0) {
                $reminderId = 0;
            }

            $arrUsersData = json_decode($selectedUsersData);


            // checking for slots;
            $flagSMS = "N";
            $strSMSTimingIds = "";
            if (isset($arrTimeSlot['SMS']) && !empty($arrTimeSlot['SMS'])) {
                $flagSMS = "Y";
                $strSMSTimingIds = implode(',', $arrTimeSlot['SMS']);
            }

            $flagCALL = "N";
            $strCALLTimingIds = "";
            if (isset($arrTimeSlot['CALL']) && !empty($arrTimeSlot['CALL'])) {
                $flagCALL = "Y";
                $strCALLTimingIds = implode(',', $arrTimeSlot['CALL']);
            }

            $flagEMAIL = "N";
            $strEMAILTimingIds = "";
            if (isset($arrTimeSlot['EMAIL']) && !empty($arrTimeSlot['EMAIL'])) {
                $flagEMAIL = "Y";
                $strEMAILTimingIds = implode(',', $arrTimeSlot['EMAIL']);
            }
            
//            $chkImpromptu = 0;
            $flagsyncbacktoa1law = 0;
//            if(isset($request->input('chkImpromptu'))) {
////                $chkImpromptu = 1;
//                $flagsyncbacktoa1law = 1;
//            }

            // if reminder id is zero, that means it is an add case.
            if ($reminderId == 0) {
                if (COUNT($arrUsersData) > 0) {
                    foreach ($arrUsersData AS $userIndex => $arrEachUserData) {
                        $phone = $arrEachUserData->phoneNumber;
                        $arrParamsToCreateReminder = array();
                        $arrParamsToCreateReminder['snsusersid'] = Auth::id();
                        $arrParamsToCreateReminder['cardcode'] = $arrEachUserData->cardCode;
                        $arrParamsToCreateReminder['caseno'] = $arrEachUserData->caseNo;
                        $arrParamsToCreateReminder['contacttype'] = $arrEachUserData->contactType;
                        $arrParamsToCreateReminder['salutation'] = $arrEachUserData->salutation;
                        $arrParamsToCreateReminder['firstname'] = $arrEachUserData->firstName;
                        $arrParamsToCreateReminder['lastname'] = $arrEachUserData->lastName;
                        $arrParamsToCreateReminder['fullname'] = $arrEachUserData->fullName;
                        $arrParamsToCreateReminder['reminderdatetime'] = $reminderDateTime;
                        $arrParamsToCreateReminder['title'] = $txtReminderTitle;
                        $arrParamsToCreateReminder['purpose'] = $txtAppointmentType;
                        $arrParamsToCreateReminder['templateid'] = $lstTemplates;
                        $arrParamsToCreateReminder['email'] = strtolower($arrEachUserData->email);

                        $arrParamsToCreateReminder['phoneno'] = $reminderClass->formatphone($arrEachUserData->phoneNumber);
//                        $arrParamsToCreateReminder['phoneno'] = self::formatphone1($phone);
//                        $arrParamsToCreateReminder['phoneno'] = $this->formatphone($arrEachUserData->phoneNumber);

                        $arrParamsToCreateReminder['bodyemail'] = $taAppoinmentTemplateBody;
                        $arrParamsToCreateReminder['bodysms'] = $taAppoinmentTemplateBody;
                        $arrParamsToCreateReminder['languageid'] = $lstLanguage;
                        $arrParamsToCreateReminder['flagsms'] = $flagSMS;
                        $arrParamsToCreateReminder['smstimingid'] = $strSMSTimingIds;
                        $arrParamsToCreateReminder['flagcall'] = $flagCALL;
                        $arrParamsToCreateReminder['calltimingid'] = $strCALLTimingIds;
                        $arrParamsToCreateReminder['flagemail'] = $flagEMAIL;
                        $arrParamsToCreateReminder['emailtimingid'] = $strEMAILTimingIds;
                        $arrParamsToCreateReminder['isreschedule'] = $chkIsReschedule;
                        $arrParamsToCreateReminder['isprivate'] = $chkMakePrivate ?? 'N';
                        $arrParamsToCreateReminder['arrTimeSlot'] = $arrTimeSlot;
                        $arrParamsToCreateReminder['flagpromotional'] = 0;
                        $arrParamsToCreateReminder['flagsyncbacktoa1law'] = ($arrEachUserData->contactType == "A1LawOTS") ? 1 : 0;
//                        $arrParamsToCreateReminder['flagsyncbacktoa1law'] = $flagsyncbacktoa1law;
                        Reminder::addInReminder($arrParamsToCreateReminder);
                    }
                }
            }
            // if reminder id is greater than zero, then it means it is update case.
            else {
                $strResponseMessage = "Reminder Updated Succesfully";
                if (COUNT($arrUsersData) > 0) {

                    $arrUsersData = $arrUsersData[0];
                    $arrParamsToUpdateReminder = array();
                    $arrParamsToUpdateReminder['snsusersid'] = Auth::id();
                    $arrParamsToUpdateReminder['cardcode'] = $arrUsersData->cardCode;
                    $arrParamsToUpdateReminder['caseno'] = $arrUsersData->caseNo;
                    $arrParamsToUpdateReminder['contacttype'] = $arrUsersData->contactType;
                    $arrParamsToUpdateReminder['salutation'] = $arrUsersData->salutation;
                    $arrParamsToUpdateReminder['firstname'] = $arrUsersData->firstName;
                    $arrParamsToUpdateReminder['lastname'] = $arrUsersData->lastName;
                    $arrParamsToUpdateReminder['fullname'] = $arrUsersData->fullName;
                    $arrParamsToUpdateReminder['reminderdatetime'] = $reminderDateTime;
                    $arrParamsToUpdateReminder['title'] = $txtReminderTitle;
                    $arrParamsToUpdateReminder['purpose'] = $txtAppointmentType;
                    $arrParamsToUpdateReminder['templateid'] = $lstTemplates;
                    $arrParamsToUpdateReminder['email'] = strtolower($arrUsersData->email);

                    $arrParamsToUpdateReminder['phoneno'] = $reminderClass->formatphone($arrUsersData->phoneNumber);
                    //$arrParamsToUpdateReminder['phoneno'] = $arrUsersData->phoneNumber;
//                    $arrParamsToCreateReminder['phoneno'] = self::formatphone1($arrUsersData->phoneNumber);

                    $arrParamsToUpdateReminder['bodyemail'] = $taAppoinmentTemplateBody;
                    $arrParamsToUpdateReminder['bodysms'] = $taAppoinmentTemplateBody;
                    $arrParamsToUpdateReminder['languageid'] = $lstLanguage;
                    $arrParamsToUpdateReminder['flagsms'] = $flagSMS;
                    $arrParamsToUpdateReminder['smstimingid'] = $strSMSTimingIds;
                    $arrParamsToUpdateReminder['flagcall'] = $flagCALL;
                    $arrParamsToUpdateReminder['calltimingid'] = $strCALLTimingIds;
                    $arrParamsToUpdateReminder['flagemail'] = $flagEMAIL;
                    $arrParamsToUpdateReminder['emailtimingid'] = $strEMAILTimingIds;
                    $arrParamsToUpdateReminder['isreschedule'] = $chkIsReschedule;
                    $arrParamsToUpdateReminder['isprivate'] = $chkMakePrivate ?? 'N';
                    $arrParamsToUpdateReminder['arrTimeSlot'] = $arrTimeSlot;
                    $arrParamsToUpdateReminder['reminderId'] = $reminderId;
                    $arrParamsToUpdateReminder['flagpromotional'] = 0;
                    $arrParamsToUpdateReminder['flagsyncbacktoa1law'] = ($arrUsersData->contactType == "A1LawOTS") ? 1 : 0;
//                    $arrParamsToUpdateReminder['flagsyncbacktoa1law'] = $flagsyncbacktoa1law;
                    Reminder::updateInReminder($arrParamsToUpdateReminder);
                }
            }
        } catch (\Exception $e) {
            //  throw $e;
            DB::rollback();
            Session::flash('message', 'Oops something went wrong');
            return Redirect::to('/');
        }

        // If we reach here, then// data is valid and working.//
        DB::commit();

        Session::flash('message', $strResponseMessage);
        return Redirect::to('/');
    }

    public function getRemindersList(Request $request) {

        $arrFilterParams = array();
        $arrFilterParams['dateRangeFilter'] = $request->input('dateRangeFilter');
        $arrFilterParams['startDate'] = $request->input('startDate');
        $arrFilterParams['endDate'] = $request->input('endDate');
        $arrFilterParams['isPrivate'] = $request->input('isPrivate');
        $arrFilterParams['searchText'] = $request->input('searchText');
        $result = Reminder::getReminderDetailsForDashboard($arrFilterParams);
        $arrAdminSettings = AdminSetting::getInfo();
        //echo "<pre>"; print_r($result); exit();
        return Datatables::of($result)
                        ->addColumn('snsUserName', function($eachReminder) {

                            return $eachReminder->firstname;
                        })
                        ->editColumn('reminderDateTime', function($eachReminder) {

                            return $eachReminder->reminderdatetime;
                        })
                        ->addColumn('confirmDate', function($eachReminder) {

                            if (!empty($eachReminder->confirmdate)) {
                                return $eachReminder->confirmdate;
                            } else {
                                return "";
                            }
                        })
                        ->addColumn('twilioResponseColor', function($eachReminder) {
                            if (isset($eachReminder->color)) {
                                return $eachReminder->color;
                            } else {
                                $defaultColor = "#FFFFFF";

                                $objMasterReminder = new MasterReminderResponseColor();

                                $defaultColorObj = $objMasterReminder->getDefaultResponseColors();

                                if (!empty($defaultColorObj)) {
                                    $defaultColor = $defaultColorObj;
                                }

                                return $defaultColor;
                            }
                        })
                        ->make(true);
    }

    public function deleteReminders(Request $request) {

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'EMPTY',
            'message' => 'No reminders selected'
        );

        $arrReminderIds = $request->input('reminderIds');


        if (!empty($arrReminderIds)) {

            DB::beginTransaction();

            try {
                Reminder::deleteReminders($arrReminderIds);
            } catch (\Exception $e) {
                DB::rollback();
            }

            // If we reach here, then// data is valid and working.//
            DB::commit();

            $arrSendResponse = array(
                'statusCode' => 200,
                'data' => array(),
                'error' => 0,
                'flagMsg' => 'UPDATE',
                'message' => 'Selected reminder(s) deleted successfully.'
            );
        }

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    /* calaudit to snsweb */

    public static function explodeDataEventCategory($other1) {
        $skuList = explode(PHP_EOL, $other1);
        return $skuList;
    }

    public function calauditsns(Request $request) {
        try {
            //$arrAdminSettings = AdminSetting::getInfo();
            $reminderselectpoint = DB::select("select * from calaudit1 where reminder1 =1 and issync=0 ");
            //$reminderselectpoint = DB::select('select * from calaudit1 where reminder1 =1 and chglast > (select sccl_sync from sns_calaudit_cron_log limit 0,1) order by chglast ');
            //echo "<pre>"; print_r($reminderselectpoint); exit;
            // if (!empty($reminderselectpoint) && $arrAdminSettings->synca1law == 1) {
    //        echo  "<pre>";print_r(trim($reminderselectpoint[0]->other1));exit;
            if (!empty($reminderselectpoint)) {
                foreach ($reminderselectpoint as $keypoint => $remindervalue) {
                    //echo "<pre>"; print_r($remindervalue); 
    //                list($other1, $cat) = self::explodeDataEventCategory(trim($remindervalue->other1));
                    $other1cat = self::explodeDataEventCategory(trim($remindervalue->other1));
                    $other1 = $other1cat[0];
                    $findremindertempl = '';
    //                $findremindertempl = DB::table('master_reminder_template')->select('*')->where('purpose', '=', trim($remindervalue->other1))->first();
                    $findremindertempl = DB::table('master_reminder_template')->select('*')->where('purpose', '=', trim($other1))->first();
                    //echo "hi<pre>"; print_r($findremindertempl);exit;
                    if (!empty($findremindertempl) && isset($findremindertempl)) {
                        $snsuserexist = DB::table('users')->select('*')->where('snsusersunqid', '=', trim($remindervalue->initials0))->first();
                        if (!isset($snsuserexist) && empty($snsuserexist)) {
                            $user_create = User::create([
                                        'firstname' => (trim($remindervalue->first)) ?? $remindervalue->initials0,
                                        'lastname' => (trim($remindervalue->last)) ?? $remindervalue->initials0,
                                        'username' => $remindervalue->initials0,
                                        'snsusersunqid' => $remindervalue->initials0,
                                        'email' => trim($remindervalue->email),
                                        'password' => $remindervalue->initials0,
                                        'isconfirmed' => 1
                            ]);
                            if ($user_create) {
                                //$userdetails = DB::table('users')->select('*')->where('recid','=',$user_create)->get();
    //                            $remindervalue->snsusersid = $user_create;
                                $remindervalue->snsusersid = $user_create->recid;
                                $this->calaudit_addreminder($remindervalue, $findremindertempl);
                            }
                        } else {
                            $remindervalue->snsusersid = $snsuserexist->recid;
                            $this->calaudit_addreminder($remindervalue, $findremindertempl);
                        }
                    } else {
                        // store failure log due to mismatch templates
//                        self::storeTemplateFailureLog($remindervalue);
                    }
                    DB::update("update calaudit1 set issync ='1' where srno='" . $remindervalue->srno . "' ");
                }
                echo "Data Migrated successfully";
                // DB::update("update sns_calaudit_cron_log set sccl_sync ='" . date('Y-m-d h:i:s') . "' ");
            } else {
                echo "No Data to Synchronize";
            }
        } catch (Exception $ex) {
//            self::storeExceptionFailureLog($ex);
        }
    }
    
    /*
     * failure log of template mismatch
     * 
     */
    public function storeTemplateFailureLog($remindervalue) {
        $other1cat = self::explodeDataEventCategory(trim($remindervalue->other1));
        $other1 = $other1cat[0];
        $details = config('constants.failurelog.template_mismatch');
        $data = [
            'failure_type' => $details['title'],
            'failure_type_id' => $details['id'],
            'short_description' => str_replace('#TEMPLATE_NAME#', $other1, $details['short_description']),
            'description' => SELF::generateDetailDescriptionForTemplateFailureLog($remindervalue),
            'issyncflag' => '1',
            'flagneedtosync' => '1'
        ];
        FailureController::storeFailureLog($data);
        return true;
    }
    
    public function generateDetailDescriptionForTemplateFailureLog($remindervalue) {
        $other1cat = self::explodeDataEventCategory(trim($remindervalue->other1));
        $other1 = $other1cat[0];
        $template = $remindervalue->srno . PHP_EOL;
        $template .= trim($other1) . PHP_EOL . PHP_EOL;
        $template .= 'Template does not match with SNS templates';
        return $template;
    }
    
    /**
     * When exception occurs in migrating from calaudit1 to remidner template log this as failure and store in  table
     * **/
    public function storeExceptionFailureLog($exception) {
        $details = config('constants.failurelog.calaudit_to_reminder_exception');
        $data = [
            'failure_type' => $details['title'],
            'failure_type_id' => $details['id'],
            'short_description' => $details['short_description'],
            'description' => $exception,
            'issyncflag' => '0',
            'flagneedtosync' => '0'
        ];
        FailureController::storeFailureLog($data);
        return true;
    }

    public function calaudit_addreminder($remindervalue, $findremindertempl) {
//        list($other1, $cat) = self::explodeDataEventCategory(trim($remindervalue->other1));
        $other1cat = self::explodeDataEventCategory(trim($remindervalue->other1));
        $other1 = $other1cat[0];
        $cat = $other1cat[1] ?? 1;
        $arrParamsToCreateReminder = array();
        $arrParamsToCreateReminder['snsusersid'] = $remindervalue->snsusersid;
        $arrParamsToCreateReminder['caseno'] = $remindervalue->caseno;
        $arrParamsToCreateReminder['cardcode'] = '';
        $arrParamsToUpdateReminder['salutation'] = '';
        $arrParamsToCreateReminder['contacttype'] = 'A1Law';
        $arrParamsToCreateReminder['firstname'] = trim($remindervalue->first);
        $arrParamsToCreateReminder['lastname'] = trim($remindervalue->last);
        $arrParamsToCreateReminder['fullname'] = trim($remindervalue->first) . ' ' . trim($remindervalue->last);
        $arrParamsToCreateReminder['reminderdatetime'] = $remindervalue->date;
//        $arrParamsToCreateReminder['title'] = $remindervalue->other1;
//        $arrParamsToCreateReminder['purpose'] = $remindervalue->other1;
        $arrParamsToCreateReminder['title'] = $other1;
        $arrParamsToCreateReminder['purpose'] = $other1;
        $arrParamsToCreateReminder['calender_val'] = $cat;
        $arrParamsToCreateReminder['templateid'] = $findremindertempl->recid;
        $arrParamsToCreateReminder['email'] = $this->formatemail($remindervalue->email);
        if (trim($remindervalue->phonehome) != '') {
            $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonehome);
        } elseif (trim($remindervalue->phonebus) != '') {
            $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonebus);
        } elseif (trim($remindervalue->phonefax) != '') {
            $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonefax);
        } elseif (trim($remindervalue->phonecar) != '') {
            $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonecar);
        } else {
            $arrParamsToCreateReminder['phoneno'] = '';
        }
        $arrTimeSlot = array();
        if (isset($findremindertempl->smstimingid) && !empty($findremindertempl->smstimingid)) {
            $arrTimeSlot['SMS'] = (is_array($findremindertempl->smstimingid)) ? array($findremindertempl->smstimingid) : explode(',', $findremindertempl->smstimingid);
        }
        if (isset($findremindertempl->calltimingid) && !empty($findremindertempl->calltimingid)) {
            $arrTimeSlot['CALL'] = (is_array($findremindertempl->calltimingid)) ? array($findremindertempl->calltimingid) : explode(',', $findremindertempl->calltimingid) ;
        }
        if (isset($findremindertempl->emailtimingid) && !empty($findremindertempl->emailtimingid)) {
            $arrTimeSlot['EMAIL'] = (is_array($findremindertempl->emailtimingid)) ? array($findremindertempl->emailtimingid) : explode(',', $findremindertempl->emailtimingid);
        }

        $flagSMS = "N";
        $strSMSTimingIds = "";
        if (isset($arrTimeSlot['SMS']) && !empty($arrTimeSlot['SMS'])) {
            $flagSMS = "Y";
            //$strSMSTimingIds = implode(',', $arrTimeSlot['SMS']);
        }
        $flagCALL = "N";
        $strCALLTimingIds = "";
        if (isset($arrTimeSlot['CALL']) && !empty($arrTimeSlot['CALL'])) {
            $flagCALL = "Y";
            //$strCALLTimingIds = implode(',', $arrTimeSlot['CALL']);
        }
        $flagEMAIL = "N";
        $strEMAILTimingIds = "";
        if (isset($arrTimeSlot['EMAIL']) && !empty($arrTimeSlot['EMAIL'])) {
            $flagEMAIL = "Y";
            //$strEMAILTimingIds = implode(',', $arrTimeSlot['EMAIL']);
        }

        $arrParamsToCreateReminder['bodyemail'] = $findremindertempl->bodyemail;
        $arrParamsToCreateReminder['bodysms'] = $findremindertempl->bodysms;
        $arrParamsToCreateReminder['languageid'] = $findremindertempl->language;
        $arrParamsToCreateReminder['flagsms'] = $flagSMS;
        $arrParamsToCreateReminder['smstimingid'] = $findremindertempl->smstimingid;
        $arrParamsToCreateReminder['flagcall'] = $flagCALL;
        $arrParamsToCreateReminder['calltimingid'] = $findremindertempl->calltimingid;
        $arrParamsToCreateReminder['flagemail'] = $flagEMAIL;
        $arrParamsToCreateReminder['emailtimingid'] = $findremindertempl->emailtimingid;
        $arrParamsToCreateReminder['isreschedule'] = $findremindertempl->isreschedule;
        $arrParamsToCreateReminder['isprivate'] = 'N';
        $arrParamsToCreateReminder['arrTimeSlot'] = $arrTimeSlot;
        $arrParamsToCreateReminder['flagpromotional'] = 0;
        $arrParamsToCreateReminder['isactive'] = "A";
        Reminder::addInReminder($arrParamsToCreateReminder);
    }

    public function isdone(Request $request) {

        $id = $request->id;
        if ($id != 0) {
            $result = DB::update("update sns_reminder set isdone ='1' where recid='" . $id . "'");
        } else {
            $ids = "'" . implode("','", $request->checkids) . "'";
            $result = DB::update("update sns_reminder set isdone ='1' where recid in($ids)");
        }
        if ($result) {
            die(json_encode($result));
        } else {
            die(json_encode($result));
        }
        exit;
    }

    public function formatphone($number1) {
        $number2 = trim($number1);
        $number = preg_replace('#[^0-9+]#', '', $number2);
        if (ctype_digit($number) && strlen($number) == 10) {
            $number = '+1-' . $number;
            return $number;
        } else {
            return $number = $number2;
        }
    }
    
    public function formatemail($email1) {
        $email2 = strtolower(trim($email1));
        $result = filter_var( $email2, FILTER_VALIDATE_EMAIL );
        if($result) {
            return $email2;
        } else {
            return "";
        }
    }
    
    public static function formatphone1($number1) {
        $number2 = trim($number1);
        $number = preg_replace('#[^0-9+]#', '', $number2);
        if (ctype_digit($number) && strlen($number) == 10) {
            $number = '+1-' . $number;
            return $number;
        } else {
            return $number = $number2;
        }
    }

    /**
     * sync staff details to the users tables 
     * date: 15-03-2018
     * @param Request $request
     */
    public function syncStaffDetailsToUsers(Request $request) {
        $staffDetails = DB::select("select * from staff where isSync=0");
        if (!empty($staffDetails)) {
            foreach ($staffDetails as $keypoint => $staffVal) {
                // check if user  exists or not!!!  
                $snsuserexist = DB::table('users')->select('*')->where([
                            ['snsusersunqid', '=', trim($staffVal->initials)],
                            ['username', trim($staffVal->username)]
                        ])->first();

                // create user objecct to insert/update if  exists
                $userObject = array(
                    'snsusersunqid' => trim($staffVal->initials),
                    'role' => 'users',
                    'username' => trim($staffVal->username),
                    'password' => (!empty(trim($staffVal->password)) ? trim($staffVal->password) : trim($staffVal->username)),
                    'firstname' => trim($staffVal->fname),
                    'lastname' => trim($staffVal->lname),
                    'email' => '',
                    'lawwarelogin' => 0,
                    'isactive' => 'A',
                    'isdeleted' => 0,
                    'isconfirmed' => 1,
                    'phonenumber' => ''
                );
                $suplicateUpdate = array('snsusersunqid' => trim($staffVal->initials), 'username' => trim($staffVal->username));

                User::updateOrCreate($suplicateUpdate, $userObject);
//                if (isset($snsuserexist) && !empty($snsuserexist)) {
//                    // update users data 
//                    $result = DB::table('users')
//                            ->where([
//                                ['snsusersunqid', trim($staffVal->initials)],
//                                ['username', trim($staffVal->username)]
//                            ])
//                            ->update($userObject);
//                } else {
//                    $result = User::create($userObject);
//                }
                DB::update("update staff set isSync = '1' where initials='" . trim($staffVal->initials) . "' ");
            }
        } else {
            echo "No Data to Synchronize";
        }
    }

    public function updateTwilioStatusFromTheServer($reminderId, $twilioStatus, $confirmdate) {
        $array['reminderId'] = $reminderId;
        $array['twilioStatus'] = $twilioStatus;
        $array['confirmdate'] = $confirmdate;
        Reminder::updateTwilioStatusFromTheServer($array);
    }

    public function fetchDefaultTemplates() {
        try {
            $data = DB::select("SELECT * FROM tbl_apptoto_addsmsemail");
            foreach ($data as $key => $val) {
                // insert process for the sns_reminder table.
    //            echo "<pre>";print_r($val->EmailSubject);exit;
                $masterObj = (new MasterTemplates);
                $whenSMS = $masterObj->getSmstimingidAttribute($val->whenSMS);
                $whenCALL = $masterObj->getCalltimingidAttribute($val->whenCALL);
                $whenEMAIL = $masterObj->getEmailtimingidAttribute($val->whenEMAIL);
                $masterTemplateObj = array(
                    'recid' => $val->ID,
                    'title' => trim($val->Name),
                    'purpose' => (!empty(trim($val->Purpose))) ? trim($val->Purpose) : trim($val->Name),
                    'bodyemail' => trim($val->Body),
                    'bodysms' => $val->EmailSubject,
                    'language' => ($val->TwilioLang + 1),
                    'flagsms' => ($val->isSMS ? 'Y' : 'N'),
                    'smstimingid' => $whenSMS,
                    'flagcall' => ($val->isCALL ? 'Y' : 'N'),
                    'calltimingid' => $whenCALL,
                    'flagemail' => ($val->isEMAIL ? 'Y' : 'N'),
                    'emailtimingid' => $whenEMAIL,
                    'isreschedule' => ($val->IsReschedule ? 'Y' : 'N'),
                    'isactive' => 'A',
                    'isdeleted' => 0
                );
                $suplicateUpdate = array('recid' => $val->ID);
                MasterTemplates::updateOrCreate($suplicateUpdate, $masterTemplateObj);
                // insert process for the sns_reminder table.
            }
            echo "Templates imported successfully.";
        
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * Migrate reminders from windows dummy table to web reminder table
     * @date: 25-04-2018
     * 
     * **/
    public function syncReminderFromWindowsToWeb(Request $request) {
        try {
//            $winreminder = DB::select("SELECT distinct(r.eventno), r.*, fd.phone as PhoneNumber, fd.email as emailaddress FROM tbl_lrs_reminder as r LEFT JOIN tbl_finalreminderdetails as fd ON fd.MainEventId = r.eventno WHERE issync='0'");
            $winreminder = DB::select("SELECT r.*, '' as PhoneNumber, '' as emailaddress FROM tbl_lrs_reminder as r WHERE issync='0'");
            if(!empty($winreminder)) {
                foreach($winreminder as $keypoint=>$valpoint) {
//                    echo "<pre>";print_r($valpoint);exit;
                    self::createRemindersFromWindowsReminderToWebReminder($valpoint);
                    // code here to migrate reminders from windows to web application.
                    DB::update("update tbl_lrs_reminder set issync = '1' where eventno = '" . $valpoint->eventno . "' ");
                }
            }
            echo "One time reminder data migrate from windows to web reminder table is done";
        } catch (Exception $ex) {
            self::storeExceptionFailureLog($ex);
        }
    }
    
    public static function getTemplateIdFromTemplateName($tempName) {
        $templateName = trim($tempName);
        $record = MasterTemplates::where('title', $templateName)
                    ->orwhere('purpose', $templateName)->first();
        return $record;
    }
    
    public static function checkSNSUserDetails($windowuser) {
        $user = trim($windowuser);
        $record = User::where('snsusersunqid', $user)
                ->orwhere('win_user_id', $user)->first();
        return $record;
    }
    
    public static function getCardCodeValues($cardcodeValue) {
        $returnCardcodeVal = array('cardcode' => '', 'contacttype' => 'SNS');
        if(!empty($cardcodeValue)) {
            if(strtolower($cardcodeValue) == "a1law") {
                $returnCardcodeVal = array('cardcode' => 'A1Law', 'contacttype' => 'A1Law');
            } else if(is_numeric($cardcodeValue)) {
                $returnCardcodeVal = array('cardcode' => $cardcodeValue, 'contacttype' => 'OTS');
            } else {
                $result = self::getContactDetails($reminderdata);
                if(!empty($result)) {
                    $returnCardcodeVal = array('cardcode' => $result->cardcode, 'contacttype' => 'SNS');
                } else {
                   $returnCardcodeVal = array('cardcode' => $cardcodeValue, 'contacttype' => 'SNS');
                }
            }
        }
        return $returnCardcodeVal;
    }
    
    public static function getCaseNoValues($reminderdata) {
        $caseno = $reminderdata->Caseno;
        $returnCaseno = "";
        if(!empty($caseno)) {
            if(strtolower($caseno) == 'a1law') {
                $returnCaseno = 'A1Law';
            } else if(is_numeric($caseno)) {
                $returnCaseno = $caseno;
            } else {
                $result = self::getContactDetails($reminderdata);
                if(!empty($result)) {
                    $returnCaseno = $result->case;
                } else {
                   $returnCaseno = $caseno;
                }
            }
        }
        return $returnCaseno;
    }
    
    public static function getContactDetails($objdata) {
        $result = \App\Contact::where([
            ['firstname', $objdata->first],
            ['lastname', $objdata->last]
                ])->first();
        return $result;
    }
    
    public static function changetimingslots($timing) {
        $times = explode(',', $timing);
        if(!empty($times)) {
            $val = array();
            foreach($times as $k=>$v) {
                if($v == '0') {
                    $v = '1';
                }
                $val[] = $v;
            }
        }
        $finalTimings = implode(',', $val);
        return $finalTimings;
    }
    
    public static function getTimeSlotArray($data) {
        $whenSMS = self::changetimingslots($data->whenSMS);
        $whenCALL = self::changetimingslots($data->whenCALL);
        $whenEMAIL = self::changetimingslots($data->whenEMAIL);
        if (isset($whenEMAIL) && !empty($whenSMS)) {
            $arrTimeSlot['SMS'] = (!is_array($whenSMS)) ? explode(',', $whenSMS) : array($whenSMS);
        }
        if (isset($whenCALL) && !empty($whenCALL)) {
            $arrTimeSlot['CALL'] = (!is_array($whenCALL)) ? explode(',', $whenCALL) : array($whenCALL);
        }
        if (isset($whenEMAIL) && !empty($whenEMAIL)) {
            $arrTimeSlot['EMAIL'] = (!is_array($whenEMAIL)) ? explode(',', $whenEMAIL) : array($whenEMAIL);
        }
        return $arrTimeSlot;
    }
    /**
     * sync/create reminder from windows to web sns
     * **/
    public static function createRemindersFromWindowsReminderToWebReminder($data=array()) {
        if($data) {
//            echo "<pre>";print_r($data);exit;
            $reminderClass = new ReminderController();
            
            $templateDetails = self::getTemplateIdFromTemplateName($data->location);
            $templateId = 0;
            $snsuserid = 1;
            if(isset($templateDetails) && !empty($templateDetails)) {
                $templateId  = $templateDetails->recid;
            }
            $snsuserdetails = self::checkSNSUserDetails($data->CreatedUserFK);
            if(isset($snsuserdetails) && !empty($snsuserdetails)) {
                $snsuserid = $snsuserdetails->recid;
            }
            // get cardcode and contact type return values
            $cardcodecontacttype = self::getCardCodeValues($data->CardcodeFK);
            $cardcode = $cardcodecontacttype['cardcode'] ?? '';
            $contacttype = $cardcodecontacttype['contacttype'] ?? 'SNS';
            
            // get reminder date time format
            $date = Carbon::parse($data->date);
            $reminderDateTime = $date->format('Y-m-d H:i:s');
            
            $createDate = Carbon::parse($data->CreatedDate);
            $createdDateTime = $createDate->format('Y-m-d H:i:s');
            
            $updatedDate = Carbon::parse($data->ModifiedDate);
            $updatedDateTime = $updatedDate->format('Y-m-d H:i:s');
//            echo "<pre>";print_r($reminderDateTime);exit;
            $event = self::explodePurposeAndBody($data->event);
            $remidner = new Reminder();
            $remidner->snsusersid = $snsuserid;
            $remidner->cardcode = $cardcode;
            $remidner->caseno = self::getCaseNoValues($data);
            $remidner->contacttype = $contacttype;
//            $remidner->flagsyncbacktoa1law = '0';
            $remidner->salutation = '';
            $remidner->firstname = trim($data->first);
            $remidner->lastname = trim($data->last);
            $remidner->fullname = trim($data->first) . " " . trim($data->last);
            $remidner->reminderdatetime = $reminderDateTime;
            $remidner->title = trim($data->location);
            $remidner->purpose = trim($data->location);
            $remidner->templateid = $templateId;
            $remidner->email = trim($data->emailaddress);
            $remidner->phoneno = $reminderClass->formatphone($data->PhoneNumber);
            $remidner->bodyemail = $event;
            $remidner->bodysms = $event;
            $remidner->languageid = ($data->TwilioLang) + 1;
            $remidner->flagsms = $data->SMS ? 'Y' : 'N';
            $remidner->smstimingid = self::changetimingslots($data->whenSMS);
            $remidner->flagcall = $data->CALL ? 'Y' : 'N';
            $remidner->calltimingid = self::changetimingslots($data->whenCALL);
            $remidner->flagemail = $data->EMAIL ? 'Y' : 'N';
            $remidner->emailtimingid = self::changetimingslots($data->whenEMAIL);
            $remidner->isreschedule = '';
            $remidner->isprivate = $data->IsPrivate ? 'Y' : 'N';
            $remidner->twiliostatus = $data->LastUpdatedStatusFK;
            $remidner->flagneedtosyncwithserveragain = 'N';
            $remidner->flagrecall = '';
            $remidner->isactive = '1';
            $remidner->createddate = $createdDateTime;
            $remidner->updateddate = $updatedDateTime;
            $remidner->isdeleted = '0';
            $remidner->confirmdate = '';
            $remidner->flagpromotional = '0';
            $remidner->isdone = '0';
            $remidner->calender_val = '1';
            $remidner->color_val = '1';
            $remidner->isnotify = '';
            $remidner->syncfromwindows = '1';
            $remidner->save();
            
            self::importReminderTimingsFromWindowsToWeb($remidner, $data);
//            $arrTimeSlot = self::getTimeSlotArray($data);
//            $remidnerArray = array(
//                'snsusersid' => $snsuserid,
//                'cardcode' => $cardcode,
//                'caseno' => self::getCaseNoValues($data),
//                'contacttype' => $contacttype,
//                'flagsyncbacktoa1law' => '0',
//                'salutation' => '',
//                'firstname' => trim($data->first),
//                'lastname' => trim($data->last),
//                'fullname' => trim($data->first) . " " . trim($data->last), 
//                'reminderdatetime' => $reminderDateTime,
//                'title' => trim($data->location),
//                'purpose' => trim($data->location),
//                'templateid' => $templateId,
//                'email' => trim($data->emailaddress),
//                'phoneno' => $reminderClass->formatphone($data->PhoneNumber),
//                'bodyemail' => $data->event,
//                'bodysms' => $data->event,
//                'languageid' => ($data->TwilioLang) + 1,
//                'flagsms' => $data->SMS ? 'Y' : 'N',
//                'smstimingid' => self::changetimingslots($data->whenSMS),
//                'flagcall' => $data->CALL ? 'Y' : 'N',
//                'calltimingid' => self::changetimingslots($data->whenCALL),
//                'flagemail' => $data->EMAIL ? 'Y' : 'N',
//                'emailtimingid' => self::changetimingslots($data->whenEMAIL),
//                'isreschedule' => '',
//                'isprivate' => $data->IsPrivate ? 'Y' : 'N',
//                'twiliostatus' => $data->LastUpdatedStatusFK,
//                'flagneedtosyncwithserveragain' => 'N',
//                'flagrecall' => '',
//                'isactive' => '1',
//                'createddate' => $createdDateTime,
//                'updateddate' => $updatedDateTime,
//                'isdeleted' => '0',
//                'confirmdate' => '',
//                'flagpromotional' => '0',
//                'isdone' => '0',
//                'calender_val' => '1',
//                'color_val' => '1',
//                'isnotify' => '',
//                'arrTimeSlot' => $arrTimeSlot
//            );
//            
//            $remidnerDetails = Reminder::addInReminder($remidnerArray);
        } else {
            echo "No records are pending for migration";
        }
    }
    
    public static function importReminderTimingsFromWindowsToWeb($remidnerdata, $originalData) {
        $arrAdminSettings = AdminSetting::getInfo();
//        echo "<pre>";print_r($arrAdminSettings);
//        echo "<pre>";print_r($remidnerdata);
//        echo "<pre>";print_r($originalData);exit;
        $reminderClass = new ReminderController();
        // store data in remidner timings
        $reminderTimings = DB::select("SELECT * FROM tbl_lrs_reminder_timing WHERE EventFK = '" . $originalData->eventno . "'");
//        echo "<pre>";print_r($reminderTimings);exit;
        foreach($reminderTimings as $rkey=>$rval) {
            // get the exploded records for values and unit
            list($value, $unit) = explode(' ',$rval->ReminderWhen);
            
            // save timings into remidner_timing  table
            $reminderTimings = new \App\ReminderTiming();
//            $reminderTimings->reminderid = $rval->EventFK;
            $reminderTimings->reminderid = $remidnerdata->recid;
            $reminderTimings->value = $value;
            $reminderTimings->unit = $unit;
            $reminderTimings->typex = $rval->ReminderType;
            $reminderTimings->isactive = $rval->Isactive ? 'A' : 'I';
            $reminderTimings->createddate = $rval->Createddatetime;
            $reminderTimings->updateddate = $rval->Createddatetime;
            $reminderTimings->isdeleted = '0';
            $reminderTimings->save();
            // save timings into remidner_timing  table

            // save reminder sync record in  reminder synctables
            $timingId = $reminderTimings->recid;
            $remindertimings = DB::select("SELECT * FROM tbl_finalreminderdetails WHERE EventID = '" . $rval->TimingID . "'");
//            echo "<pre>";print_r($remindertimings);exit;
            $phoneNumber = '';
            $emailAddress = '';
            foreach($remindertimings as $remindertimingskey=>$remindertimingsval) {
                
                if($remindertimingsval->SMSStatus) {
                    $reminderType = "SMS";
                } else if($remindertimingsval->EmailStatus) {
                    $reminderType = "EMAIL";
                } else if($remindertimingsval->CallStatus) {
                    $reminderType = "CALL";
                }
                
                // generate valued reminder sync data.
//                $bodymail = self::explodePurposeAndBody($remidnerdata->bodyemail);
//                echo "<pre>";print_r($bodymail);exit;
                $arrParamsForTemplateBodyReplace = array();
                $arrParamsForTemplateBodyReplace['strTemplateBody'] = $remidnerdata->bodyemail;
                $arrParamsForTemplateBodyReplace['reminderId'] = $remidnerdata->recid;
                $arrParamsForTemplateBodyReplace['typeX'] = $reminderType;

                $strTemplateBody = \App\ReminderSync::replaceInsertFieldsFromTemplateBody($arrParamsForTemplateBodyReplace);
                // generate valued reminder sync data.
                $phoneNumber = $reminderClass->formatphone($remindertimingsval->Phone);
                $emailAddress = $remindertimingsval->Email;
                
                $remindersync = new \App\ReminderSync();
                $remindersync->snsusersid = $remidnerdata->snsusersid;
                $remindersync->clientkey = $arrAdminSettings->clientkey;
                $remindersync->reminderid = $remidnerdata->recid;
                $remindersync->remindertimingid = $timingId;
                $remindersync->languageid = $remidnerdata->languageid;
                $remindersync->title = $remidnerdata->title;
                $remindersync->purpose = $remidnerdata->purpose;
//                $remindersync->bodyemail = $remidnerdata->bodyemail;    // convert the variables to values
                $remindersync->bodyemail = $strTemplateBody;    // convert the variables to values
//                $remindersync->bodysms = $remidnerdata->bodysms;    // convert the variables to values
                $remindersync->bodysms = $strTemplateBody;    // convert the variables to values
                $remindersync->userphone = $phoneNumber;
                $remindersync->useremail = $emailAddress;
                $remindersync->remindereventdatetime = $remindertimingsval->EventDatetime;
                $remindersync->appointmentdatetime = $remindertimingsval->AppointmentDatetime;
                $remindersync->remindertype = $reminderType;
//                $remindersync->flagscyncserver = self::flagSyncToServer($remindertimingsval);
                $remindersync->flagscyncserver = 'Y';
                $remindersync->twiliostatus = $remidnerdata->twiliostatus;
                $remindersync->flagdisablecancelreschedule = 'N';
                $remindersync->flagpromotional = '0';
                $remindersync->recalltime = 'N';
                $remindersync->recallinterval = '0';
                $remindersync->recallcalltime = '0';
                $remindersync->reminderrecallid = '0';
                $remindersync->isactive = $remindertimingsval->IsStatus ? 'A' : 'I';
                $remindersync->createddate = $remindertimingsval->CreatedDttm;
                $remindersync->updateddate = $remindertimingsval->ModifiedDttm;
                $remindersync->isdeleted = $remidnerdata->isdeleted;
                $remindersync->twilioevents = '0';
                $remindersync->confirmdate = $remindertimingsval->ModifiedDttm;
                $remindersync->sid = $remindertimingsval->SID;
                $remindersync->syncfromwindows = '1';

                $remindersync->save();
                // save reminder sync record in  reminder synctables
    //            echo "<pre>";print_r($remindertimings);exit;
            }
        }
        // update remidner table for phone and email 
            DB::table('reminder')
                    ->where('recid', $remidnerdata->recid)
                    ->update([
                        'phoneno' => $phoneNumber,
                        'email' => $emailAddress
                    ]);
//        self::insertIntoReminderTimingRecords($remidnerdata);
//        $remindertimings = DB::select("SELECT * FROM tbl_finalreminderdetails WHERE MainEventId = '" . $originalData->eventno . "'");
//        echo "<pre>";print_r($remindertimings);exit;
        
//        echo "<pre>";print_r($remindertimings);exit;
    }
    
    
    public static function explodePurposeAndBody($bodyMail) {
        $body = explode(":", $bodyMail);
        array_shift($body);
        $finalBody = implode(" ", $body);
        return $finalBody;
    }
    
    public static function flagSyncToServer($remidnersyncval) {
//        echo "<pre>";print_r($remidnersyncval);exit;
        $datetimenow = Carbon::now()->format('Y-m-d H:i:s');
//        echo "<pre>";print_r($datetimenow);exit;
        $reminderdatetime = $remidnersyncval->EventDatetime;
        if($datetimenow > $reminderdatetime) {
            $flagSync = 'Y';
        } else  {
            $flagSync = 'N';
        }
        return $flagSync;
    }
    
    
    /**
     * Migrate reminders from windows dummy table to web reminder table
     * @date: 25-04-2018
     * 
     * **/
    public function syncReminderTimingFromWindowsToWeb_remove(Request $request) {
        try {
            
//            $winreminder = DB::select("SELECT distinct(r.eventno), r.*, fd.phone as PhoneNumber, fd.email as emailaddress FROM tbl_lrs_reminder as r LEFT JOIN tbl_finalreminderdetails as fd ON fd.MainEventId = r.eventno WHERE issync='0'");
            $winreminder = DB::select("SELECT r.*, '' as PhoneNumber, '' as emailaddress FROM tbl_lrs_reminder as r WHERE issync='0'");
            if(!empty($winreminder)) {
                foreach($winreminder as $keypoint=>$valpoint) {
//                    echo "<pre>";print_r($valpoint);exit;
                    self::createRemindersFromWindowsReminderToWebReminder($valpoint);
                    // code here to migrate reminders from windows to web application.
                    DB::update("update tbl_lrs_reminder set issync = '1' where eventno = '" . $valpoint->eventno . "' ");
                }
            }
            echo "One time reminder data migrate from windows to web reminder table is done";
        } catch (Exception $ex) {
            self::storeExceptionFailureLog($ex);
        }
    }

}
