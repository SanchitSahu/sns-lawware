<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use File;
use App\ReminderSync;
use App\Reminder;
use App\AdminSetting;
use App\Http\Controllers\DebugMailSend;
use Illuminate\Support\Facades\Redirect;
use Mail;

class SendRequestToServer extends Controller {

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('checkTwilioServerConnected');
    }

    public function setClientRequestData() {

        $arrAdminSettings = AdminSetting::getInfo();

        $this->client = new Client([
            // Base URI is used with relative requests
            // 'base_uri' => 'http://52.66.93.186/twiliosns/',

            'base_uri' => 'http://35.165.166.7/twiliosns/',

            //'base_uri' => 'http://35.165.166.7/twiliosns/',

            //'base_uri' => 'http://getsynergyweb.com/twiliosns/',
//            'base_uri' => 'http://tccsnsweb.com:8081/twiliosns/',
            // 'base_uri' => 'http://localhost/twiliosns/',
            'headers' => [
                'clientKey' => $arrAdminSettings->clientkey
            ]
                // You can set any number of default request options.
                // 'timeout'  => 2.0,
        ]);
    }

    public function syncLatestRemindersToServer() {

        $this->setClientRequestData();

        $arrRemindersToUpdate = ReminderSync::getRemindersToUpdateOnServer();

        $arrDebugMailParams = array();
        $arrDebugMailParams['subject'] = 'Reminders to cloud for sync';
        $arrDebugMailParams['arrData'] = $arrRemindersToUpdate;
        DebugMailSend::sendDebugMail($arrDebugMailParams);


        // if any reminders are present for update
        if (COUNT($arrRemindersToUpdate) > 0) {

            if ((isset($arrRemindersToUpdate['arrResultNotSynced']) && empty($arrRemindersToUpdate['arrResultNotSynced'])) &&
                    (isset($arrRemindersToUpdate['arrResultNeedToUpdate']) && empty($arrRemindersToUpdate['arrResultNeedToUpdate'])) &&
                    (isset($arrRemindersToUpdate['arrResultNeedToDelte']) && empty($arrRemindersToUpdate['arrResultNeedToDelte']))) {
                
            } else {
                try {

                    $arrRequestDataToPost = array();
                    $arrRequestDataToPost['arrResultNotSynced'] = $arrRemindersToUpdate['arrResultNotSynced'];
                    $arrRequestDataToPost['arrResultNeedToUpdate'] = $arrRemindersToUpdate['arrResultNeedToUpdate'];
                    $arrRequestDataToPost['arrResultNeedToDelte'] = $arrRemindersToUpdate['arrResultNeedToDelte'];

                    $reponse = $this->client->request('POST', 'addRemindersToCloud', [
                        'form_params' => ['arrRequestData' => $arrRequestDataToPost]
                    ]);

                    $status = $reponse->getStatusCode();

                    $header = $reponse->getHeader('content-type');

                    $body = $reponse->getBody();

                    $strBody = (string) $body;

                    $arrBody = json_decode($strBody, true);

                    if (!isset($arrBody['error']) || !isset($arrBody['flagMsg']) || !isset($arrBody['message']) || !isset($arrBody['data'])) {
                        
                    } else {
                        $error = $arrBody['error'];
                        $flagMsg = $arrBody['flagMsg'];
                        $message = $arrBody['message'];
                        $data = $arrBody['data'];
                        // no error occured

                        $arrDebugMailParams = array();
                        $arrDebugMailParams['subject'] = 'Response from Reminders to cloud for sync';
                        $arrDebugMailParams['arrData'] = $data;
                        DebugMailSend::sendDebugMail($arrDebugMailParams);
                        if ($error == 0) {
                            switch ($flagMsg) {
                                case "INSERTED":
                                    if (isset($data) && !empty($data['arrResponse'])) {

                                        $arrResponseData = $data['arrResponse'];

                                        // classifying response from the server.
                                        $arrReminderAndSyncIdInserted = $arrResponseData['arrReminderAndSyncIdInserted'];
                                        $arrReminderIdsToUpdate = $arrResponseData['arrReminderIdsToUpdate'];
                                        $arrReminderIdsToDelete = $arrResponseData['arrReminderIdsToDelete'];

                                        // processing for all the reminders that added into the server.
                                        $arrRemindersSyncedWithServer = array();

                                        foreach ($arrReminderAndSyncIdInserted AS $key => $arrEachReminderForUpdate) {
                                            $reminderSyncId = $arrEachReminderForUpdate['reminderSyncId'];
                                            $reminderId = $arrEachReminderForUpdate['reminderId'];

                                            array_push($arrRemindersSyncedWithServer, $reminderSyncId);
                                        }

                                        $arrRemindersSyncedWithServer = array_unique($arrRemindersSyncedWithServer);

                                        if (!empty($arrRemindersSyncedWithServer)) {
                                            ReminderSync::updateRemindersFlagServerSync($arrRemindersSyncedWithServer);
                                        }

                                        // processing for all the reminders that are updated on the server.
                                        if (!empty($arrReminderIdsToUpdate)) {
                                            Reminder::updateFlagForDoNotSyncWithServerAgain($arrReminderIdsToUpdate);
                                        }

                                        // processing for all the reminders that are deleted on the server.
                                        if (!empty($arrReminderIdsToDelete)) {
                                            Reminder::updateFlagForDoNotSyncWithServerAgain($arrReminderIdsToUpdate);
                                        }


                                        /// function call to check if any reminders pending to upload on the cloud.
                                        $arrRemindersToUpdateCheck = ReminderSync::getRemindersToUpdateOnServer();

                                        // if any reminders left to sync on cloud. If so call this function again.
                                        if ((isset($arrRemindersToUpdateCheck['arrResultNotSynced']) && !empty($arrRemindersToUpdateCheck['arrResultNotSynced'])) ||
                                                (isset($arrRemindersToUpdateCheck['arrResultNeedToUpdate']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToUpdate'])) ||
                                                (isset($arrRemindersToUpdateCheck['arrResultNeedToDelte']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToDelte']))) {

                                            if (isset($arrRemindersToUpdateCheck['arrResultNotSynced']) && !empty($arrRemindersToUpdateCheck['arrResultNotSynced'])) {
                                                ReminderSync::updateRemindersFlagServerSync($arrRemindersToUpdateCheck['arrResultNotSynced']);
                                            }

                                            // processing for all the reminders that are updated on the server.
                                            if (isset($arrRemindersToUpdateCheck['arrResultNeedToUpdate']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToUpdate'])) {

                                                Reminder::updateFlagForDoNotSyncWithServerAgain($arrRemindersToUpdateCheck['arrResultNeedToUpdate']);
                                            }

                                            // processing for all the reminders that are deleted on the server.
                                            if (isset($arrRemindersToUpdateCheck['arrResultNeedToDelte']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToDelte'])) {
                                                Reminder::updateFlagForDoNotSyncWithServerAgain($arrRemindersToUpdateCheck['arrResultNeedToDelte']);
                                            }
                                        }
                                    }
                                    // some error occured
                                    else {
                                        
                                    }
                                    break;
                                case "NOREMINDERS":

                                    break;
                                default:

                                    break;
                            }
                        }
                        // some error occured
                        else {
                            
                        }
                    }
                } catch (GuzzleException $e) {
                    // any issue occur
                    echo Psr7\str($e->getRequest());
                    if ($e->hasResponse()) {
                        echo Psr7\str($e->getResponse());
                    }
                }
            }
        }
    }

    public function getLatestReminderStatusFromCloud() {


        $this->setClientRequestData();

        $arrDebugMailParams = array();
        $arrDebugMailParams['subject'] = 'Get Latest Reminders status from cloud';
        $arrDebugMailParams['arrData'] = array();
        DebugMailSend::sendDebugMail($arrDebugMailParams);
        try {
            $reponse = $this->client->request('GET', 'getLatestReminderStatusFromCloud');

            $status = $reponse->getStatusCode();

            $header = $reponse->getHeader('content-type');

            $body = $reponse->getBody();

            $strBody = (string) $body;
// print '<pre>strBody :: ';
// print_r($strBody);
// print '</pre>';
// die();
            $arrBody = json_decode($strBody, true);

            if (!isset($arrBody['error']) || !isset($arrBody['flagMsg']) || !isset($arrBody['message']) || !isset($arrBody['data'])) {
                
            } else {
                $error = $arrBody['error'];
                $flagMsg = $arrBody['flagMsg'];
                $message = $arrBody['message'];
                $data = $arrBody['data'];
                $arrDebugMailParams = array();
                $arrDebugMailParams['subject'] = 'Response Get Latest Reminders status from cloud';
                $arrDebugMailParams['arrData'] = $data;
                DebugMailSend::sendDebugMail($arrDebugMailParams);
                // no error occured
                if ($error == 0) {
                    switch ($flagMsg) {
                        case "FETCHED":
                            if (isset($data)) {
                                $arrRemindersToUpdated = $data;

                                if (!empty($arrRemindersToUpdated)) {
                                    foreach ($arrRemindersToUpdated AS $key => $arrReminderToUpdate) {
                                        $reminderId = $arrReminderToUpdate['reminderid'];
                                        $twilioStatus = $arrReminderToUpdate['twiliostatus'];
                                        $confirmDate = $arrReminderToUpdate['updateddate'];

                                        $arrParams = array();
                                        $arrParams['reminderId'] = $reminderId;
                                        $arrParams['twilioStatus'] = $twilioStatus;
                                        $arrParams['confirmdate'] = $confirmDate;

                                        Reminder::updateTwilioStatusFromTheServer($arrParams);
                                    }
                                }
                            }
                            // some error occured
                            else {
                                
                            }
                            break;
                        case "NOREMINDERS":

                            break;
                        default:

                            break;
                    }
                }
                // some error occured
                else {
                    
                }
            }
        } catch (GuzzleException $e) {
            // any issue occur
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function getLatestAllReminderStatusFromCloud() {


        $this->setClientRequestData();

        $arrDebugMailParams = array();
        $arrDebugMailParams['subject'] = 'Get Latest Reminders status from cloud';
        $arrDebugMailParams['arrData'] = array();
        DebugMailSend::sendDebugMail($arrDebugMailParams);
        try {
            $reponse = $this->client->request('GET', 'getLatestAllReminderStatusFromCloud');

            $status = $reponse->getStatusCode();

            $header = $reponse->getHeader('content-type');

            $body = $reponse->getBody();

            $strBody = (string) $body;

            $arrBody = json_decode($strBody, true);

            if (!isset($arrBody['error']) || !isset($arrBody['flagMsg']) || !isset($arrBody['message']) || !isset($arrBody['data'])) {
                
            } else {

                $error = $arrBody['error'];
                $flagMsg = $arrBody['flagMsg'];
                $message = $arrBody['message'];
                $data = $arrBody['data'];

                // no error occured
                if ($error == 0) {
                    switch ($flagMsg) {
                        case "FETCHED":
                            if (isset($data)) {
                                $arrRemindersToUpdated = $data;

                                if (!empty($arrRemindersToUpdated)) {
                                    foreach ($arrRemindersToUpdated AS $key => $arrReminderToUpdate) {
                                        if (isset($arrReminderToUpdate['reminderid']) && isset($arrReminderToUpdate['remindertimingid']) && isset($arrReminderToUpdate['twiliostatus'])) {


                                            Reminder::updateAllTwilioStatusFromTheServer($arrReminderToUpdate);
                                        }
                                    }
                                }
                            }

                            break;
                        case "NOREMINDERS":

                            break;
                        default:

                            break;
                    }
                }
                // some error occured
                else {
                    
                }
            }
        } catch (GuzzleException $e) {
            // any issue occur
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    //get latest twilio logs from cloud
    public function getLatestReminderTwilioUpdateFromCloud() {

        $this->setClientRequestData();

        $arrDebugMailParams = array();
        $arrDebugMailParams['subject'] = 'Get Latest Reminders twilio status update';
        $arrDebugMailParams['arrData'] = array();
        DebugMailSend::sendDebugMail($arrDebugMailParams);
        try {
            $reponse = $this->client->request('GET', 'getTwilioLogsFromCloud');

            $status = $reponse->getStatusCode();

            $header = $reponse->getHeader('content-type');

            $body = $reponse->getBody();

            $strBody = (string) $body;

            $arrBody = json_decode($strBody, true);

            $arrDebugMailParams = array();
            $arrDebugMailParams['subject'] = 'RESPONSE Get Latest Reminders twilio status update';
            $arrDebugMailParams['arrData'] = $arrBody;
            DebugMailSend::sendDebugMail($arrDebugMailParams);

            if (isset($arrBody['error']) || isset($arrBody['flagMsg']) || isset($arrBody['message']) || isset($arrBody['data'])) {
                // no error occured
                $error = $arrBody['error'];
                $flagMsg = $arrBody['flagMsg'];
                $message = $arrBody['message'];
                $data = $arrBody['data'];
                if (!empty($flagMsg)) {
                    switch ($flagMsg) {
                        case "FETCHED":
                            if (!empty($data)) {
                                \App\TwiloLog::updateTwilioLogsFromServer($data);
                            }
                            break;

                        case "NOREMINDERS":
                            break;

                        default:
                    }
                }
                // some error occured
            }
        } catch (GuzzleException $e) {
            // any issue occur
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    //send email from local to cloud
    public function syncEmailSettings() {

        $this->setClientRequestData();


        $arrAdminSettingsData = AdminSetting::checkIfEmailAndAdminSettingsUpdated();

        if (!empty($arrAdminSettingsData)) {

            $arrAdminSettingsData = $arrAdminSettingsData[0];

            $arrMultipartData = array();

            foreach ($arrAdminSettingsData AS $key => $arrEachSettingValue) {
                $arrTmpArray = array();

                if ($key != 'companylogo') {
                    $arrTmpArray['name'] = $key;
                    $arrTmpArray['contents'] = $arrEachSettingValue;
                } else {

                    $arrTmpArray['name'] = 'flCompanyLogo';

                    // checking file exists
                    $companyLogoPath = config('filesystems.disks.local.root') . '/public/origimg/' . $arrEachSettingValue;
//                    $companyLogoPath = 'public/storage/origimg/' . $arrEachSettingValue;
                    $flagCompanyLogoExists = File::exists($companyLogoPath);
                    if (!$flagCompanyLogoExists) {
                        $companyLogoPath = 'public/img/no-img.png';
                    }
                    $fileHandler = fopen($companyLogoPath, 'r');

                    if (!empty($arrEachSettingValue)) {
                        $arrTmpArray['contents'] = $fileHandler;
                        $arrTmpArray['filename'] = $arrEachSettingValue;
                    } else {
                        $arrTmpArray['contents'] = '';
                    }

                    // sending file name.
                    $arrTemp2 = array();
                    $arrTemp2['name'] = $key;
                    $arrTemp2['contents'] = $arrEachSettingValue;

                    array_push($arrMultipartData, $arrTemp2);
                }

                array_push($arrMultipartData, $arrTmpArray);
            }

            $arrDebugMailParams = array();
            $arrDebugMailParams['subject'] = 'To Update email/sms/call settings on cloud';
            $arrDebugMailParams['arrData'] = $arrMultipartData;
            DebugMailSend::sendDebugMail($arrDebugMailParams);

            try {
                $reponse = $this->client->request('POST', 'addSettingsToCloud', [
                    'multipart' => $arrMultipartData
                ]);



                $status = $reponse->getStatusCode();

                $header = $reponse->getHeader('content-type');

                $body = $reponse->getBody();

                $strBody = (string) $body;

                $arrBody = json_decode($strBody, true);
                $arrDebugMailParams = array();
                $arrDebugMailParams['subject'] = 'RESPONSE To Update email/sms/call settings on cloud';
                $arrDebugMailParams['arrData'] = $arrBody;
                DebugMailSend::sendDebugMail($arrDebugMailParams);
                if (!isset($arrBody['error']) || !isset($arrBody['flagMsg']) || !isset($arrBody['message']) || !isset($arrBody['data'])) {
                    
                } else {
                    $error = $arrBody['error'];
                    $flagMsg = $arrBody['flagMsg'];
                    $message = $arrBody['message'];
                    $data = $arrBody['data'];

                    // no error occured
                    if ($error == 0) {
                        switch ($flagMsg) {
                            case "UPDATED":
                                AdminSetting::updateAdminSettingOnCloud();
                                break;
                            default:
                                break;
                        }
                    }
                }
            } catch (GuzzleException $e) {
                // any issue occur
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            }
        }
    }

    /* created by Bhanushankar joshi */
    public function remindernotify() {
        // select group_concat(recid) from sns_reminder where sns_reminder.isnotify = 0

        $result = Reminder::where('isnotify', '0')->pluck('recid')->toArray();
//        $result = json_decode($result);
        \DB::enableQueryLog();
        if($result) {
            $result1 = \DB::update("update sns_reminder set sns_reminder.isnotify='1'where sns_reminder.recid in (" . implode($result, ',') . ") ");
        }
//             $db = \DB::getQueryLog();
       die(json_encode($result));
    }
    
    /** added by Keyur Mistry on 27th March 2018 
     * Pending script, to completed
     * Script to sync the case report (count of card, card2, case, casecard) client key wise, for  sending to the server.
     * **/
    public function syncCaseReportDataToServer() {

        $this->setClientRequestData();

        $arrRemindersToUpdate = ReminderSync::getRemindersToUpdateOnServer();

        $arrDebugMailParams = array();
        $arrDebugMailParams['subject'] = 'Reminders to cloud for sync';
        $arrDebugMailParams['arrData'] = $arrRemindersToUpdate;
        DebugMailSend::sendDebugMail($arrDebugMailParams);


        // if any reminders are present for update
        if (COUNT($arrRemindersToUpdate) > 0) {

            if ((isset($arrRemindersToUpdate['arrResultNotSynced']) && empty($arrRemindersToUpdate['arrResultNotSynced'])) &&
                    (isset($arrRemindersToUpdate['arrResultNeedToUpdate']) && empty($arrRemindersToUpdate['arrResultNeedToUpdate'])) &&
                    (isset($arrRemindersToUpdate['arrResultNeedToDelte']) && empty($arrRemindersToUpdate['arrResultNeedToDelte']))) {
                
            } else {
                try {

                    $arrRequestDataToPost = array();
                    $arrRequestDataToPost['arrResultNotSynced'] = $arrRemindersToUpdate['arrResultNotSynced'];
                    $arrRequestDataToPost['arrResultNeedToUpdate'] = $arrRemindersToUpdate['arrResultNeedToUpdate'];
                    $arrRequestDataToPost['arrResultNeedToDelte'] = $arrRemindersToUpdate['arrResultNeedToDelte'];

                    $reponse = $this->client->request('POST', 'addRemindersToCloud', [
                        'form_params' => ['arrRequestData' => $arrRequestDataToPost]
                    ]);

                    $status = $reponse->getStatusCode();

                    $header = $reponse->getHeader('content-type');

                    $body = $reponse->getBody();

                    $strBody = (string) $body;

                    $arrBody = json_decode($strBody, true);

                    if (!isset($arrBody['error']) || !isset($arrBody['flagMsg']) || !isset($arrBody['message']) || !isset($arrBody['data'])) {
                        
                    } else {
                        $error = $arrBody['error'];
                        $flagMsg = $arrBody['flagMsg'];
                        $message = $arrBody['message'];
                        $data = $arrBody['data'];
                        // no error occured

                        $arrDebugMailParams = array();
                        $arrDebugMailParams['subject'] = 'Response from Reminders to cloud for sync';
                        $arrDebugMailParams['arrData'] = $data;
                        DebugMailSend::sendDebugMail($arrDebugMailParams);
                        if ($error == 0) {
                            switch ($flagMsg) {
                                case "INSERTED":
                                    if (isset($data) && !empty($data['arrResponse'])) {

                                        $arrResponseData = $data['arrResponse'];

                                        // classifying response from the server.
                                        $arrReminderAndSyncIdInserted = $arrResponseData['arrReminderAndSyncIdInserted'];
                                        $arrReminderIdsToUpdate = $arrResponseData['arrReminderIdsToUpdate'];
                                        $arrReminderIdsToDelete = $arrResponseData['arrReminderIdsToDelete'];

                                        // processing for all the reminders that added into the server.
                                        $arrRemindersSyncedWithServer = array();

                                        foreach ($arrReminderAndSyncIdInserted AS $key => $arrEachReminderForUpdate) {
                                            $reminderSyncId = $arrEachReminderForUpdate['reminderSyncId'];
                                            $reminderId = $arrEachReminderForUpdate['reminderId'];

                                            array_push($arrRemindersSyncedWithServer, $reminderSyncId);
                                        }

                                        $arrRemindersSyncedWithServer = array_unique($arrRemindersSyncedWithServer);

                                        if (!empty($arrRemindersSyncedWithServer)) {
                                            ReminderSync::updateRemindersFlagServerSync($arrRemindersSyncedWithServer);
                                        }

                                        // processing for all the reminders that are updated on the server.
                                        if (!empty($arrReminderIdsToUpdate)) {
                                            Reminder::updateFlagForDoNotSyncWithServerAgain($arrReminderIdsToUpdate);
                                        }

                                        // processing for all the reminders that are deleted on the server.
                                        if (!empty($arrReminderIdsToDelete)) {
                                            Reminder::updateFlagForDoNotSyncWithServerAgain($arrReminderIdsToUpdate);
                                        }


                                        /// function call to check if any reminders pending to upload on the cloud.
                                        $arrRemindersToUpdateCheck = ReminderSync::getRemindersToUpdateOnServer();

                                        // if any reminders left to sync on cloud. If so call this function again.
                                        if ((isset($arrRemindersToUpdateCheck['arrResultNotSynced']) && !empty($arrRemindersToUpdateCheck['arrResultNotSynced'])) ||
                                                (isset($arrRemindersToUpdateCheck['arrResultNeedToUpdate']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToUpdate'])) ||
                                                (isset($arrRemindersToUpdateCheck['arrResultNeedToDelte']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToDelte']))) {

                                            if (isset($arrRemindersToUpdateCheck['arrResultNotSynced']) && !empty($arrRemindersToUpdateCheck['arrResultNotSynced'])) {
                                                ReminderSync::updateRemindersFlagServerSync($arrRemindersToUpdateCheck['arrResultNotSynced']);
                                            }

                                            // processing for all the reminders that are updated on the server.
                                            if (isset($arrRemindersToUpdateCheck['arrResultNeedToUpdate']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToUpdate'])) {

                                                Reminder::updateFlagForDoNotSyncWithServerAgain($arrRemindersToUpdateCheck['arrResultNeedToUpdate']);
                                            }

                                            // processing for all the reminders that are deleted on the server.
                                            if (isset($arrRemindersToUpdateCheck['arrResultNeedToDelte']) && !empty($arrRemindersToUpdateCheck['arrResultNeedToDelte'])) {
                                                Reminder::updateFlagForDoNotSyncWithServerAgain($arrRemindersToUpdateCheck['arrResultNeedToDelte']);
                                            }
                                        }
                                    }
                                    // some error occured
                                    else {
                                        
                                    }
                                    break;
                                case "NOREMINDERS":

                                    break;
                                default:

                                    break;
                            }
                        }
                        // some error occured
                        else {
                            
                        }
                    }
                } catch (GuzzleException $e) {
                    // any issue occur
                    echo Psr7\str($e->getRequest());
                    if ($e->hasResponse()) {
                        echo Psr7\str($e->getResponse());
                    }
                }
            }
        }

        //     $db = \DB::getQueryLog();
        die(json_encode($result));
    }

}
