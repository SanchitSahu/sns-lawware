<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterTemplates;
use Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\MasterTemplatesRequest;
use Session;
use Illuminate\Support\Facades\Redirect;
use DB;
use Carbon\Carbon;

class MasterTemplatesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Redirect::to('settings');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $flagcolumn = ['SMS', 'CALL', 'EMAIL'];
        $language = \App\TwilioLanguage::getlanguage();
        $fields = \App\ReminderFields::getfields();
        $slots = \App\ReminderSlots::getslots();
        $column = [0, 1, 2, 3];
        $datatitle='Create';
        return view('mastertemplates.create')->with(['language' => $language, 'fields' => $fields, 'slots' => $slots,
                    'column' => $column, 'flagcolumn' => $flagcolumn, 'datatitle'=>$datatitle]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MasterTemplatesRequest $request) {
        DB::beginTransaction();
        try {
            // process the store
            $input = $request->all();
            $contact = MasterTemplates::create($input);
        } catch (\Exception $e) {
            DB::rollback();
            //throw $e;
            Session::flash('message', 'Oops something went wrong');
            return Redirect::to('settings');
        }
        // If we reach here, then// data is valid and working.//
        DB::commit();
        // redirect
        Session::flash('message', 'Templates created successfully ');
        return Redirect::to('settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $mastertemplates = MasterTemplates::find($id);
        $flagcolumn = ['SMS', 'CALL', 'EMAIL'];
        $language = \App\TwilioLanguage::getlanguage();
        $fields = \App\ReminderFields::getfields();
        $slots = \App\ReminderSlots::getslots();
        $column = [0, 1, 2, 3];
            $datatitle='Edit';
        return view('mastertemplates.edit')->with(['language' => $language, 'fields' => $fields, 'slots' => $slots,
                    'column' => $column, 'flagcolumn' => $flagcolumn, 'mastertemplates' => $mastertemplates,
            'datatitle'=>$datatitle]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MasterTemplatesRequest $mastertemplaterequest, $id) {
        DB::beginTransaction();
        try {

            $request = $mastertemplaterequest->all();
            // process the store
            $mastertemplates = MasterTemplates::find($id);
            MasterTemplates::setemptyvalue($request, $mastertemplates);
            $mastertemplates->update($request);
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            Session::flash('message', 'Oops something went wrong');
            return Redirect::to('settings');
        }
        // If we reach here, then// data is valid and working.//
        DB::commit();
        // redirect
        Session::flash('message', 'Templates updated successfully ');
        return Redirect::to('settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $contacts = MasterTemplates::find($id);
        $contacts->isdeleted = MasterTemplates::IS_DELETED;
        $contacts->save();
    }

    /**
     * get the list from master templates table.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function getlist(Request $request) {
        $request = $request->all();
        // get all the contacts
        $templates = MasterTemplates::select(['recid', 'title', 'purpose', 'createddate', 'bodyemail'])->active();
        $datatables = Datatables::of($templates)
                ->editColumn('bodyemail', function ($templates) {
                    return strlen($templates->bodyemail) > 50 ? substr($templates->bodyemail, 0, 50) . "..." : $templates->bodyemail;
                })->editColumn('createddate', function ($user) {
                       return $user->createddate->format('d/m/Y');
                })
                ->filterColumn('createddate', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(createddate,'%d/%m/%Y') like ?", ["%$keyword%"]);
        });

        // Global search function

        return $datatables->make(true);
    }

}
