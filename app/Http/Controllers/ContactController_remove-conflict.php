<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\MasterCountry;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Http\Request;
use App\Contact;
use Session;
use Illuminate\Support\Facades\Redirect;
use DB;
use Excel;
use URL;

class ContactController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('contacts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $phonetype = Contact::phonetype();
        $emailtype = Contact::emailtype();
        $salutation = Contact::salutationtype();
        $countrycode = MasterCountry::getcountry();

        return view('contacts.create')->with(['phonetype' => $phonetype, 'countrycode' => $countrycode,
                    'emailtype' => $emailtype, 'salutation' => $salutation]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactFormRequest $request) {

        // process the store
        $input = $request->all();
//        $contact = Contact::create($input);
//        $lastinsertedid = "RS" . $contact->recid;
//        $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
//        $contact->update($caseId);
        self::saveContact($input);
        // redirect
        Session::flash('message', 'Contact created successfully ');
        return Redirect::to('contacts');
    }

    public static function saveContact($input) {
        $contact = Contact::create($input);
        $lastinsertedid = "RS" . $contact->recid;
        $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
        $contact->update($caseId);
        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $contact = Contact::editcontacts($id);
        $phonetype = Contact::phonetype();
        $emailtype = Contact::emailtype();
        $salutation = Contact::salutationtype();
        $countrycode = MasterCountry::getcountry();
        // show the edit form and pass the contact
        return view('contacts.edit')
                        ->with(['contact' => $contact, 'phonetype' => $phonetype, 'countrycode' => $countrycode,
                            'emailtype' => $emailtype, 'salutation' => $salutation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(ContactFormRequest $contact, $id) {

        $request = $contact->all();
        $contacts = Contact::where(['cardcode' => $id])->first();

        if (!empty($contacts)) {
            $contacts->update($request);
        } else {
            $contact = Contact::create($request);
            $lastinsertedid = "RS" . $contact->recid;
            $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
            $contact->update($caseId);
        }
        // redirect
        Session::flash('message', 'Contact updated successfully!');
        return Redirect::to('contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $contacts = Contact::where(['cardcode' => $id])->first();
        $contacts->isdeleted = Contact::IS_DELETED;
        $contacts->save();
    }

    /**
     * get the list from contacts table.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function getlist() {

        $result = Contact::listcontacts();
        return Datatables::of($result)
                        ->make(true);
    }

<<<<<<< HEAD
=======
    public function searchForContact_old(Request $request, $queryString) {
$queryString=str_replace(" ","",$queryString);
        $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                                FROM (
                                    (
                                        /*SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,IF(c.first LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(c.middle LIKE "' . $queryString . '%", 1, 0) middlenamestart,IF(c.last LIKE "' . $queryString . '%", 1, 0) lastnamestart, "OTS" contactType
                                        FROM card c, card2 c2, casecard cc
                                        WHERE c.firmcode = c2.firmcode
                                            AND cc.cardcode = c.cardcode
                                            AND (CONCAT("",c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.last,c.first) LIKE "%' . $queryString . '%" '
                . '                         OR CONCAT("",c.salutation,c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.salutation,c.last,c.first) LIKE "%' . $queryString . '%" '
                . '                         OR CONCAT("",c.salutation,c.first,c.middle,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.first,c.middle,c.last) LIKE "%' . $queryString . '%"  OR c.first LIKE "' . $queryString . '%" OR c.middle LIKE "' . $queryString . '%" OR c.last LIKE "' . $queryString . '%" )*/
                SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", TRIM(c.salutation), TRIM(c.first), TRIM(c.last)) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,IF(c.first LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(c.middle LIKE "' . $queryString . '%", 1, 0) middlenamestart,IF(c.last LIKE "' . $queryString . '%", 1, 0) lastnamestart, "OTS" contactType
                                        FROM card c, card2 c2, casecard cc
                                        WHERE c.firmcode = c2.firmcode
                                            AND cc.cardcode = c.cardcode
                                            AND (CONCAT("",TRIM(c.first),TRIM(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",TRIM(c.last),TRIM(c.first)) LIKE "%' . $queryString . '%" '
                . '                         OR CONCAT("",TRIM(c.salutation),TRIM(c.first),TRIM(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",TRIM(c.salutation),TRIM(c.last),TRIM(c.first)) LIKE "%' . $queryString . '%" '
                . '                         OR CONCAT("",TRIM(c.salutation),TRIM(c.first),TRIM(c.middle),TRIM(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",TRIM(c.first),TRIM(c.middle),TRIM(c.last)) LIKE "%' . $queryString . '%"  OR TRIM(c.first) LIKE "' . $queryString . '%" OR TRIM(c.middle) LIKE "' . $queryString . '%" OR TRIM(c.last) LIKE "' . $queryString . '%" )
                                        GROUP BY c.cardcode
                                    ) 

                                    UNION
                                    
                                    (
                                        SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,IF(cl.firstname LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(cl.middlename LIKE "' . $queryString . '%", 1, 0) middlenamestart, IF(cl.lastname LIKE "' . $queryString . '%", 1, 0) lastnamestart, "SNS" contactType
                                        FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
                                        WHERE cl.isdeleted = "0" AND ( CONCAT("",cl.firstname,cl.lastname) LIKE "%' . $queryString . '%" OR CONCAT("",cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
                             . '        OR CONCAT("",cl.salutation,cl.firstname,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.salutation,cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
                                       . 'OR CONCAT("",cl.salutation,cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"   OR cl.firstname LIKE "' . $queryString . '%" OR cl.middlename LIKE "' . $queryString . '%" OR cl.lastname LIKE "' . $queryString . '%" ) 
                                    ) 

                                ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');
>>>>>>> origin/master

    public function searchForContact(Request $request, $queryString, $searchType) {
        $queryString = str_replace(" ", "", $queryString);

//        if($searchType === "participant") {}    
        if ($searchType == "case") {
//            DB::enableQueryLog();
            $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                                    FROM (
                                        (
                                            SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,1 as firstnamestart,1 as middlenamestart,1 as lastnamestart, "A1LawOTS" contactType
                                            FROM card c, card2 c2, casecard cc
                                            WHERE c.firmcode = c2.firmcode
                                                AND cc.cardcode = c.cardcode
                                                /*AND (CONCAT("",c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.last,c.first) LIKE "%' . $queryString . '%" '
                            . '                         OR CONCAT("",c.salutation,c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.salutation,c.last,c.first) LIKE "%' . $queryString . '%" '
                            . '                         OR CONCAT("",c.salutation,c.first,c.middle,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.first,c.middle,c.last) LIKE "%' . $queryString . '%"  OR c.first LIKE "' . $queryString . '%" OR c.middle LIKE "' . $queryString . '%" OR c.last LIKE "' . $queryString . '%" )*/
                                            AND cc.caseno = "' . $queryString . '"
                                                AND (c.first <> "" AND IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) <> "")
                                            GROUP BY c.cardcode
                                        )
                                    ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');
//            $db = DB::getQueryLog();
        } else {
            $result = self::fetchCardDetailsQuery($queryString);
        }


        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => $result,
            'error' => 0,
            'flagMsg' => 'FETCH',
            'message' => 'Add reminder contacts fetch'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

<<<<<<< HEAD
    public static function fetchCardDetailsQuery($queryString) {
        $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                                    FROM (
                                        (
                                            SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,IF(c.first LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(c.middle LIKE "' . $queryString . '%", 1, 0) middlenamestart,IF(c.last LIKE "' . $queryString . '%", 1, 0) lastnamestart, "OTS" contactType
                                            FROM card c, card2 c2, casecard cc
                                            WHERE c.firmcode = c2.firmcode
                                                AND cc.cardcode = c.cardcode
                                                AND (CONCAT("",c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.last,c.first) LIKE "%' . $queryString . '%" '
                        . '                         OR CONCAT("",c.salutation,c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.salutation,c.last,c.first) LIKE "%' . $queryString . '%" '
                        . '                         OR CONCAT("",c.salutation,c.first,c.middle,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.first,c.middle,c.last) LIKE "%' . $queryString . '%"  OR c.first LIKE "' . $queryString . '%" OR c.middle LIKE "' . $queryString . '%" OR c.last LIKE "' . $queryString . '%" )
                                            GROUP BY c.cardcode
                                        ) 

                                        UNION

                                        (
                                            SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,IF(cl.firstname LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(cl.middlename LIKE "' . $queryString . '%", 1, 0) middlenamestart, IF(cl.lastname LIKE "' . $queryString . '%", 1, 0) lastnamestart, "SNS" contactType
                                            FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
                                            WHERE cl.isdeleted = "0" AND ( CONCAT("",cl.firstname,cl.lastname) LIKE "%' . $queryString . '%" OR CONCAT("",cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
                        . '        OR CONCAT("",cl.salutation,cl.firstname,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.salutation,cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
                        . 'OR CONCAT("",cl.salutation,cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"   OR cl.firstname LIKE "' . $queryString . '%" OR cl.middlename LIKE "' . $queryString . '%" OR cl.lastname LIKE "' . $queryString . '%" ) 
                                        ) 

                                    ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');
        return $result;
=======
    /** 
     * Method to search contacts in participants  tab in reminder create page
     * 
     * **/
    public function searchForContact(Request $request, $queryString) {
        $queryStringSort = str_replace(" ","",$queryString);

        $queryString = addslashes($queryString);
        $queryString = urldecode($queryString);
        $queryArray = explode(" ",$queryString);
//        $whereArr = $whereContactArr = array();
//        foreach($queryArray as $key=>$val) {
//            $whereCaseCondition = ""; $whereContactCondition = "";
////            $whereOrCondition = array();
//            $whereCaseCondition = "(c.salutation LIKE '" . $val . "%' OR c.first LIKE '" . $val . "%' OR c.middle LIKE '" . $val . "%' OR c.last LIKE '" . $val . "%' OR cc.caseno LIKE '" . $val . "%')";
////            $whereOrClause = "(" . implode(' OR ', $whereOrCondition). ")";
//            $whereArr[$key] = $whereCaseCondition;
//            
//            $whereContactCondition = "(cl.salutation LIKE '" . $val . "%' OR cl.firstname LIKE '" . $val . "%' OR cl.middlename LIKE '" . $val . "%' OR cl.lastname LIKE '" . $val . "%' OR cl.case LIKE '" . $val . "%')";
//            $whereContactArr[$key] = $whereContactCondition;
////            echo "<pre>";print_r($val);exit;
//        }
        $strQryParams = "";
        $strWhereQry = "";
        $strContactWhereQry = "";
        $strContactQryParams = "";
        
        foreach($queryArray as $key=>$strSearchKey) {
            $strSearchKey = trim($strSearchKey);
            $strSearchKey = trim($strSearchKey, ",");

//            if ($key == 0 || true) {
            if ($key == 0) {

                $strWhereQry .= "IF(c.first LIKE '" . $strSearchKey . "', 1, 0) firstNameSearchTop,
                                IF(c.middle LIKE '" . $strSearchKey . "', 1, 0) middleNameSearchTop,
                                IF(c.last LIKE '" . $strSearchKey . "', 1, 0) lastNameSearchTop, ";

                $strWhereQry .= "IF(c.first LIKE '" . $strSearchKey . "%', 1, 0) firstnamestart,
                                IF(c.middle LIKE '" . $strSearchKey . "%', 1, 0) middlenamestart,
                                IF(c.last LIKE '" . $strSearchKey . "%', 1, 0) lastnamestart, ";
                
                $strContactWhereQry .= "IF(cl.firstname LIKE '" . $strSearchKey . "', 1, 0) firstNameSearchTop,
                                IF(cl.middlename LIKE '" . $strSearchKey . "', 1, 0) middleNameSearchTop,
                                IF(cl.lastname LIKE '" . $strSearchKey . "', 1, 0) lastNameSearchTop, ";
                
                $strContactWhereQry .= "IF(cl.firstname LIKE '" . $strSearchKey . "%', 1, 0) firstnamestart,
                                IF(cl.middlename LIKE '" . $strSearchKey . "%', 1, 0) middlenamestart,
                                IF(cl.lastname LIKE '" . $strSearchKey . "%', 1, 0) lastnamestart, ";
            }
            
            /*$strQryParams .= "    (c.first LIKE '%" . $strSearchKey . "%'
                                OR c.last LIKE '%" . $strSearchKey . "%'
                                OR c.middle LIKE '%" . $strSearchKey . "%'
                                
                                OR cc.caseno LIKE '%" . $strSearchKey . "%') OR ";
            $strContactQryParams .= "(cl.firstname LIKE '%" . $strSearchKey . "%'
                                OR cl.lastname LIKE '%" . $strSearchKey . "%'
                                OR cl.middlename LIKE '%" . $strSearchKey . "%'
                                
                                OR cl.case LIKE '%" . $strSearchKey . "%') OR ";*/
            $strQryParams .= " (TRIM(c.first) LIKE '" . $strSearchKey . "%'
                                OR TRIM(c.last) LIKE '" . $strSearchKey . "%'
                                OR TRIM(c.middle) LIKE '" . $strSearchKey . "%'
                                OR TRIM(cc.caseno) LIKE '" . $strSearchKey . "%') OR ";
            $strContactQryParams .= " (TRIM(cl.firstname) LIKE '" . $strSearchKey . "%'
                                OR TRIM(cl.lastname) LIKE '" . $strSearchKey . "%'
                                OR TRIM(cl.middlename) LIKE '" . $strSearchKey . "%'
                                OR TRIM(cl.case) LIKE '" . $strSearchKey . "%') OR ";
        }
//        echo $strWhereQry;exit;
        $strWhereQry = rtrim($strWhereQry, ", ");
        $strWhereQry = $strWhereQry;
        $strQryParams = rtrim($strQryParams, "OR ");
        
        $strContactWhereQry = rtrim($strContactWhereQry, ", ");
        $strContactWhereQry = $strContactWhereQry;
        $strContactQryParams = rtrim($strContactQryParams, "OR ");
        
//        DB::enableQueryLog();
        $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType, firstNameSearchTop, middleNameSearchTop, lastNameSearchTop, priorSearchFirst, priorSearchSecond
                                FROM (
                                    (
                                        SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,' . $strWhereQry .', "OTS" contactType, IF(CONCAT("",TRIM(c.first),TRIM(c.last)) LIKE "' . $queryStringSort .'", 1, 0) priorSearchFirst, IF(CONCAT("",TRIM(c.last),TRIM(c.first)) LIKE "' . $queryStringSort .'", 1, 0) priorSearchSecond
                                        FROM card c, card2 c2, casecard cc
                                        WHERE c.firmcode = c2.firmcode
                                            AND cc.cardcode = c.cardcode
                                            AND (' . $strQryParams . ')
                                        GROUP BY c.cardcode HAVING (TRIM(fullName) <> "" AND (TRIM(email) <> "" OR TRIM(phoneNumber) <> "") )
                                    ) 

                                    UNION
                                    
                                    (
                                        SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,' . $strContactWhereQry . ', "SNS" contactType, IF(CONCAT("",TRIM(cl.firstname),TRIM(cl.lastname)) LIKE "' . $queryStringSort .'", 1, 0) priorSearchFirst, IF(CONCAT("",TRIM(cl.lastname),TRIM(cl.firstname)) LIKE "' . $queryStringSort .'", 1, 0) priorSearchSecond
                                        FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
                                        WHERE cl.isdeleted = "0" AND (' . $strContactQryParams . ') 
                                    ) 

                                ) AS t2 ORDER BY priorSearchFirst DESC, priorSearchSecond DESC, firstNameSearchTop DESC, middleNameSearchTop DESC, lastNameSearchTop DESC, firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');

//$db = DB::getQueryLog();
//echo "<pre>";print_r($db);exit;
        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => $result,
            'error' => 0,
            'flagMsg' => 'FETCH',
            'message' => 'Add reminder contacts fetch'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
>>>>>>> origin/master
    }

    public function importExportExcelORCSV() {
        return view('file_import_export');
    }

    public function importFileIntoDB(Request $request) {
        try {
            $request = $request->all();

            $validatorfile = Validator::make($request, [
                        'import_contact' => 'required',
            ]);

            if ($validatorfile->fails()) {
                return ['error' => 2, 'errormessage' => 'Please select appropriate file'];
            }
            $file = $request['import_contact'];

            if (!in_array($file->getMimeType(), ['application/vnd.ms-excel', 'text/csv', 'text/plain'])) {
                return ['error' => 2, 'errormessage' => ' Please import only CSV file !!'];
            }
            if ($file) {
                $path = $file->getRealPath();
                $data = Excel::load($path)->get();
                if ($data->count()) {
                    return Contact::addCsvUpload($data->toArray());
                }
            }
        } catch (\Exception $e) {
            //throw $e;
            return ['error' => 2, 'errormessage' => 'please attach proper format'];
        }
    }

    public function downloadSampleExcelFile() {

        return Excel::load('storage/exports/Synergy Notification System.xlsx', function($reader) {
                    
                })->download('csv');
    }

    public function downloadExcelFile() {

        return Excel::load('storage/exports/synergynotification.csv', function($reader) {
                    
                })->download('csv');
    }

    /* public function removecontacts(Request $request){
      $ids="'". implode("','",$request->checkids)."'";
      $result = DB::update("update sns_contact_list set isdeleted ='1' where cardcode in($ids)");
      if ($result) {
      die(json_encode($result));
      } else {
      die(json_encode($result));
      }
      exit;
      } */

    public function ajaxSearchForCaseNo(Request $request) {
        $query = $request->get('term', '');
        $queryString = $query;

        $result = DB::select("SELECT caseno as value, yourfileno as id FROM `case` as c WHERE c.caseno LIKE '%" . $queryString . "%'");
        return json_encode($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quickContactStore(request $request) {
        $input = $request->all();
        $result = self::checkEmailPhoneDuplication($input);
        if (!$result) {
            // process the store
            self::saveContact($input);
            $result = self::checkEmailPhoneDuplication($input);
        }
<<<<<<< HEAD

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => $result,
            'error' => 0,
            'flagMsg' => 'FETCH',
            'message' => 'Add reminder contacts fetch'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public static function checkEmailPhoneDuplication($request) {
        $contactNumber = $request['phonenumber'];
        $email = $request['email'];

        $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                                    FROM (
                                        /*(
                                            SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,1 as firstnamestart,1 as middlenamestart,1 as lastnamestart, "OTS" contactType
                                            FROM card c, card2 c2, casecard cc
                                            WHERE c.firmcode = c2.firmcode
                                                AND cc.cardcode = c.cardcode
                                                AND ((ExtractNumber(c.car) = "' . $contactNumber . '" OR ExtractNumber(c.home) = "' . $contactNumber . '" OR ExtractNumber(c2.phone2) = "' . $contactNumber . '" OR ExtractNumber(c2.phone1) = "' . $contactNumber . '") OR email = "' . $email . '")
                                            GROUP BY c.cardcode
                                        ) 

                                        UNION*/

                                        (
                                            SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,1 as firstnamestart,1 as middlenamestart, 1 as lastnamestart, "SNS" contactType
                                            FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
                                            WHERE cl.isdeleted = "0" AND ( ExtractNumber(phonenumber) = "' . $contactNumber . '" OR email = "' . $email . '" ) 
                                        ) 

                                    ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');

        return $result;
=======
        exit;
    }*/
    
    /**
     * Sync contact from temp table to web contact table.
     * **/
    public static function getPhoneContactType($type = "mobile") {
        $array = config('constants.contact_migrate.phonetype');
        $type = strtolower($type);
        return $array[$type];
    }
    public static function getEmailContactType($type = "home") {
        $array = config('constants.contact_migrate.emailtype');
        $type = strtolower($type);
        return $array[$type];
    }
    public function syncContactFromWindowsToWeb(Request $request) {
        try {
            $countrycode = MasterCountry::getcountry();
            $phonetype = Contact::phonetype();
            $emailtype = Contact::emailtype();
        
            $windowsContactList = DB::select("select * from tbl_rs_contactlist ");
            if (!empty($windowsContactList)) {
                foreach ($windowsContactList as $keypoint => $contactvalue) {
                    $contactvalue = (array) $contactvalue;
                    $coCode = (!empty($contactvalue['CountryCode']) ? $contactvalue['CountryCode'] : '+1');
                    $code = array_search($coCode, $countrycode);
                    
                    $phonet = array_search($contactvalue['MobileType'], $phonetype);
                    $emailt = array_search($contactvalue['EmailType'], $emailtype);
                    
                    // formate phone number 
                    $reminderObj = new ReminderController();
                    $phoneNumber = $reminderObj->formatphone($contactvalue['PhoneNo']);
                    $phoneNumber1 = substr($phoneNumber, -10);
                    
                    $contactArray = array(
                        'salutation' => $contactvalue['Salutation'],
                        'firstname' => $contactvalue['FName'],
                        'middlename' => $contactvalue['MName'],
                        'lastname' => $contactvalue['LName'],
                        'country_id' => $code ?? '+1',
                        'phonenumber' => $phoneNumber1,
                        'phonetype' => $phonet ?? "",
                        'email' => $contactvalue['Email'],
                        'emailtype' => $emailt ?? "",
                        'dob' => $contactvalue['DOB'],
                        'zipcode' => $contactvalue['Zipcode'],
                        'notes' => $contactvalue['Notes'],
                        'win_con_id' => $contactvalue['CID'],
                    );
                    
                    $contactExists = Contact::where(['win_con_id' => $contactvalue['CID']])->first();
                    
                    if(!$contactExists) {
                        // create new record
                        $contact = Contact::create($contactArray);
                        $lastinsertedid = "RS" . $contact->recid;
                        $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
                        $contact->update($caseId);
                    } else {
                        // update records if found int windows
                        $updateArray = array('win_con_id' => $contactvalue['CID']);
                        Contact::updateOrCreate($updateArray, $contactArray);
                    }
                }
                echo "All contacts imported successfully";
            } else {
                echo "No Data to Synchronize";
            }
        } catch (Exception $ex) {
//            self::storeExceptionFailureLog($ex);
        }
>>>>>>> origin/master
    }

}
