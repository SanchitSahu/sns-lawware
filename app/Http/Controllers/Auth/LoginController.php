<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   
     public function username()
    {
         
        return 'username';
    }
    
    
   public function login(Request $request)
    {
        $data = $request->all();

        $rules = [
            'username' => 'required|max:255|exists:users',
            'password' => 'required'
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            //login data not exist in db
            return redirect('/login')->withErrors($validator)->withInput();
        } else {
            $email = $data['username'];
            $pass = $data['password'];
            //in my table users, status must be 1 to login into app
            $matchWhere = ['username' => $email, 'password' => $pass,'isconfirmed'=>1];

            $user = \App\User::where($matchWhere)->UserActive()->first();
           
             if (!empty($user)) {  
                   if($user->isconfirmed==0){
                   //not status 1 or active
                $validator->errors()->add('username', 'Your account is not verified yet.');
                return redirect('/login')->withErrors($validator)->withInput();
            }
                 //start session and save data
                Auth::loginUsingId($user->recid);
                return redirect('/');
            }  else {
            
                //not status 1 or active
                $validator->errors()->add('username', 'These credentials do not match our records.');
                return redirect('/login')->withErrors($validator)->withInput();
            }
        }
    }

    

   
  

}
