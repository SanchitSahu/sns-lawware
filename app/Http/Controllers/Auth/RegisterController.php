<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Session;
use DB;
use URL;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'firstname' => 'required|string|max:255',
                    'lastname' => 'required|string|max:255',
                    'username' => 'required|string|max:255|unique:users',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|confirmed',
        ]);
    }

    /*
     * *
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validatormanually(array $data) {

        return Validator::make($data, [
                    'snsusersunqid' => 'required|unique:users|max:255',
                    'email' => 'required|email',
                    'password' => 'required',
                    'username' => 'required|unique:users|max:255',
                    'firstname' => 'required|string|max:255',
                    'lastname' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {

        User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'username' => $data['username'],
            'snsusersunqid' => $data['username'],
            'email' => $data['email'],
            'password' => $data['password'],
            'isconfirmed' => 0
        ]);
    }

    /**
     * Create a new user instance after a valid registration from sns.
     *
     * @param  array  $data
     * @return User
     */
    public function userregister(Request $request) {
        try {

            $input = $request->all();
            $validator = $this->validatormanually($input);
            if ($validator->fails()) {
                if ($validator->errors()->first() == 'The snsusersunqid has already been taken.' ||
                        $validator->errors()->first() == 'The username has already been taken.') {
                    return response()->json(['success' => '0',
                                'snsusersexisit' => 'success',
                                'message' => $validator->errors()->first()], 400);
                }
                return response()->json(['success' => '0', 'message' => $validator->errors()->first()], 400);
            }
            $register = User::create($input);

            if ($register) {
                //update snsuniquid

                $update = ['lawwarelogin' => 1, 'role' => 'users']; //put this value equal to datatable column name where it will be saved
                $register->update($update);
                return ['success' => 1, 'message' => 'successfully register', 'snsusersexisit' => 'fail'];
            }
            return ['success' => 0, 'message' => 'oops something went wrong'];
        } catch (\Exception $e) {
            // return $e;
            return ['success' => 0, 'message' => 'oops something went wrong'];
        }
    }

    public function userlogin(Request $request) {

        try {

            $data = $request->all();

            $rules = [
                'username' => 'required|max:255|exists:users',
                'password' => 'required'
            ];

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {
                //login data not exist in db
                return redirect('/login')->withErrors($validator)->withInput();
            } else {

                $email = $data['username'];
                $snsusersunqid = $data['snsusersunqid'];
                $pass = base64_decode($data['password']);
                //in my table users, status must be 1 to login into app
                $matchWhere = ['username' => $email, 'password' => $pass, 'snsusersunqid' => $snsusersunqid];

                $user = \App\User::where($matchWhere)->UserActive()->first();

                if (!empty($user)) {

                    $user->firstname = ($data['firstname']) ?? $user->firstname;
                    $user->lastname = ($data['lastname']) ?? $user->lastname;
                    $user->email = ($data['email']) ?? $user->email;
                    $user->save();
                    //start session and save data
                    Auth::loginUsingId($user->recid);
                    return redirect('/');
                } else {
                    //not status 1 or active
                    $validator->errors()->add('username', 'These credentials do not match our records.');
                    return redirect('/login')->withErrors($validator)->withInput();
                }
            }
        } catch (\Exception $e) {
            //   return $e;
            return ['success' => 0, 'message' => 'oops something went wrong'];
        }
    }

    public function registerwebuser(Request $request) {
        DB::beginTransaction();
        try {
            $data = $request->all();

            $validator = $this->validator($data);

            //send error message
            if ($validator->fails()) {
                return redirect('/register')->withErrors($validator)->withInput();
            }


            $confirmation_code = str_random(30);

            User::create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'username' => $data['username'],
                'snsusersunqid' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
                'isconfirmed' => 0,
                'confirmation_code' => $confirmation_code
            ]);

            $message = 'Thanks for creating an account with the verification.<br>
            Please follow the link below to verify your email address. <br>' .
                    URL::to('register/verify/' . $confirmation_code);
            $mergevars = [
                'BODYEMAIL' => $message,
            ];
            $result = \Mail::send('auth.emailconfirm', [], function($message) use ($mergevars, $data) {
                        $headers = $message->getHeaders();
                        $headers->addTextHeader('X-MC-MergeVars', json_encode($mergevars));
                        $headers->addTextHeader('X-MC-Template', 'email-template-register');
                        $message->to($data['email'])->subject('Verify your email address');
                    });
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', 'Sorry something went wrong.');
            return Redirect::to('/login');
        }

        DB::commit();
        Session::flash('success', 'Thanks for signing up! Please check your email.');
        return Redirect::to('/login');
    }

    public function confirm($confirmation_code) {

        try {
        $user = User::whereConfirmationCode($confirmation_code)->first();
        if (!empty($user)) {

            $user->isconfirmed = 1;
            $user->confirmation_code = null;
            $user->save();

            Session::flash('success', 'You have successfully verified your account.');
            return Redirect::to('/login');
        }
        Session::flash('error', 'Sorry code which you entered has expired.');
        return Redirect::to('/login');
        }catch (\Exception $e) {
        Session::flash('error', 'Opps something went wrong.');
        return Redirect::to('/login');
        }
    }

}

//http://localhost/sns/user/login?username=AWS&password=TGF3V2FyZQ==&firstname=tes&lastname=test&snsusersunqid=AWS
//http://52.66.93.186/sns/user/login?username=AWS&password=TGF3V2FyZQ==&firstname=tes&lastname=test&snsusersunqid=AWS&email=test@test.com