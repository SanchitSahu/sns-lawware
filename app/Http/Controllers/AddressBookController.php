<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\MasterCountry;
use App\Addressbook;
use App\AddressbookDetail;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Http\Request;
use App\Contact;
use Session;
use Illuminate\Support\Facades\Redirect;
use DB;
use Excel;
use URL;
use Auth;

class AddressBookController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['data_list'] = Addressbook::where('status','1')->get();
        //echo '<pre>';print_r($data);exit;
        return view('addressbook.index',$data);
    }
    
    public function addressbookContact($id) {
        $data = AddressbookDetail::getContact($id);
        $arr_ids = [];
        foreach($data as $row){
            $arr_ids[] = $row->cardcode;
        }
        //print_r($arr_ids);exit;
        
        $result1 = DB::connection('mysql3')->table('card')->select(DB::raw('card.cardcode AS cardCode, casecard.caseno AS caseNo, CONCAT_WS(" ", card.salutation,card.first, card.middle, card.last) AS fullName, card.email AS email, IF(card.business <> "", card.business, IF(card.car <> "", card.car, IF(card.home <> "", card.home, IF(card2.phone1 <> "", card2.phone1, IF(card2.phone2 <> "", card2.phone2, "" ) ) ) ) ) AS phoneNumber, card.salutation AS salutation'))
                ->join('card2', 'card.firmcode', '=','card2.firmcode')
                ->join('casecard','casecard.cardcode','=','card.cardcode')
                ->whereIn('card.cardcode', $arr_ids)
                ->groupby('card.cardcode')
                ->havingRaw('fullName <> "" AND (email <> "" OR phoneNumber <> "") ')
                ->get(); 
     
        $result2 = DB::table('contact_list')->select(DB::raw('sns_contact_list.cardcode AS cardCode, sns_contact_list.case AS caseNo, CONCAT_WS(" ", sns_contact_list.salutation,sns_contact_list.firstname, sns_contact_list.middlename, sns_contact_list.lastname) AS fullName, sns_contact_list.email AS email, IF(sns_master_countries.rec_id < "", CONCAT_WS("-", sns_master_countries.countrycode, sns_contact_list.phonenumber), sns_contact_list.phonenumber) AS phoneNumber, sns_contact_list.salutation AS salutation'))
                ->leftjoin('master_countries','contact_list.country_id','=','master_countries.rec_id')
                ->whereIn('cardcode', $arr_ids)
                ->where('master_countries.isactive','A')
                ->where('master_countries.isdeleted','0')
                ->where('contact_list.isactive','A')
                ->where('contact_list.isdeleted','0')
                // ->union($result_n)
                ->get();
     
        $result = $result1->merge($result2);

        return $result;
        
    }

    public function addContact($addid, $cardcode) {
        $data = new AddressbookDetail();
        $data->addid =  $addid;
        $data->cardcode = $cardcode;
        $data->save();
        return 'success';
    }
    
    public function deleteContact($addid, $cardcode) {
        $data = AddressbookDetail::deleteContact($addid, $cardcode);
        return 'success';
    }
    
    public function addNewAddressBook(request $request){
        $data = new Addressbook();
        $data->name = $request->input('name');
        $data->created_at = date("Y-m-d H:i");
        $data->updated_at = date("Y-m-d H:i");  
        $data->save();
        return 'success';
    }
    
    public function allAddressBook(){
        $data = Addressbook::where('status','1')->get();
        return $data;
    }
    
    public function deleteAddressBook($recid){
        $data = Addressbook::where('recid', $recid)->update(['status'=>'0']);
        return $data;
    }
    
    public function addressBookContactInReminder($addid){
        
        $data = AddressbookDetail::getContact($addid);
        $arr_ids = [];
        foreach($data as $row){
            $arr_ids[] = $row->cardcode;
        }
        
        $result1 = DB::connection('mysql3')->table('card')->select(DB::raw('card.cardcode id, card.cardcode cardCode, casecard.caseno caseNo, card.salutation salutation, card.first firstName, card.middle middleName, card.last lastName, CONCAT_WS(" ", card.salutation, card.first, card.last) fullName, card.email email, IF(card.business <> "", card.business, IF(card.car <> "", card.car, IF(card.home <> "", card.home, IF(card2.phone1 <> "", card2.phone1, IF(card2.phone2 <> "", card2.phone2, "" ) ) ) ) ) phoneNumber,1 as firstnamestart,1 as middlenamestart,1 as lastnamestart, "LawwareOTS" contactType'))
                ->join('card2', 'card.firmcode', '=','card2.firmcode')
                ->join('casecard','casecard.cardcode','=','card.cardcode')
                ->whereIn('card.cardcode', $arr_ids)
                ->groupby('card.cardcode')
                ->havingRaw('fullName <> "" AND (email <> "" OR phoneNumber <> "") ')
                ->get(); 
     
        $result2 = DB::table('contact_list')->select(DB::raw('sns_contact_list.recid id, sns_contact_list.cardcode cardCode, sns_contact_list.case caseNo, sns_contact_list.salutation salutation, sns_contact_list.firstname firstName, sns_contact_list.middlename middleName, sns_contact_list.lastname lastName, CONCAT_WS(" ", sns_contact_list.salutation, sns_contact_list.firstname, sns_contact_list.lastname) fullName, sns_contact_list.email email, IF(sns_master_countries.rec_id > "", CONCAT_WS("-", sns_master_countries.countrycode, sns_contact_list.phonenumber), sns_contact_list.phonenumber) phoneNumber,1 as firstnamestart,1 as middlenamestart, 1 as lastnamestart, "SNS" contactType'))
                ->leftjoin('master_countries','contact_list.country_id','=','master_countries.rec_id')
                ->whereIn('cardcode', $arr_ids)
                ->where('master_countries.isactive','A')
                ->where('master_countries.isdeleted','0')
                ->where('contact_list.isactive','A')
                ->where('contact_list.isdeleted','0')
                // ->union($result_n)
                ->get();
        
        $result = $result1->merge($result2);

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => $result,
            'error' => 0,
            'flagMsg' => 'FETCH',
            'message' => 'Add reminder contacts fetch'
        );
        
        return $arrSendResponse;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

}
