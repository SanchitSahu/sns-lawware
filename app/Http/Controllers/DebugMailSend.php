<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Carbon\Carbon;

class DebugMailSend extends Controller
{
    public static function sendDebugMail($arrParams) {

        $FLAG_DEBUG_MAIL = env('FLAG_DEBUG_MAIL');

        if($FLAG_DEBUG_MAIL == 1 && false) {
            $subject = $arrParams['subject'];
            $arrData = $arrParams['arrData'];
            $mailBody = json_encode($arrData);

            $now = Carbon::now();
            $formatedDate = $now->format('Y/m/d H:i:s');

            $subject .= " : " . $formatedDate;

            $arrDataToPass = array();
            $arrDataToPass['subject'] = $subject;

            Mail::raw($mailBody, function ($message) use ($arrDataToPass){
                $message->to('milind@solulab.com')->subject($arrDataToPass['subject']);
            });
        }
    }
}
