<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\MasterReminderResponseColor;
use DB;
use Carbon\Carbon;

class TwiloLogController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $daterange = \App\TwiloLog::getlogDateRangeArray();
        $twiliologcolor = MasterReminderResponseColor::getTwilioColor();
        return view('twilolog.index')->with(['daterange' => $daterange,
                    'twiliologcolor' => $twiliologcolor]);
    }

    /**
     * get the list from master templates table.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function getlist(Request $request) {

        $request = $request->all();

        // get all the contacts
        $query=\App\TwiloLog::twiliolist($request);

//        $logs = \App\TwiloLog::with('twilioremindersync')
//       ->select(['twilio_log.recid', 'twilio_log.twiliostatus', 'tonumber', 'duration', 'startdate', 'typex'])
//        ;
        $datatables = Datatables::of($query)
                        ->editColumn('startdate', function ($user) {
                              $arrAdminSettings = \App\AdminSetting::getInfo();


                 if($arrAdminSettings->systemtimezone==24){
                            return $user->startdate ? Carbon::parse($user->startdate)->format('d/m/Y H:i') : '';
                  }else{
                       return $user->startdate ? Carbon::parse($user->startdate)->format('d/m/Y g:i a') : '';
                  }
                            })
                        ->setRowAttr([
                            'style' => function($user) {
                                $color = MasterReminderResponseColor::getTwilioColor();
                                if (array_key_exists(ucfirst($user->twiliostatus), $color)) {
                                    return "background-color:" . $color[ucfirst($user->twiliostatus)];
                                }
                                return '#fffff';
                            },
                        ])
                        ->filterColumn('caseno', function ($query, $keyword) {
                            $query->whereRaw("caseno like ?", ["%$keyword%"]);
                        })->filterColumn('reminderid', function ($query, $keyword) {
                            $query->whereRaw("reminderid like ?", ["%$keyword%"]);
                        })->filterColumn('firstname', function ($query, $keyword) {
                            $query->whereRaw("firstname like ?", ["%$keyword%"]);
                        })->filterColumn('lastname', function ($query, $keyword) {
                    $query->whereRaw("lastname like ?", ["%$keyword%"]);
                })->filterColumn('startdate', function ($query, $keyword) {
                    $query->whereRaw("DATE_FORMAT(startdate,'%d/%m/%Y %H:%i') like ?", ["%$keyword%"]);
                });

        // Global search function

        return $datatables->make(true);
    }
    
    public function addtwiliodate($id){
   
    $reminder= \App\TwiloLog::find($id)->twilioremindersync;      
    $newreminder = $reminder->replicate();
    $newreminder->remindereventdatetime =  Carbon::now()->addMinutes(2)->format('Y-m-d H:i'); // the new project_id
    $newreminder->sid='';
    $newreminder->confirmdate='';
    $newreminder->twiliostatus=0;
    $newreminder->save();
     
    }

}
