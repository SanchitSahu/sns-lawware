<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\MasterCountry;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Http\Request;
use App\Contact;
use Session;
use Illuminate\Support\Facades\Redirect;
use DB;
use Excel;
use URL;

class ContactController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('contacts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $phonetype = Contact::phonetype();
        $emailtype = Contact::emailtype();
        $salutation = Contact::salutationtype();
        $countrycode = MasterCountry::getcountry();

        return view('contacts.create')->with(['phonetype' => $phonetype, 'countrycode' => $countrycode,
                    'emailtype' => $emailtype, 'salutation' => $salutation]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactFormRequest $request) {

        // process the store
        $input = $request->all();
//        $contact = Contact::create($input);
//        $lastinsertedid = "RS" . $contact->recid;
//        $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
//        $contact->update($caseId);
        self::saveContact($input);
        // redirect
        Session::flash('message', 'Contact created successfully ');
        return Redirect::to('contacts');
    }

    public static function saveContact($input) {
        $contact = Contact::create($input);
        $lastinsertedid = "RS" . $contact->recid;
        $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
        $contact->update($caseId);
        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $contact = Contact::editcontacts($id);
        $phonetype = Contact::phonetype();
        $emailtype = Contact::emailtype();
        $salutation = Contact::salutationtype();
        $countrycode = MasterCountry::getcountry();
        // show the edit form and pass the contact
        return view('contacts.edit')
                        ->with(['contact' => $contact, 'phonetype' => $phonetype, 'countrycode' => $countrycode,
                            'emailtype' => $emailtype, 'salutation' => $salutation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(ContactFormRequest $contact, $id) {

        $request = $contact->all();
        $contacts = Contact::where(['cardcode' => $id])->first();

        if (!empty($contacts)) {
            $contacts->update($request);
        } else {
            $contact = Contact::create($request);
            $lastinsertedid = "RS" . $contact->recid;
            $caseId = ['case' => $lastinsertedid, 'cardcode' => $lastinsertedid, 'firmcode' => $lastinsertedid]; //put this value equal to datatable column name where it will be saved
            $contact->update($caseId);
        }
        // redirect
        Session::flash('message', 'Contact updated successfully!');
        return Redirect::to('contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $contacts = Contact::where(['cardcode' => $id])->first();
        $contacts->isdeleted = Contact::IS_DELETED;
        $contacts->save();
    }

    /**
     * get the list from contacts table.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function getlist() {

        $result = Contact::listcontacts();
        return Datatables::of($result)
                        ->filterColumn('notes', function ($query, $keyword) {
                            $query->whereRaw("notes like ?", ["%$keyword%"]);
                        })
                        ->make(true);
    }

    public function searchForContact(Request $request, $queryString, $searchType) {
        $queryString = trim($queryString);
        // echo $searchType;exit;

//        if($searchType === "participant") {}    
        if ($searchType == "case") {
//            DB::enableQueryLog();
            $result = DB::connection('mysql3')->select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                                    FROM (
                                        (
                                            SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,1 as firstnamestart,1 as middlenamestart,1 as lastnamestart, "LawwareOTS" contactType
                                            FROM card c, card2 c2, casecard cc
                                            WHERE c.firmcode = c2.firmcode
                                                AND cc.cardcode = c.cardcode
                                                /*AND (CONCAT("",c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.last,c.first) LIKE "%' . $queryString . '%" '
                            . '                         OR CONCAT("",c.salutation,c.first,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.salutation,c.last,c.first) LIKE "%' . $queryString . '%" '
                            . '                         OR CONCAT("",c.salutation,c.first,c.middle,c.last) LIKE "%' . $queryString . '%" OR CONCAT("",c.first,c.middle,c.last) LIKE "%' . $queryString . '%"  OR c.first LIKE "' . $queryString . '%" OR c.middle LIKE "' . $queryString . '%" OR c.last LIKE "' . $queryString . '%" )*/
                                            AND cc.caseno = "' . $queryString . '"
                                                AND (c.first <> "" AND IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) <> "")
                                            GROUP BY c.cardcode
                                        )
                                    ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');
//            $db = DB::getQueryLog();
        } else {
            $result = self::fetchCardDetailsQuery($queryString);
        }

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => $result,
            'error' => 0,
            'flagMsg' => 'FETCH',
            'message' => 'Add reminder contacts fetch'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public static function fetchCardDetailsQuery($queryString) {
        // echo $queryString; exit;
        $lawwareContacts = DB::connection('mysql3')->select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                    FROM (SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,IF(c.first LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(c.middle LIKE "' . $queryString . '%", 1, 0) middlenamestart,IF(c.last LIKE "' . $queryString . '%", 1, 0) lastnamestart, "OTS" contactType
                                            FROM card c, card2 c2, casecard cc
                                            WHERE c.firmcode = c2.firmcode
                                                AND cc.cardcode = c.cardcode
                                                AND (CONCAT("",trim(c.first),trim(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",trim(c.last),trim(c.first)) LIKE "%' . $queryString . '%" '
                        . '                         OR CONCAT("",trim(c.salutation),trim(c.first),trim(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",trim(c.salutation),trim(c.last),trim(c.first)) LIKE "%' . $queryString . '%" '
                        . '                         OR CONCAT("",trim(c.salutation),trim(c.first),trim(c.middle),trim(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",trim(c.first),trim(c.middle),trim(c.last)) LIKE "%' . $queryString . '%"  OR trim(c.first) LIKE "' . $queryString . '%" OR trim(c.middle) LIKE "' . $queryString . '%" OR trim(c.last) LIKE "' . $queryString . '%" OR trim(cc.caseno) LIKE "%' . $queryString . '%" OR trim(c.email) LIKE "%' . $queryString . '%" OR trim(c.home) LIKE "%' . $queryString . '%" OR trim(c.business) LIKE "%' . $queryString . '%" OR trim(c.fax) LIKE "%' . $queryString . '%" OR trim(c.car) LIKE "%' . $queryString . '%")
                                            GROUP BY c.cardcode) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');
        
        $snsContacts = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                    FROM (SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,IF(cl.firstname LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(cl.middlename LIKE "' . $queryString . '%", 1, 0) middlenamestart, IF(cl.lastname LIKE "' . $queryString . '%", 1, 0) lastnamestart, "SNS" contactType
                                            FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
                                            WHERE cl.isdeleted = "0" AND ( CONCAT("",cl.firstname,cl.lastname) LIKE "%' . $queryString . '%" OR CONCAT("",cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
                        . '        OR CONCAT("",cl.salutation,cl.firstname,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.salutation,cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
                        . 'OR CONCAT("",cl.salutation,cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"   OR cl.firstname LIKE "' . $queryString . '%" OR cl.middlename LIKE "' . $queryString . '%" OR cl.lastname LIKE "' . $queryString . '%" OR cl.case LIKE "%' . $queryString . '%"  OR cl.notes LIKE "%' . $queryString . '%" OR cl.phonenumber LIKE "%' . $queryString . '%" OR cl.email LIKE "%' . $queryString . '%")) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC'
                    );
        
        $result = array_merge($lawwareContacts, $snsContacts);
        
        // $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
        //                             FROM (
        //                                 (
        //                                     SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,IF(c.first LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(c.middle LIKE "' . $queryString . '%", 1, 0) middlenamestart,IF(c.last LIKE "' . $queryString . '%", 1, 0) lastnamestart, "OTS" contactType
        //                                     FROM card c, card2 c2, casecard cc
        //                                     WHERE c.firmcode = c2.firmcode
        //                                         AND cc.cardcode = c.cardcode
        //                                         AND (CONCAT("",trim(c.first),trim(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",trim(c.last),trim(c.first)) LIKE "%' . $queryString . '%" '
        //                 . '                         OR CONCAT("",trim(c.salutation),trim(c.first),trim(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",trim(c.salutation),trim(c.last),trim(c.first)) LIKE "%' . $queryString . '%" '
        //                 . '                         OR CONCAT("",trim(c.salutation),trim(c.first),trim(c.middle),trim(c.last)) LIKE "%' . $queryString . '%" OR CONCAT("",trim(c.first),trim(c.middle),trim(c.last)) LIKE "%' . $queryString . '%"  OR trim(c.first) LIKE "' . $queryString . '%" OR trim(c.middle) LIKE "' . $queryString . '%" OR trim(c.last) LIKE "' . $queryString . '%" OR trim(cc.caseno) LIKE "%' . $queryString . '%")
        //                                     GROUP BY c.cardcode
        //                                 ) 

        //                                 UNION

        //                                 (
        //                                     SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,IF(cl.firstname LIKE "' . $queryString . '%", 1, 0) firstnamestart,IF(cl.middlename LIKE "' . $queryString . '%", 1, 0) middlenamestart, IF(cl.lastname LIKE "' . $queryString . '%", 1, 0) lastnamestart, "SNS" contactType
        //                                     FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
        //                                     WHERE cl.isdeleted = "0" AND ( CONCAT("",cl.firstname,cl.lastname) LIKE "%' . $queryString . '%" OR CONCAT("",cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
        //                 . '        OR CONCAT("",cl.salutation,cl.firstname,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.salutation,cl.lastname,cl.firstname) LIKE "%' . $queryString . '%"'
        //                 . 'OR CONCAT("",cl.salutation,cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"  OR CONCAT("",cl.firstname,cl.middlename,cl.lastname) LIKE "%' . $queryString . '%"   OR cl.firstname LIKE "' . $queryString . '%" OR cl.middlename LIKE "' . $queryString . '%" OR cl.lastname LIKE "' . $queryString . '%" OR cl.case LIKE "%' . $queryString . '%"  OR cl.notes LIKE "%' . $queryString . '%" ) 
        //                                 ) 

        //                             ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');                
        return $result;
    }

    public function importExportExcelORCSV() {
        return view('file_import_export');
    }

    public function importFileIntoDB(Request $request) {
        
        try {
            $request = $request->all();

            $validatorfile = Validator::make($request, [
                        'import_contact' => 'required',
            ]);

            if ($validatorfile->fails()) {
                return ['error' => 2, 'errormessage' => 'Please select appropriate file'];
            }
            $file = $request['import_contact'];

            if (!in_array($file->getMimeType(), ['application/vnd.ms-excel', 'text/csv', 'text/plain'])) {
                return ['error' => 2, 'errormessage' => ' Please import only CSV file !!'];
            }
            if ($file) {
                $path = $file->getRealPath();
                $data = Excel::load($path)->get();
                if ($data->count()) {
                    return Contact::addCsvUpload($data->toArray());
                }
            }

        } catch (\Exception $e) {
            //throw $e;
            return ['error' => 2, 'errormessage' => 'please attach proper format'];
        }
    }

    public function downloadSampleExcelFile() {

        return Excel::load('storage/exports/Synergy Notification System1.xlsx', function($reader) {
                    
                })->download('csv');
    }

    public function downloadExcelFile() {

        return Excel::load('storage/exports/synergynotification.csv', function($reader) {
                    
                })->download('csv');
    }

    /* public function removecontacts(Request $request){
      $ids="'". implode("','",$request->checkids)."'";
      $result = DB::update("update sns_contact_list set isdeleted ='1' where cardcode in($ids)");
      if ($result) {
      die(json_encode($result));
      } else {
      die(json_encode($result));
      }
      exit;
      } */

    public function ajaxSearchForCaseNo(Request $request) {
        $query = $request->get('term', '');
        $queryString = $query;

        $result = DB::select("SELECT caseno as value, yourfileno as id FROM `case` as c WHERE c.caseno LIKE '%" . $queryString . "%'");
        return json_encode($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function quickContactStore(request $request) {
        $input = $request->all();
        $result = self::checkEmailPhoneDuplication($input);
        if (!$result) {
            // process the store
            self::saveContact($input);
            $result = self::checkEmailPhoneDuplication($input);
        }

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => $result,
            'error' => 0,
            'flagMsg' => 'FETCH',
            'message' => 'Add reminder contacts fetch'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public static function checkEmailPhoneDuplication($request) {
        $contactNumber = $request['phonenumber'];
        $email = $request['email'];

        $result = DB::select('SELECT id, cardCode, caseNo, salutation, firstName, middleName, lastName, fullName, email, phoneNumber, firstnamestart, middlenamestart , lastnamestart, contactType
                                    FROM (
                                        /*(
                                            SELECT c.cardcode id, c.cardcode cardCode, cc.caseno caseNo, c.salutation salutation, c.first firstName, c.middle middleName, c.last lastName, CONCAT_WS(" ", c.salutation, c.first, c.last) fullName, c.email email, IF(c.business <> "", c.business, IF(c.car <> "", c.car, IF(c.home <> "", c.home, IF(c2.phone1 <> "", c2.phone1, IF(c2.phone2 <> "", c2.phone2, "" ) ) ) ) ) phoneNumber,1 as firstnamestart,1 as middlenamestart,1 as lastnamestart, "OTS" contactType
                                            FROM card c, card2 c2, casecard cc
                                            WHERE c.firmcode = c2.firmcode
                                                AND cc.cardcode = c.cardcode
                                                AND ((ExtractNumber(c.car) = "' . $contactNumber . '" OR ExtractNumber(c.home) = "' . $contactNumber . '" OR ExtractNumber(c2.phone2) = "' . $contactNumber . '" OR ExtractNumber(c2.phone1) = "' . $contactNumber . '") OR email = "' . $email . '")
                                            GROUP BY c.cardcode
                                        ) 

                                        UNION*/

                                        (
                                            SELECT cl.recid id, cl.cardcode cardCode, cl.case caseNo, cl.salutation salutation, cl.firstname firstName, cl.middlename middleName, cl.lastname lastName, CONCAT_WS(" ", cl.salutation, cl.firstname, cl.lastname) fullName, cl.email email, IF(smc.rec_id > "", CONCAT_WS("-", smc.countrycode, cl.phonenumber), cl.phonenumber) phoneNumber,1 as firstnamestart,1 as middlenamestart, 1 as lastnamestart, "SNS" contactType
                                            FROM sns_contact_list cl LEFT JOIN sns_master_countries smc ON cl.country_id = smc.rec_id AND smc.isactive = "A"
                                            WHERE cl.isdeleted = "0" AND ( ExtractNumber(phonenumber) = "' . $contactNumber . '" AND email = "' . $email . '" ) 
                                        ) 

                                    ) AS t2 ORDER BY firstnamestart DESC, middlenamestart DESC, lastnamestart DESC');

        return $result;
    }

}
