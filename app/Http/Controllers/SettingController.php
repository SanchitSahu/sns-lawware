<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
//use Illuminate\Support\Facades\Storage;
use App\MasterReminderResponseColor;
use App\TwilioLanguage;
use App\AdminSetting;
use Image;
use File;
use Storage;
use App\Http\Requests\SettingRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller {
    
    public function index() {

        if(Auth::user()->role != 'admin'){
            return view('NotAllowed');
        }
        
        $appointmentColor = MasterReminderResponseColor::getResponseColors();

        $twilioLogColor = MasterReminderResponseColor::getTwilioLogColor();

        $twilioLanguage = TwilioLanguage::getlanguage();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();

        $arrAdminSettings = AdminSetting::getInfo();

        $arrTimeZoneList = AdminSetting::getTimeZoneList();
        $arrRecallTimes = AdminSetting::getRecallTimes();
        $arrRecalIntervals = AdminSetting::getRecalIntervals();
        $arrSettings = AdminSetting::find(1);
        $arrResponseToSend = array(
            'appointmentColor' => $appointmentColor,
            'twilioLogColor' => $twilioLogColor,
            'twilioLanguage' => $twilioLanguage,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'arrAdminSettings' => $arrAdminSettings,
            'arrTimeZoneList' => $arrTimeZoneList,
            'arrRecallTimes' => $arrRecallTimes,
            'arrRecalIntervals' => $arrRecalIntervals,
            'arrSettings' => $arrSettings
        );
        // echo "<pre>"; print_r($arrAdminSettings); exit();
        return view('settings')->with($arrResponseToSend);
    }

    public function updateLogColors(Request $request) {

        $colorCode = $request->input('colorCode');
        $colorId = $request->input('colorId');

        $arrDataParams = array(
            'colorCode' => $colorCode,
            'colorId' => $colorId
        );

        MasterReminderResponseColor::updateColours($arrDataParams);

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'UPDATE',
            'message' => 'Color Log Updated Successfully'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public function updateTimeZone(Request $request) {
        $lstTimeZoneDropDown = $request->input('lstTimeZoneDropDown');

        $arrDataParams = array(
            'lstTimeZoneDropDown' => $lstTimeZoneDropDown
        );

        AdminSetting::updateTimeZone($arrDataParams);

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'UPDATE',
            'message' => 'Timezone Updated Successfully'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public function updateRecallSettings(Request $request) {
        $recallEnable = $request->input('recallEnable');
        $howManyTimes = $request->input('howManyTimes');
        $timingInterval = $request->input('timingInterval');

        $arrDataParams = array(
            'recallEnable' => $recallEnable,
            'howManyTimes' => $howManyTimes,
            'timingInterval' => $timingInterval
        );

        AdminSetting::updateRecallSettings($arrDataParams);

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'UPDATE',
            'message' => 'Recall settings Updated Successfully'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    //update csv settings
    public function updateCsvSettings(Request $request) {
        $request = $request->all();
        AdminSetting::updateCsvUpload($request);

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'UPDATE',
            'message' => 'Csv Auto Upload Updated Successfully'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public function updateCompanyLogoAndEmailSignature(Request $request) {
        $taEmailSignature = $request->input('taEmailSignature');
        $taCompanyName = $request->input('companyname');
        $flCompanyLogo = "";
        $fileName = "";
        if ($request->hasFile('flCompanyLogo')) {
            $globStoragePath = config('filesystems.disks.local.root');
            // check if origimg folder is available or not!
            self::checkCreatePhysicalPath('/public/origimg/');

            // check if resizimg  folder is available or not!
            self::checkCreatePhysicalPath('/public/resizimg/');

            $arrOldImageParameters = AdminSetting::getInfo();
            $varOldCompanyLogo = $arrOldImageParameters->companylogo;
            $clientKey = $arrOldImageParameters->clientkey;

            if (!empty($varOldCompanyLogo)) {
                $arrFilesToDelete = array(
//                    'public/storage/origimg/' . $varOldCompanyLogo,
//                    'public/storage/resizimg/' . $varOldCompanyLogo
                    $globStoragePath . '/public/origimg/' . $varOldCompanyLogo,
                    $globStoragePath . '/public/resizimg/' . $varOldCompanyLogo
                );
                File::delete($arrFilesToDelete);
            }

            $dt = Carbon::now();
            $timestamp = $dt->timestamp;

//            $fileNameToBeSaved = "company_logo_" . Str::slug($clientKey) . '.jpg';
            $fileNameToBeSaved = "company_logo_" . Str::slug($clientKey) . '_' . $timestamp . '.jpg';

            $flCompanyLogo = $request->file('flCompanyLogo')->storeAs('public/origimg', $fileNameToBeSaved);

            $arrImagePathComponent = explode('/', $flCompanyLogo);

            $fileName = $arrImagePathComponent[2];

            $img = Image::make($request->file('flCompanyLogo')->getRealPath());

            // prevent possible upsizing
            $img->resize(null, 250, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // save the same file as jpg with default quality
//            $img->save('storage/app/public/resizimg/' . $fileName, 100);
            $img->save($globStoragePath . '/public/resizimg/' . $fileName, 100);
        }


        $arrDataParams = array(
            'fileName' => $fileName,
            'taEmailSignature' => $taEmailSignature,
            'taCompanyName' => $taCompanyName,
        );

        AdminSetting::updateCompanyLogoAndEmailSignature($arrDataParams);


        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(
                'flCompanyLogo' => $flCompanyLogo
            ),
            'error' => 0,
            'flagMsg' => 'UPDATE',
            'message' => 'Company Logo and Email signature Updated Successfully'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public function updateEmailSettings(SettingRequest $request) {

        $input = $request->all();
        $settings = AdminSetting::find(1);

        $update = AdminSetting::updatedata($input, $settings);
        if ($update) {
            return ['message' => 'Settings Updated Successfully'];
        }
        return ['message' => 'oops something went wrong'];
    }

    //update csv settings
    public function addPromotionalMessage(Request $request) {
        DB::beginTransaction();
        try {
            $request = $request->all();
            $validatorfile = Validator::make($request, [
                        'flagcsv' => 'required|mimes:csv,txt',
            ]);
            if ($validatorfile->fails()) {
                return ['error' => 1, 'errormessage' => 'Please upload csv file!!'];
            }

            if ($request['flagcsv']) {
                $file = $request['flagcsv'];
                $path = $file->getRealPath();
                $data = Excel::load($path)->noHeading()->get();
                if ($data->count()) {
                    \App\PromotionalMessage::addPromotionalData($data->toArray());
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return ['error' => 1, 'message' => 'oops something went wrong'];
        }

        // If we reach here, then// data is valid and working.//
        DB::commit();
        return ['success' => 1, 'message' => 'Settings Updated Successfully'];
    }

    public function exportReport(Request $request) {

        $request = $request->all();
        $validator = Validator::make($request, [
                    'fromdate' => 'required|date|before_or_equal:todate',
                    'todate' => 'required|date',
        ]);
        if ($validator->fails()) {
            return ['error' => 1, 'message' => $validator->errors()];
        }
        $fromdate = Carbon::parse($request['fromdate'])->format('Y-m-d');
        $todate = Carbon::parse($request['todate'])->format('Y-m-d');
        $data = \App\ReportModel::getExportData($fromdate, $todate);
        $totaldata = \App\ReportModel::getTotalData($fromdate, $todate);

        Excel::create('MonthlyUsageReport' . Carbon::now()->format('Ymd'), function($excel) use ($data, $totaldata) {
            $excel->sheet('MonthlyUsageReport' . Carbon::now()->format('Ymd'), function($sheet) use ($data, $totaldata) {
                $sheet->fromArray($data, '', '', true);
                $sheet->row(1, ['CREATED USER', 'CALL', 'SMS', 'EMAIL', 'TOTAL REMINDER', 'CANCELLABLE', 'PRIVATE REMINDER', 'CANCEL BY USER', 'CONFIRM BY USER', 'PERCENTAGE']);
                $sheet->appendRow(['Total:', $totaldata[0]['totalcalls'],
                    $totaldata[0]['totalsms'], $totaldata[0]['totalemail'],
                    $totaldata[0]['totalreminders'], $totaldata[0]['cancellable'],
                    $totaldata[0]['private'], $totaldata[0]['tcanclebyuser'],
                    $totaldata[0]['tconfirmbyuser'], $totaldata[0]['percentage']]);
            });
        })->store('csv');
//                  echo url('/storage/exports/MonthlyUsageReport'.Carbon::now()->format('Ymd').'.csv');exit;
//        return ['url'=> config('app.url') . '/storage/exports/MonthlyUsageReport'.Carbon::now()->format('Ymd').'.csv'];
        return ['url' => url('/storage/exports/MonthlyUsageReport' . Carbon::now()->format('Ymd') . '.csv')];
    }

    /**
     * added to save default language 
     */
    public function updateDefaultLanguageSettings(Request $request) {
        $languageId = $request->input('defaultLanguage');

        $arrDataParams = array(
//            'isDefaultLang' => '1',
            'languageId' => $languageId
        );

        TwilioLanguage::updateDefaultLanguage($arrDataParams);

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'UPDATE',
            'message' => 'Default Language Updated Successfully'
        );

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    public function updateparalegalhandling(Request $request) {
        $plh = ($request->onoffparalegal == '0') ? '1' : '0';
        $settingid = $request->settingsid;
        $result = DB::update("update sns_admin_settings set onoffparalegal=$plh where recid='" . $settingid . "' ");
        if ($result) {
            return json_encode(true);
        }
    }

    // function to add or remove solutation by sourabh
    public function updatesalutation(Request $request) {
        $sal = $request->onoffsalutation;
        $settingid = $request->settingsid;
        $result = DB::update("update sns_admin_settings set onoffsalutation=$sal where recid='" . $settingid . "' ");
        if ($result) {
            return json_encode(true);
        }
    }

    /* public function updatesyna1law(Request $request){
      $synca1law = ($request->synca1law == '0') ? '1' : '0' ;
      $settingid = $request->settingsid;
      $result = DB::update("update sns_admin_settings set synca1law=$synca1law where recid='".$settingid."' ");
      if($result){
      return json_encode(true);
      }

      } */

    public function updatesyncsnstoa1law(Request $request) {
//        $onoffsynca1law = ($request->onoffsynca1law == '0') ? '1' : '0';
        //$onoffsynca1law = ($request->onoffsynca1law == '0') ? '0' : '1';
        $onoffsynca1law = $request->onoffsynca1law ? '1' : '0';
        $settingid = $request->settingsid;
        $result = DB::update("update sns_admin_settings set onoffsynca1law=$onoffsynca1law where recid='" . $settingid . "' ");
        if ($result) {
            return json_encode(true);
        }
    }

    public function checkCreatePhysicalPath($dirName = "/public/") {
        $storageRootPath = config('filesystems.disks.local.root');
        $publicPath = $dirName;
        $directoryPath = $storageRootPath . $publicPath;
        if (\File::isDirectory($directoryPath)) {
            $path = true;
        } else {
            $path = \Storage::makeDirectory($publicPath);
        }
        return $path;
    }

}
