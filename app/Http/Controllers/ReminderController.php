<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;
use App\ReminderSlots;
use App\MasterTemplates;
use App\TwilioLanguage;
use App\Http\Requests\ReminderForm;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\MasterReminderResponseColor;
use Datatables;
use Auth;
use App\AdminSetting;
use App\calaudit1;
use App\User;
use App\Addressbook;

class ReminderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $arrDateRange = ReminderController::getDateRangeArray();
        $arrAppointmentColor = MasterReminderResponseColor::getResponseColors();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();
        $arrAdminSettings = AdminSetting::getInfo();
        $arrResponseToSend = array(
            'arrDateRange' => $arrDateRange,
            'arrAppointmentColor' => $arrAppointmentColor,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'arrAdminSettingsparalegal' => $arrAdminSettings->onoffparalegal,
            'arrAdminSettingssalutation' => $arrAdminSettings->onoffsalutation
            // added by sourabh for salutation
        );
       //echo '<pre>';print_r($arrResponseToSend);exit;
        return view('reminders/home')->with($arrResponseToSend);
    }

    public static function getDateRangeArray() {
        return array(
            'UP' => 'Upcoming',
            'TD' => 'Today',
            'TO' => 'Tomorrow',
            'PW' => 'Past Week',
            'SD' => 'Specific Date',
            'CR' => 'Custom Range'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($strUserSearch = "") {
        $view = 0;
        $arrSlots = ReminderSlots::getslots();
        $arrTemplate = MasterTemplates::all();
        $flagcolumn = ['SMS', 'CALL', 'EMAIL'];
        $language = \App\TwilioLanguage::getlanguage();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();
        $fields = \App\ReminderFields::getfields();
        $slots = \App\ReminderSlots::getslots();
        $addressbook = Addressbook::where('status','1')->get();
        $column = [0, 1, 2, 3];
        $countrycode = \App\MasterCountry::getcountry();

        $arrResponseToSend = array(
            'arrSlots' => $arrSlots,
            'arrTemplate' => $arrTemplate,
            'flagcolumn' => $flagcolumn,
            'language' => $language,
            'fields' => $fields,
            'slots' => $slots,
            'column' => $column,
            'strUserSearch' => $strUserSearch,
            'view' => $view,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'addressbook' => $addressbook,
            'countrycode' => $countrycode
        );

        return view('reminders/addreminder')->with($arrResponseToSend);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReminderForm $request) {
        return ReminderController::addUpdateReminderDetails($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function edit($reminderId) {
        $view = 0;
        //view data in reminder
        $parts = (\Request::path());
        $ex = explode("/", $parts);

        if (isset($ex[2])) {
            $view = 1;
        }
        $arrSlots = ReminderSlots::getslots();
        $arrTemplate = MasterTemplates::all();
        $flagcolumn = ['SMS', 'CALL', 'EMAIL'];
        $language = \App\TwilioLanguage::getlanguage();
        $twilioDefaultLanguage = TwilioLanguage::getDefaultLanguage();
        $fields = \App\ReminderFields::getfields();
        $slots = \App\ReminderSlots::getslots();
        $column = [0, 1, 2, 3];
        $countrycode = \App\MasterCountry::getcountry();

        $arrReminderDetail = Reminder::getReminderDetails($reminderId);

        $arrSelectedUser = array();

        $arrSelectedUserInfo = array();
        $arrSelectedUserInfo['cardCode'] = $arrReminderDetail['cardcode'];
        $arrSelectedUserInfo['caseNo'] = $arrReminderDetail['caseno'];
        $arrSelectedUserInfo['contactType'] = $arrReminderDetail['contacttype'];
        $arrSelectedUserInfo['salutation'] = $arrReminderDetail['salutation'];
        $arrSelectedUserInfo['firstName'] = $arrReminderDetail['firstname'];
        $arrSelectedUserInfo['lastName'] = $arrReminderDetail['lastname'];
        $arrSelectedUserInfo['fullName'] = $arrReminderDetail['fullName'];
        $arrSelectedUserInfo['email'] = $arrReminderDetail['email'];
        $arrSelectedUserInfo['phoneNumber'] = $arrReminderDetail['phoneno'];

        array_push($arrSelectedUser, $arrSelectedUserInfo);

        $jsonSelectedUser = json_encode($arrSelectedUser);

        $arrResponseToSend = array(
            'arrSlots' => $arrSlots,
            'arrTemplate' => $arrTemplate,
            'flagcolumn' => $flagcolumn,
            'language' => $language,
            'fields' => $fields,
            'slots' => $slots,
            'column' => $column,
            'arrReminderDetail' => $arrReminderDetail,
            'reminderId' => $reminderId,
            'arrSelectedUserInfo' => $arrSelectedUserInfo,
            'jsonSelectedUser' => $jsonSelectedUser,
            'view' => $view,
            'twilioDefaultLanguage' => $twilioDefaultLanguage,
            'countrycode' => $countrycode
        );

        return view('reminders/editreminder')->with($arrResponseToSend);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reminder $reminder) {
        return ReminderController::addUpdateReminderDetails($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $reminder) {
        //
    }
    
    public static function checkImpromptuValues($request) {
        $chkImpromptu = $request->input('chkImpromptu');
        $txtCaseNo = $request->input('txtCaseNo');
        $flagSyncA1Law = 0;
        if((isset($chkImpromptu) && !empty($chkImpromptu)) && (isset($txtCaseNo) && !empty($txtCaseNo))) {
            $flagSyncA1Law = 1;
        }
//        echo  $flagSyncA1Law;exit;  
        echo "<pre>";print_r($request->all());exit;
    }

    public static function addUpdateReminderDetails($request) {
        //echo '<pre>';print_r($request->input());exit;
        DB::beginTransaction();
//        self::checkImpromptuValues($request);
//echo "<pre>";print_r($request->all());exit;
        try {

            $strResponseMessage = "Reminder Added Succesfully";
            $txtReminderTitle = $request->input('txtReminderTitle') ?? $request->input('txtAppointmentType');
            $txtDate = $request->input('txtDate');
            $txtTime = $request->input('txtTime');
//            $reminderDateTimeFrm = $txtDate . " " . $txtTime . ":00";
            $reminderDateTimeFrm = $txtDate . " " . $txtTime;
            $finalDate = Carbon::parse($reminderDateTimeFrm);
            $reminderDateTime = (string) $finalDate;

            $lstTemplates = $request->input('lstTemplates');
            $txtAppointmentType = $request->input('txtAppointmentType');
            $lstLanguage = $request->input('lstLanguage');
            $taAppoinmentTemplateBody = $request->input('taAppoinmentTemplateBody');
//            $chkIsReschedule = $request->input('chkIsReschedule');
            $chkMakePrivate = $request->input('chkMakePrivate');
            $arrTimeSlot = $request->input('chkFlagTimeSlot');
            $selectedUsersData = $request->input('selectedUsersData');

            $reminderId = $request->input('hidReminderId');

            if (!is_numeric($reminderId) || $reminderId <= 0) {
                $reminderId = 0;
            }

            $arrUsersData = json_decode($selectedUsersData);


            // checking for slots;
            $chkIsReschedule = "N";
            if ($request->input('chkIsReschedule')) {
               $chkIsReschedule = "Y";
            }
            
            $chkIsCallback = "N";
            if ($request->input('chkIsCallback')) {
               $chkIsCallback = "Y";
            }
            
            $chkIsDisable = "N";
            if ($request->input('chkIsDisable')) {
                $chkIsDisable = "Y";
            }
            
            
            $flagSMS = "N";
            $strSMSTimingIds = "";
            if (isset($arrTimeSlot['SMS']) && !empty($arrTimeSlot['SMS'])) {
                $flagSMS = "Y";
                $strSMSTimingIds = implode(',', $arrTimeSlot['SMS']);
            }

            $flagCALL = "N";
            $strCALLTimingIds = "";
            if (isset($arrTimeSlot['CALL']) && !empty($arrTimeSlot['CALL'])) {
                $flagCALL = "Y";
                $strCALLTimingIds = implode(',', $arrTimeSlot['CALL']);
            }

            $flagEMAIL = "N";
            $strEMAILTimingIds = "";
            if (isset($arrTimeSlot['EMAIL']) && !empty($arrTimeSlot['EMAIL'])) {
                $flagEMAIL = "Y";
                $strEMAILTimingIds = implode(',', $arrTimeSlot['EMAIL']);
            }
            
//            $chkImpromptu = 0;
            $flagsyncbacktoa1law = 0;
//            if(isset($request->input('chkImpromptu'))) {
////                $chkImpromptu = 1;
//                $flagsyncbacktoa1law = 1;
//            }

            // if reminder id is zero, that means it is an add case.
            if ($reminderId == 0) {
                if (COUNT($arrUsersData) > 0) {
                    foreach ($arrUsersData AS $userIndex => $arrEachUserData) {

                        $arrParamsToCreateReminder = array();
                        $arrParamsToCreateReminder['snsusersid'] = Auth::id();
                        $arrParamsToCreateReminder['cardcode'] = $arrEachUserData->cardCode;
                        $arrParamsToCreateReminder['caseno'] = $arrEachUserData->caseNo;
                        $arrParamsToCreateReminder['contacttype'] = $arrEachUserData->contactType;
                        $arrParamsToCreateReminder['salutation'] = $arrEachUserData->salutation;
                        $arrParamsToCreateReminder['firstname'] = $arrEachUserData->firstName;
                        $arrParamsToCreateReminder['lastname'] = $arrEachUserData->lastName;
                        $arrParamsToCreateReminder['fullname'] = $arrEachUserData->fullName;
                        $arrParamsToCreateReminder['reminderdatetime'] = $reminderDateTime;
                        $arrParamsToCreateReminder['title'] = $txtReminderTitle;
                        $arrParamsToCreateReminder['purpose'] = $txtAppointmentType;
                        $arrParamsToCreateReminder['templateid'] = $lstTemplates;
                        $arrParamsToCreateReminder['email'] = strtolower($arrEachUserData->email);
                        $arrParamsToCreateReminder['phoneno'] = (new self)->formatphone($arrEachUserData->phoneNumber);
                        $arrParamsToCreateReminder['bodyemail'] = $taAppoinmentTemplateBody;
                        $arrParamsToCreateReminder['bodysms'] = $taAppoinmentTemplateBody;
                        $arrParamsToCreateReminder['languageid'] = $lstLanguage;
                        $arrParamsToCreateReminder['flagsms'] = $flagSMS;
                        $arrParamsToCreateReminder['smstimingid'] = $strSMSTimingIds;
                        $arrParamsToCreateReminder['flagcall'] = $flagCALL;
                        $arrParamsToCreateReminder['calltimingid'] = $strCALLTimingIds;
                        $arrParamsToCreateReminder['flagemail'] = $flagEMAIL;
                        $arrParamsToCreateReminder['emailtimingid'] = $strEMAILTimingIds;
                        $arrParamsToCreateReminder['isreschedule'] = $chkIsReschedule;
                        $arrParamsToCreateReminder['isdisable'] = $chkIsDisable;
                        $arrParamsToCreateReminder['iscallback'] = $chkIsCallback;
                        $arrParamsToCreateReminder['isprivate'] = $chkMakePrivate ?? 'N';
                        $arrParamsToCreateReminder['arrTimeSlot'] = $arrTimeSlot;
                        $arrParamsToCreateReminder['flagpromotional'] = 0;
                        // added by sourabh for salutation cheking
                        $arrAdminSettings = AdminSetting::getInfo();
                        $salutationstatus =$arrAdminSettings->onoffsalutation ;
                        $arrParamsToUpdateReminder['salutationstatus'] = $salutationstatus;

                        $arrParamsToCreateReminder['flagsyncbacktoa1law'] = ($arrEachUserData->contactType == "LawwareOTS") ? 1 : 0;
//                        $arrParamsToCreateReminder['flagsyncbacktoa1law'] = $flagsyncbacktoa1law;
                        // added by sourabh for salutation cheking
                        $arrAdminSettings = AdminSetting::getInfo();
                        $salutationstatus =$arrAdminSettings->onoffsalutation ;
                        $arrParamsToCreateReminder['salutationstatus'] = $salutationstatus;
                        Reminder::addInReminder($arrParamsToCreateReminder);
                    }
                }
            }
            // if reminder id is greater than zero, then it means it is update case.
            else {
                $strResponseMessage = "Reminder Updated Succesfully";
                if (COUNT($arrUsersData) > 0) {

                    $arrUsersData = $arrUsersData[0];
                    $arrParamsToUpdateReminder = array();
                    $arrParamsToUpdateReminder['snsusersid'] = Auth::id();
                    $arrParamsToUpdateReminder['cardcode'] = $arrUsersData->cardCode;
                    $arrParamsToUpdateReminder['caseno'] = $arrUsersData->caseNo;
                    $arrParamsToUpdateReminder['contacttype'] = $arrUsersData->contactType;
                    $arrParamsToUpdateReminder['salutation'] = $arrUsersData->salutation;
                    $arrParamsToUpdateReminder['firstname'] = $arrUsersData->firstName;
                    $arrParamsToUpdateReminder['lastname'] = $arrUsersData->lastName;
                    $arrParamsToUpdateReminder['fullname'] = $arrUsersData->fullName;
                    $arrParamsToUpdateReminder['reminderdatetime'] = $reminderDateTime;
                    $arrParamsToUpdateReminder['title'] = $txtReminderTitle;
                    $arrParamsToUpdateReminder['purpose'] = $txtAppointmentType;
                    $arrParamsToUpdateReminder['templateid'] = $lstTemplates;
                    $arrParamsToUpdateReminder['email'] = strtolower($arrUsersData->email);
                    $arrParamsToUpdateReminder['phoneno'] = (new self)->formatphone($arrUsersData->phoneNumber);
                    $arrParamsToUpdateReminder['bodyemail'] = $taAppoinmentTemplateBody;
                    $arrParamsToUpdateReminder['bodysms'] = $taAppoinmentTemplateBody;
                    $arrParamsToUpdateReminder['languageid'] = $lstLanguage;
                    $arrParamsToUpdateReminder['flagsms'] = $flagSMS;
                    $arrParamsToUpdateReminder['smstimingid'] = $strSMSTimingIds;
                    $arrParamsToUpdateReminder['flagcall'] = $flagCALL;
                    $arrParamsToUpdateReminder['calltimingid'] = $strCALLTimingIds;
                    $arrParamsToUpdateReminder['flagemail'] = $flagEMAIL;
                    $arrParamsToUpdateReminder['emailtimingid'] = $strEMAILTimingIds;
                    $arrParamsToUpdateReminder['isreschedule'] = $chkIsReschedule;
                    $arrParamsToUpdateReminder['isdisable'] = $chkIsDisable;
                    $arrParamsToUpdateReminder['iscallback'] = $chkIsCallback;
                    $arrParamsToUpdateReminder['isprivate'] = $chkMakePrivate ?? 'N';
                    $arrParamsToUpdateReminder['arrTimeSlot'] = $arrTimeSlot;
                    $arrParamsToUpdateReminder['reminderId'] = $reminderId;
                    $arrParamsToUpdateReminder['flagpromotional'] = 0;

                    $arrParamsToUpdateReminder['flagsyncbacktoa1law'] = ($arrUsersData->contactType == "LawwareOTS") ? 1 : 0;
//                    $arrParamsToUpdateReminder['flagsyncbacktoa1law'] = $flagsyncbacktoa1law;
                    Reminder::updateInReminder($arrParamsToUpdateReminder);
                }
            }
        } catch (\Exception $e) {
            //  throw $e;
            DB::rollback();
            Session::flash('message', 'Oops something went wrong');
            return Redirect::to('/');
        }

        // If we reach here, then// data is valid and working.//
        DB::commit();

        Session::flash('message', $strResponseMessage);
        return Redirect::to('/');
    }

    public function getRemindersList(Request $request) {
        
        $arrFilterParams = array();
        $arrFilterParams['dateRangeFilter'] = $request->input('dateRangeFilter');
        $arrFilterParams['startDate'] = $request->input('startDate');
        $arrFilterParams['endDate'] = $request->input('endDate');
        $arrFilterParams['isPrivate'] = $request->input('isPrivate');
        $arrFilterParams['searchText'] = $request->input('searchText');
        
        
        
        $result = Reminder::getReminderDetailsForDashboard($arrFilterParams);
        $arrAdminSettings = AdminSetting::getInfo();
        // echo "<pre>"; print_r($result); exit();
        return Datatables::of($result)
                        ->addColumn('snsUserName', function($eachReminder) {

                            return $eachReminder->firstname;
                        })
                        ->editColumn('reminderDateTime', function($eachReminder) {

                            return $eachReminder->reminderdatetime;
                        })
                        ->addColumn('confirmDate', function($eachReminder) {

                            if (!empty($eachReminder->confirmdate)) {
                                return $eachReminder->confirmdate;
                            } else {
                                return "";
                            }
                        })
                        ->addColumn('twilioResponseColor', function($eachReminder) {
                            if (isset($eachReminder->color)) {
                                return $eachReminder->color;
                            } else {
                                $defaultColor = "#FFFFFF";

                                $objMasterReminder = new MasterReminderResponseColor();

                                $defaultColorObj = $objMasterReminder->getDefaultResponseColors();

                                if (!empty($defaultColorObj)) {
                                    $defaultColor = $defaultColorObj;
                                }

                                return $defaultColor;
                            }
                        })
                        ->make(true);
    }

    public function deleteReminders(Request $request) {

        $arrSendResponse = array(
            'statusCode' => 200,
            'data' => array(),
            'error' => 0,
            'flagMsg' => 'EMPTY',
            'message' => 'No reminders selected'
        );

        $arrReminderIds = $request->input('reminderIds');


        if (!empty($arrReminderIds)) {

            DB::beginTransaction();

            try {
                Reminder::deleteReminders($arrReminderIds);
            } catch (\Exception $e) {
                DB::rollback();
            }

            // If we reach here, then// data is valid and working.//
            DB::commit();

            $arrSendResponse = array(
                'statusCode' => 200,
                'data' => array(),
                'error' => 0,
                'flagMsg' => 'UPDATE',
                'message' => 'Selected reminder(s) deleted successfully.'
            );
        }

        return \App\Http\Helpers::sendResponse($arrSendResponse);
    }

    /* calaudit to snsweb */

    public static function explodeDataEventCategory($other1) {
        $skuList = explode(PHP_EOL, $other1);
        return $skuList;
    }

    public function calauditsns(Request $request) {
        //$arrAdminSettings = AdminSetting::getInfo();
        // $reminderselectpoint = DB::select("select * from calaudit1 where reminder1 =1 and issync=0 ");
        $reminderselectpoint = DB::connection('mysql3')->select("select * from cal1 where reminder =1 and issync=0 ");
        // echo '<pre>'; print_r($reminderselectpoint); exit;
        //$reminderselectpoint = DB::select('select * from calaudit1 where reminder1 =1 and chglast > (select sccl_sync from sns_calaudit_cron_log limit 0,1) order by chglast ');
        //echo "<pre>"; print_r($reminderselectpoint); exit;
        // if (!empty($reminderselectpoint) && $arrAdminSettings->synca1law == 1) {
//        echo  "<pre>";print_r($reminderselectpoint);
//        echo  "<pre>";print_r(trim($reminderselectpoint[0]->other1));exit;
        if (!empty($reminderselectpoint)) {
            foreach ($reminderselectpoint as $keypoint => $remindervalue) {
                //echo "<pre>"; print_r($remindervalue); 
//                list($other1, $cat) = self::explodeDataEventCategory(trim($remindervalue->other1));
                $other1cat = self::explodeDataEventCategory(trim($remindervalue->tevent));
                $other1 = $other1cat[0];
                $findremindertempl = '';
//                $findremindertempl = DB::table('master_reminder_template')->select('*')->where('purpose', '=', trim($remindervalue->other1))->first();
                $findremindertempl = DB::table('master_reminder_template')->select('*')->where('title', '=', trim($other1))->first();
                //echo "hi<pre>"; print_r($findremindertempl);
                if (!empty($findremindertempl) && isset($findremindertempl)) {
                    $snsuserexist = DB::table('users')->select('*')->where('snsusersunqid', '=', trim($remindervalue->initials0))->first();

                    $lawwareUser = DB::connection('mysql3')->select("select * from staff where initials = '$remindervalue->initials' ");

                    if (!isset($snsuserexist) && empty($snsuserexist)) {
                        

                        $user_create = User::create([
                                    'firstname' => (trim($lawwareUser[0]->fname)) ?? $remindervalue->initials0,
                                    'lastname' => (trim($lawwareUser[0]->lname)) ?? $remindervalue->initials0,
                                    'username' => $remindervalue->initials0,
                                    'snsusersunqid' => $remindervalue->initials0,
                                    'email' => trim($remindervalue->email),
                                    'password' => $remindervalue->initials0,
                                    'isconfirmed' => 1
                        ]);
                        if ($user_create) {
                            //$userdetails = DB::table('users')->select('*')->where('recid','=',$user_create)->get();
//                            $remindervalue->snsusersid = $user_create;
                            $remindervalue->snsusersid = $user_create->recid;
                            $this->calaudit_addreminder($remindervalue, $findremindertempl);
                        }
                    } else {

                        DB::table('users')
                                ->where('snsusersunqid', $remindervalue->initials0)
                                ->update(['firstname' => (trim($lawwareUser[0]->fname)) ?? $remindervalue->initials0]);
                        $remindervalue->snsusersid = $snsuserexist->recid;
                        $this->calaudit_addreminder($remindervalue, $findremindertempl);
                    }
                }
                DB::connection('mysql3')->update("update cal1 set issync ='1' where eventno='" . $remindervalue->eventno . "' ");
            }
            // DB::update("update sns_calaudit_cron_log set sccl_sync ='" . date('Y-m-d h:i:s') . "' ");
        } else {
            echo "No Data to Synchronize";
        }
        

        ////// update response from sns to lawware

        $latestRminder = Reminder::getLatestReminderUpdates();

        if (COUNT($latestRminder) > 0) {
            $lastSyncDateTime = NULL;

            foreach ($latestRminder AS $key => $arrReminderToUpdate) {
                $reminderId = $arrReminderToUpdate->recid;
                $twilioStatus = $arrReminderToUpdate->twiliostatus;
                $updateddate = $arrReminderToUpdate->updateddate;
                $snsusersid = $arrReminderToUpdate->snsusersid;
                $purpose = $arrReminderToUpdate->purpose;
                $caseno = $arrReminderToUpdate->caseno;
                $confirmdate = $arrReminderToUpdate->confirmdate;

                $snsuserexist = DB::table('users')->select('*')->where('recid', '=', $snsusersid)->first();
                $objCarbon = new Carbon($lastSyncDateTime);
                $currentDateTime = $objCarbon->format('Y-m-d H:i:s');

                $initial0 = $snsuserexist->snsusersunqid;


                if ($twilioStatus == 1) {
                    $clientResponse = 'Confirmed by client';
                } elseif ($twilioStatus == 2) {
                    $clientResponse = 'Cancelled by client';
                } elseif ($twilioStatus == 3) {
                    $clientResponse = 'Rescheduled by client';
                } elseif ($twilioStatus == 4) {
                    $clientResponse = 'Request Callback by client';
                } elseif ($twilioStatus == 5) {
                    $clientResponse = 'No Action by client';
                }

                $event_text = $purpose.' : '.$clientResponse;

                $data['caseno'] = $caseno;
                $data['initials0'] = $initial0;
                $data['initials'] = $initial0;
                $data['date'] = ($confirmdate != NULL) ? $confirmdate : $currentDateTime;
                $data['event'] = $event_text;
                $data['category'] = 1;

                /// get last actno for increment by 1
                $lastActNo = DB::connection('mysql3')->select("select * from caseact where caseno = $caseno order by actno DESC limit 1 ");
                $data['actno'] = $lastActNo[0]->actno + 1;

                if ($twilioStatus != 5) {
                    DB::connection('mysql3')->table('caseact')->insert($data);
                }

                //update issync flag
                DB::table('reminder')
                ->where('recid', $reminderId)
                ->update(['issynclaw' => "1"]);
            }
        }

        
        
    }

    public function calaudit_addreminder($remindervalue, $findremindertempl) {
//        list($other1, $cat) = self::explodeDataEventCategory(trim($remindervalue->other1));
        $other1cat = self::explodeDataEventCategory(trim($remindervalue->tevent));
        $other1 = $other1cat[0];
        $cat = $other1cat[1] ?? 1;
        $arrParamsToCreateReminder = array();
        $arrParamsToCreateReminder['snsusersid'] = $remindervalue->snsusersid;
        $arrParamsToCreateReminder['caseno'] = $remindervalue->caseno;
        $arrParamsToCreateReminder['cardcode'] = '';
        $arrParamsToUpdateReminder['salutation'] = '';
        $arrParamsToCreateReminder['contacttype'] = 'Lawware';
        $arrParamsToCreateReminder['srno'] = $remindervalue->eventno;
        $arrParamsToCreateReminder['firstname'] = trim($remindervalue->first);
        $arrParamsToCreateReminder['lastname'] = trim($remindervalue->last);
        $arrParamsToCreateReminder['fullname'] = trim($remindervalue->first) . ' ' . trim($remindervalue->last);
        $arrParamsToCreateReminder['reminderdatetime'] = $remindervalue->date;
//        $arrParamsToCreateReminder['title'] = $remindervalue->other1;
//        $arrParamsToCreateReminder['purpose'] = $remindervalue->other1;
        $arrParamsToCreateReminder['title'] = $other1;
        $arrParamsToCreateReminder['purpose'] = $other1;
        $arrParamsToCreateReminder['calender_val'] = $cat;
        $arrParamsToCreateReminder['doctor'] = $remindervalue->doctor;
        $arrParamsToCreateReminder['location'] = $remindervalue->venue;
        $arrParamsToCreateReminder['templateid'] = $findremindertempl->recid;
        $arrParamsToCreateReminder['email'] = strtolower($remindervalue->email);
        $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phone);
        // if (trim($remindervalue->phonehome) != '') {
        //     $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonehome);
        // } elseif (trim($remindervalue->phonebus) != '') {
        //     $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonebus);
        // } elseif (trim($remindervalue->phonefax) != '') {
        //     $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonefax);
        // } elseif (trim($remindervalue->phonecar) != '') {
        //     $arrParamsToCreateReminder['phoneno'] = $this->formatphone($remindervalue->phonecar);
        // } else {
        //     $arrParamsToCreateReminder['phoneno'] = '';
        // }
        $arrTimeSlot = array();
        if (isset($findremindertempl->smstimingid) && !empty($findremindertempl->smstimingid)) {
            $arrTimeSlot['SMS'] = (is_array($findremindertempl->smstimingid)) ? explode(',', $findremindertempl->smstimingid) : array($findremindertempl->smstimingid);
        }
        if (isset($findremindertempl->calltimingid) && !empty($findremindertempl->calltimingid)) {
            $arrTimeSlot['CALL'] = (is_array($findremindertempl->calltimingid)) ? explode(',', $findremindertempl->calltimingid) : array($findremindertempl->calltimingid);
        }
        if (isset($findremindertempl->emailtimingid) && !empty($findremindertempl->emailtimingid)) {
            $arrTimeSlot['EMAIL'] = (is_array($findremindertempl->emailtimingid)) ? explode(',', $findremindertempl->emailtimingid) : array($findremindertempl->emailtimingid);
        }

        $flagSMS = "N";
        $strSMSTimingIds = "";
        if (isset($arrTimeSlot['SMS']) && !empty($arrTimeSlot['SMS'])) {
            $flagSMS = "Y";
            //$strSMSTimingIds = implode(',', $arrTimeSlot['SMS']);
        }
        $flagCALL = "N";
        $strCALLTimingIds = "";
        if (isset($arrTimeSlot['CALL']) && !empty($arrTimeSlot['CALL'])) {
            $flagCALL = "Y";
            //$strCALLTimingIds = implode(',', $arrTimeSlot['CALL']);
        }
        $flagEMAIL = "N";
        $strEMAILTimingIds = "";
        if (isset($arrTimeSlot['EMAIL']) && !empty($arrTimeSlot['EMAIL'])) {
            $flagEMAIL = "Y";
            //$strEMAILTimingIds = implode(',', $arrTimeSlot['EMAIL']);
        }

        $arrParamsToCreateReminder['bodyemail'] = $findremindertempl->bodyemail;
        $arrParamsToCreateReminder['bodysms'] = $findremindertempl->bodysms;
        $arrParamsToCreateReminder['languageid'] = $findremindertempl->language;
        $arrParamsToCreateReminder['flagsms'] = $flagSMS;
        $arrParamsToCreateReminder['smstimingid'] = $findremindertempl->smstimingid;
        $arrParamsToCreateReminder['flagcall'] = $flagCALL;
        $arrParamsToCreateReminder['calltimingid'] = $findremindertempl->calltimingid;
        $arrParamsToCreateReminder['flagemail'] = $flagEMAIL;
        $arrParamsToCreateReminder['emailtimingid'] = $findremindertempl->emailtimingid;
        $arrParamsToCreateReminder['isreschedule'] = $findremindertempl->isreschedule;
        $arrParamsToCreateReminder['isprivate'] = 'N';
        $arrParamsToCreateReminder['arrTimeSlot'] = $arrTimeSlot;
        $arrParamsToCreateReminder['flagpromotional'] = 0;
        $arrParamsToCreateReminder['isactive'] = "A";
        $arrParamsToCreateReminder['isdisable'] = $findremindertempl->isdisable;;
        $arrParamsToCreateReminder['iscallback'] = $findremindertempl->iscallback;;
        //added by sourabh 
        $arrAdminSettings = AdminSetting::getInfo();
        $salutationstatus =$arrAdminSettings->onoffsalutation ;
        $arrParamsToCreateReminder['salutationstatus'] = $salutationstatus;
        Reminder::addInReminder($arrParamsToCreateReminder);
    }

    public function isdone(Request $request) {

        $id = $request->id;
        if ($id != 0) {
            $result = DB::update("update sns_reminder set isdone ='1' where recid='" . $id . "'");
        } else {
            $ids = "'" . implode("','", $request->checkids) . "'";
            $result = DB::update("update sns_reminder set isdone ='1' where recid in($ids)");
        }
        if ($result) {
            die(json_encode($result));
        } else {
            die(json_encode($result));
        }
        exit;
    }

    public function formatphone($number1) {
        $number = preg_replace('#[^0-9+]#', '', $number1);
        if (ctype_digit($number) && strlen($number) == 10) {
            $number = '+91-' . $number;
            return $number;
        } else {
            return $number = $number1;
        }
    }

    /**
     * sync staff details to the users tables 
     * date: 15-03-2018
     * @param Request $request
     */
    public function syncStaffDetailsToUsers(Request $request) {
        $staffDetails = DB::select("select * from staff where isSync=0");
        if (!empty($staffDetails)) {
            foreach ($staffDetails as $keypoint => $staffVal) {
                // check if user  exists or not!!!  
                $snsuserexist = DB::table('users')->select('*')->where([
                            ['snsusersunqid', '=', trim($staffVal->initials)],
                            ['username', trim($staffVal->username)]
                        ])->first();

                // create user objecct to insert/update if  exists
                $userObject = array(
                    'snsusersunqid' => trim($staffVal->initials),
                    'role' => 'users',
                    'username' => trim($staffVal->username),
                    'password' => (!empty(trim($staffVal->password)) ? trim($staffVal->password) : trim($staffVal->username)),
                    'firstname' => trim($staffVal->fname),
                    'lastname' => trim($staffVal->lname),
                    'email' => '',
                    'lawwarelogin' => 0,
                    'isactive' => 'A',
                    'isdeleted' => 0,
                    'isconfirmed' => 1,
                    'phonenumber' => ''
                );
                $suplicateUpdate = array('snsusersunqid' => trim($staffVal->initials), 'username' => trim($staffVal->username));

                User::updateOrCreate($suplicateUpdate, $userObject);
//                if (isset($snsuserexist) && !empty($snsuserexist)) {
//                    // update users data 
//                    $result = DB::table('users')
//                            ->where([
//                                ['snsusersunqid', trim($staffVal->initials)],
//                                ['username', trim($staffVal->username)]
//                            ])
//                            ->update($userObject);
//                } else {
//                    $result = User::create($userObject);
//                }
                DB::update("update staff set isSync = '1' where initials='" . trim($staffVal->initials) . "' ");
            }
        } else {
            echo "No Data to Synchronize";
        }
    }

    public function updateTwilioStatusFromTheServer($reminderId, $twilioStatus, $confirmdate) {
        $array['reminderId'] = $reminderId;
        $array['twilioStatus'] = $twilioStatus;
        $array['confirmdate'] = $confirmdate;
        Reminder::updateTwilioStatusFromTheServer($array);
    }

    public function fetchDefaultTemplates() {
        try {
            $data = DB::select("SELECT * FROM tbl_apptoto_addsmsemail");
            foreach ($data as $key => $val) {
                // insert process for the sns_reminder table.
    //            echo "<pre>";print_r($val->EmailSubject);exit;
                $masterObj = (new MasterTemplates);
                $whenSMS = $masterObj->getSmstimingidAttribute($val->whenSMS);
                $whenCALL = $masterObj->getCalltimingidAttribute($val->whenCALL);
                $whenEMAIL = $masterObj->getEmailtimingidAttribute($val->whenEMAIL);
                $masterTemplateObj = array(
                    'recid' => $val->ID,
                    'title' => trim($val->Name),
                    'purpose' => trim($val->Purpose),
                    'bodyemail' => trim($val->Body),
                    'bodysms' => $val->EmailSubject,
                    'language' => ($val->TwilioLang + 1),
                    'flagsms' => ($val->isSMS ? 'Y' : 'N'),
                    'smstimingid' => $whenSMS,
                    'flagcall' => ($val->isCALL ? 'Y' : 'N'),
                    'calltimingid' => $whenCALL,
                    'flagemail' => ($val->isEMAIL ? 'Y' : 'N'),
                    'emailtimingid' => $whenEMAIL,
                    'isreschedule' => ($val->IsReschedule ? 'Y' : 'N'),
                    'isactive' => 'A',
                    'isdeleted' => 0
                );
                $suplicateUpdate = array('recid' => $val->ID);
                MasterTemplates::updateOrCreate($suplicateUpdate, $masterTemplateObj);
                // insert process for the sns_reminder table.
            }
            echo "Templates imported successfully.";
        
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

}
