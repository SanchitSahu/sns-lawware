<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    /**
     * Show the log page.
     *
     * @return \Illuminate\Http\Response
     */
    public function log() {
        return view('log');
    }

    /**
     * Show the log page.
     *
     * @return \Illuminate\Http\Response
     */
    public function addreminder() {
        //return view('addreminder');
    }

    /**
     * Show the log page.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings() {
        return view('settings');
    }

    /**
     * Show the log page.
     *
     * @return \Illuminate\Http\Response
     */
    public function adddata() {

        DB::beginTransaction();
        try {
          $data=DB::connection('oldsnsdb')->table('lst_countrycode')->select('CountryName','CountryCode')->get();
      $datae=  DB::connection('mysql')->table('sns_co')->insert($data);
          print_R($datae); exit;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        // If we reach here, then// data is valid and working.//
        DB::commit();
    }

}
