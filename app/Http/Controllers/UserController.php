<?php

namespace App\Http\Controllers;

use Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Http\Requests\UsersFormRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Session;
use DB;
use URL;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {

        $this->middleware('checkUserRole');
    }
    public function index() {
        return view('users.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersFormRequest $request) {
        // process the store
        $input = $request->all();
        $user_exist = User::where([
                ['username','=',trim($input['username'])],
                ['isdeleted', '!=', '1']
            ])->first();
        if(isset($user_exist) && !empty($user_exist))
        {
            Session::flash('message', 'User already exist!!');
            //return redirect('users/create')->with(array('users'=>$input));
            return Redirect::back();
        }
        else{

        $input['snsusersunqid']=$input['username'];
        $users = User::create($input);
        // redirect
        Session::flash('message', 'User created successfully ');
        return Redirect::to('users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $users = User::find($id);
        // show the edit form and pass the contact
        return view('users.edit')
                        ->with(['users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(UsersFormRequest $users, $id) {

        $request = $users->all();
        //echo "<pre>"; print_r($request); exit;
        $users = User::where(['recid' => $id])->first();
        if (!empty($users)) {
            $users->update($request);
        } else {
            $users = User::create($request);
            $lastinsertedid = $users->recid;
        }
        // redirect
        Session::flash('message', 'User updated successfully!');
        return Redirect::to('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $users = User::where(['recid' => $id])->first();
        $users->isdeleted = 1;
        $users->save();
    }

    /**
     * get the list from contacts table.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function getlist() {
        $result = User::where('isdeleted','!=','1')->get();
        return Datatables::of($result)
                        ->make(true);
    }
}
