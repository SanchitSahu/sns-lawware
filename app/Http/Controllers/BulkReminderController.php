<?php

namespace App\Http\Controllers;

use Excel;
use URL;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class BulkReminderController extends Controller {

    // add bulk reminder in table appointment table

    public static function ftp_connect() {
        // path to remote file
        // $remote_file = '/usr2/tcin/api.csv';
        $remote_file = config('app.REMOTE_URL');
        $local_file = config('app.LOCAL_URL');
        $ftp_server = config('app.FTP_SERVER');
        $ftp_user_name = config('app.FTP_USER');
        $ftp_user_pass = config('app.FTP_PASS');
        // open some file to write to
        $handle = fopen($local_file, 'w');
        // set up basic connection
        $conn_id = ftp_connect($ftp_server);

        if ($conn_id) {
            // login with username and password
            $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

            // try to download $remote_file and save it to $handle
            if (ftp_fget($conn_id, $handle, $remote_file, FTP_ASCII, 0)) {
                return true;
            }
            return false;
            // close the connection and the file handler
            ftp_close($conn_id);
            fclose($handle);
        }
        return false;
    }

    public function addBulkReminder() {

        \DB::beginTransaction();
        try {

            $connect = self::ftp_connect();
            if ($connect) {
                $path = config('app.LOCAL_URL');

                $data = Excel::load($path)->noHeading()->get();
                // remove null value
                $c = function($v) {
                    return array_filter($v) != array();
                };
                $data = (array_values(array_filter($data->toArray(), $c)));
                if (!empty($data)) {
                    foreach ($data as $key => $value) {

                        $arr[$key] = [
                            'account' => $value[0],
                            'last_name' => $value[1],
                            'first_name' => $value[2],
                            'home' => $value[3],
                            'work' => $value[4],
                            'appointment' => $value[5],
                            'appt_date' => Carbon::parse($value[6])->format('Y-m-d'),
                            'appt_time' => Carbon::parse($value[7])->format('H:i:s'),
                            'appt_doctor' => $value[8],
                            'appt_room' => $value[9],
                            'appt_room_location' => $value[10],
                            'appt_room_class' => $value[11],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                    }
                    $bulktruncate = \App\BulkApiReminder::query()->truncate();
                    $bulkapireminder = \App\BulkApiReminder::insert($arr);
                    $bulknapireminderfinal = self::addBulkReminderFinal();
                }
            }
            return ['message' => 'Not able to connect'];
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            return ['message' => 'Oops something went wrong'];
        }

        // If we reach here, then// data is valid and working.//
        \DB::commit();
        return ['message' => 'bulk reminder added successfully'];
    }

    public static function addBulkReminderFinal() {
        //select from reminder table with filterd data
        $reminder = \App\BulkApiReminder::select(['account',
                            'first_name', 'last_name',
                            'appt_date', 'work', 'home', 'appointment', 'appt_doctor', 'appt_room',
                            \DB::raw('IF(appt_time<="08:00" ,DATE_ADD(appt_time, INTERVAL 12 HOUR),appt_time) as appt_time'), 'appt_room_location',
                            'appt_room_class',
                            \DB::raw('IF(appt_doctor !=0, appt_doctor, IF(appt_room != 0, appt_room,"" ) )  room_no'),
                            \DB::raw('IF(home !=0, home, IF(work != 0, work, IF(appointment !=0, appointment, "" ) ) )  phone_number')])
                        ->where('first_name', '!=', 'CLOSED')
                        ->where('last_name', '!=', 'CLOSED')
                        ->where(function ($query) {
                            $query->where('home', '!=', 0)
                            ->orwhere('work', '!=', 0)
                            ->orwhere('appointment', '!=', 0);
                        })
                        ->groupBy('account', 'phone_number', 'appt_date')
                        ->get()->toArray();
        $addinsert = [];
        $addcontact = [];
        foreach ($reminder as $key => $value) {
            $bulkfinal = \App\BulkApiReminderFinal::where('account', $value['account'])
                    ->where('phone_number', $value['phone_number'])
                    ->where('appt_date', $value['appt_date'])
                    ->get()
                    ->toArray();
            if (empty($bulkfinal)) {
                $addinsert[] = $value;
                $addinsert[$key]['created_at'] = Carbon::now();
                $addinsert[$key]['updated_at'] = Carbon::now();
                //add contact if its new
                $contact_fetch = \App\Contact::select(['phonenumber'])
                        ->where('phonenumber', $value['phone_number'])
                        ->first();
                if (empty($contact_fetch)) {
                    $addcontact[$key]['firstname'] = $value['first_name'];
                    $addcontact[$key]['lastname'] = $value['last_name'];
                    $addcontact[$key]['phonenumber'] = $value['phone_number'];
                    $addcontact[$key]['country_id'] = 93;
                    $addcontact[$key]['createddate'] = Carbon::now();
                    $addcontact[$key]['updateddate'] = Carbon::now();
                }
            }
        }


        //update contact id
        self::addContact($addcontact);
        $reminder = \App\BulkApiReminderFinal::insert($addinsert);
        self::syncBulkReminder($addinsert);
    }

    public static function addContact($addcontact) {
        //get unique phone number
        $input = array_intersect_key($addcontact, array_unique(array_map(function ($el) {
                            return $el['phonenumber'];
                        }, $addcontact)));
        $contact = \App\Contact::insert($input);
        \DB::table('contact_list')->update(['case' => \DB::raw('CONCAT_WS(recid,"RS","")'),
            'cardcode' => \DB::raw('CONCAT_WS(recid,"RS","")'),
            'firmcode' => \DB::raw('CONCAT_WS(recid,"RS","")')]);
    }

    public static function syncBulkReminder($addinsert, $flagpromotional = '', $userid = '') {

        // add in final reminder table if entry is new

        foreach ($addinsert as $key => $value) {

            $medicalroom = \App\MedicalRoomLink::select('tid', 'medical_room_link.recid', 'title', 'purpose', 'bodyemail', 'bodysms', 'flagsms', 'smstimingid', 'flagcall', 'calltimingid', 'language', 'flagsms', 'isreschedule', 'language')
                    ->LeftJoin('master_reminder_template', 'medical_room_link.tid', 'master_reminder_template.recid')
                    ->where('room_no', $value['room_no'])
                    ->get()
                    ->toArray();
            $contact = \App\Contact::select(['phonenumber', 'cardcode', 'case', 'firstname', 'lastname', 'countrycode'])
                    ->LeftJoin('master_countries', 'master_countries.rec_id', 'country_id')
                    ->where('phonenumber', $value['phone_number'])
                    ->first();

            foreach ($medicalroom as $medkey => $medvalue) {

                // checking for slots;
                $arrtimeslots = [];
                if ($medvalue['flagsms'] == 'Y') {
                    $arrtimeslots['SMS'] = explode(",", $medvalue['smstimingid']);
                }

                if ($medvalue['flagcall'] == 'Y') {
                    $arrtimeslots['CALL'] = explode(",", $medvalue['calltimingid']);
                }

                $arr = ['snsusersid' => (empty($userid) ? 1 : $userid), 'cardcode' => $contact->cardcode,
                    'caseno' => $contact->case, 'contacttype' => 'sns', 'salutation' => '',
                    'firstname' => $contact->firstname, 'lastname' => $contact->lastname,
                    'reminderdatetime' => $value['appt_date'] . " " . $value['appt_time'],
                    'title' => $medvalue['title'], 'fullname' => $contact->firstname . " " . $contact->lastname,
                    'purpose' => $medvalue['purpose'], 'templateid' => $medvalue['tid'],
                    'templateid' => $medvalue['tid'], 'email' => "", 'phoneno' => $contact->phonenumber,
                    'bodyemail' => $medvalue['bodyemail'], 'bodysms' => $medvalue['bodysms'],
                    'languageid' => $medvalue['language'], 'flagsms' => $medvalue['flagsms'],
                    'smstimingid' => $medvalue['smstimingid'], 'flagcall' => $medvalue['flagcall'],
                    'calltimingid' => $medvalue['calltimingid'], 'flagemail' => 'N',
                    'emailtimingid' => '', 'isreschedule' => $medvalue['isreschedule'],
                    'isprivate' => 'N', 'arrTimeSlot' => $arrtimeslots, 'flagpromotional' => $flagpromotional
                ];

                \App\Reminder::addInReminder($arr);
            }
        }
    }

}
