<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'firstname' => 'required|max:255',
                        'lastname' => 'required|max:255',
                        'middlename' => 'max:255',
                        'country_id' => 'required',
                        'phonenumber' => 'required',
                        'email' => 'sometimes|nullable|email|max:255',
                        'phonetype' => 'required',
                        // 'salutation' => 'required',
//                        'zipcode' => 'min:3|max:10',
//                        'dob' => 'required',
                    ];
                }
            case 'PATCH': {
                    return [
                        'firstname' => 'required|max:255',
                        'lastname' => 'required|max:255',
                        'middlename' => 'max:255',
                        'country_id' => 'required',
                        'phonenumber' => 'required',
                        'email' => 'sometimes|nullable|email|max:255',
                        'phonetype' => 'required',
                        // 'salutation' => 'required',
//                        'zipcode' => 'min:3|max:10',
//                        'dob' => 'required',
                    ];
                }
        }
    }
    
    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
//    public function messages() {
//        switch ($this->method()) {
//            case 'POST': {
//                    return [
//                        'firstname.required' => 'Please enter firstname',
//                        'lastname.required' => 'Please enter lastname',
//                        'middlename' => 'max:255',
//                        'country_id' => 'required',
//                        'phonenumber' => 'required|unique:contact_list,phonenumber',
//                        'email' => 'sometimes|nullable|email|max:255',
//                        'phonetype' => 'required',
//                        'salutation' => 'required',
//                        'zipcode' => 'min:3|max:10',
//                        'dob' => 'required',
//                    ];
//                }
//            case 'PATCH': {
//                    return [
//                        'firstname.required' => 'required|max:255',
//                        'lastname.required' => 'required|max:255',
//                        'middlename' => 'max:255',
//                        'country_id.required' => 'required',
//                        'phonenumber.required' => 'required|unique:contact_list,phonenumber,' . $this->segment(2) . ',cardcode',
//                        'email' => 'sometimes|nullable|email|max:255',
//                        'phonetype.required' => 'required',
//                        'salutation' => 'required',
//                        'zipcode' => 'min:3|max:10',
//                        'dob' => 'required',
//                    ];
//                }
//        }
//    }

}
