<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
   switch($this->method())
    {
          case 'POST':
          {
    return [
        'firstname' => 'required|max:255',
        'lastname' => 'required|max:255',
        'email'=>'sometimes|nullable|email|max:255',
        'username' => 'required|max:15',
        'password' => 'required|max:15',
       ];
          }
            case 'PATCH':
          {
    return [
        'firstname' => 'required|max:255',
        'lastname' => 'required|max:255',
        'email'=>'sometimes|nullable|email|max:255',        
        'username' => 'required|max:15',
        'password' => 'required|max:15',
        ];
          }
    }
    }
    
    
}
