<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class ReminderForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'txtReminderTitle'      =>  'required|max:255',
            'txtReminderTitle'      =>  'max:255',
//            'txtDate'               =>  'required|date|date_format:Y/m/d',
            'txtDate'               =>  'required|date|date_format:' . config('app.APP_DEFAULT_VIEW_DATE_FORMAT'),
            'txtTime'               =>  'required|date_format:'  . config('app.APP_DEFAULT_TIME_FORMAT'),
            'txtTime'               =>  'required|date_format:'  . config('app.APP_DEFAULT_TIME_FORMAT'),
            'txtAppointmentType'    =>  'required|max:255',
            'chkFlagTimeSlot'       =>  'required|reminderTimingSlot',
            'selectedUsersData'       =>  'required|selectedGuestForReminder',
            'taAppoinmentTemplateBody'  => 'required'
        ];
    }


    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
//            'txtReminderTitle.required'                     =>  'Please enter reminder title',
            //'txtReminderTitle.max'                          =>  'Please enter maximum 255 characters',
            'txtDate.required'                              =>  'Please enter reminder date',
            'txtDate.date'                                  =>  'Please enter valid reminder date',
            'txtDate.date_format'                           =>  'Please enter valid reminder date',
            'txtTime.required'                              =>  'Please enter reminder time',
            'txtTime.date_format'                           =>  'Please enter valid reminder time',
            'txtAppointmentType.required'                   =>  'Please enter appointment type',
            'txtAppointmentType.max'                        =>  'Please enter maximum 255 characters',
            'chkFlagTimeSlot.required'                      =>  'Please select atleast one time slot',
            'chkFlagTimeSlot.reminderTimingSlot'            =>  'Please select atleast one time slot',
            'selectedUsersData.required'                    =>  'Please select atleast one participant',
            'selectedUsersData.selectedGuestForReminder'    =>  'Please select atleast one participant',
            'selectedUsersData.selected_guest_for_reminder'    =>  'Please select atleast one participant',
            'taAppoinmentTemplateBody.required'             =>  'Please selecte event template body'
        ];
    }
}
