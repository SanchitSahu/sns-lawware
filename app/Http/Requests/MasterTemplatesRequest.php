<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasterTemplatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
        'title' => 'required',
        'bodyemail' => 'required',
        'language'=> 'required',
        'purpose'=> 'required',

    ];
    }
    
    public function messages()
{
     return [
          'bodyemail.required' => 'The event body field is required.',
          'purpose.required' => 'The appointment type field is required.'
     ];
}
}
