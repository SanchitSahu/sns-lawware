<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'emailfrom' => 'required|email',
            'emailsubject' => 'required',
            'twiliosmsnumber' => 'required',
            'twiliocallnumber' => 'required',
            'twilioauthtoken' => 'required',
            'twilioaccountsid' => 'required',
            'clientkey' => 'required'
        ];
    }

}
