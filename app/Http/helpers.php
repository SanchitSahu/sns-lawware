<?php

namespace App\Http;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Helpers {

    public static function sendResponse ($arrParams) {

        $statusCode = 200;
        if(isset($arrParams['statusCode']) && !empty($arrParams['statusCode'])) {
            $statusCode = $arrParams['statusCode'];
        }

        $error = 1;
        if(isset($arrParams['error'])) {
            $error = $arrParams['error'];
        }


        $flagMsg = "SUCCESS";
        if(isset($arrParams['flagMsg']) && !empty($arrParams['flagMsg'])) {
            $flagMsg = $arrParams['flagMsg'];
        }

        $message = "";
        if(isset($arrParams['message']) && !empty($arrParams['message'])) {
            $message = $arrParams['message'];
        }

        $data = array();
        if(isset($arrParams['data']) && !empty($arrParams['data'])) {
            $data = $arrParams['data'];
        }

        $arrResponse = array(
            'error'     =>  $error,
            'flagMsg'   =>  $flagMsg,
            'message'   =>  $message,
            'data'      =>  $data
        );

        return response($arrResponse, $statusCode)
                  ->header('Content-Type', 'application/json');
    }
}

?>