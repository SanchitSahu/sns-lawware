<?php

namespace App\Http\Middleware;

use Closure;
use User;
use Session;

class checkUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role =  \Auth::user()->role;
        if ($role != 'admin')
        {
           Session::flash('message', 'You are not authorized to access this resource.');
            return redirect('/');
        }
        else{
            return $next($request);
        }

    }
    
}