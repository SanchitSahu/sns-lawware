<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\DebugMailSend;

class beforeSendRequestToCloud
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $flagConnected = beforeSendRequestToCloud::checkNetConnection();

        if($flagConnected) {
            return $next($request);
        }
        else {
            print '<pre>';
            print_r("Cron request not loaded, Unable to connect with cloud server");
            print '</pre>';
            die();
            // $arrDebugMailParams = array();
            // $arrDebugMailParams['subject'] = 'Cron request not loaded, Unable to connect with cloud server';
            // $arrDebugMailParams['arrData'] = array();
            // DebugMailSend::sendDebugMail($arrDebugMailParams);
        }
    }


    public static function checkNetConnection() {
        $isConnected = true;
        $flagConnected = @fsockopen("52.66.93.186", 80); 
        
        //website, port  (try 80 or 443)
        if ($flagConnected){
            fclose($flagConnected);
        }
        else{
            $isConnected = false; //action in connection failure
        }

        return $isConnected;
    }
}
