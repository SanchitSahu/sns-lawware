<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    
    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';
    const IS_DELETED = 1;
    protected  $table='users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password','username','role','snsusersunqid','lawwarelogin',
        'firstname','lastname','isconfirmed','confirmation_code'
    ];
/**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function setFirstNameAttribute($value) {
        $this->attributes['firstname'] = ucwords($value);
    }

    public function setLastNameAttribute($value) {
        $this->attributes['lastname'] = ucwords($value);
    }
      public function scopeUserActive($query) {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }
    
    /*public function setPasswordAttribute($pswd){
        $this->attributes['password'] = Hash::make($pswd);
    }*/
}
