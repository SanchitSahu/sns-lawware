<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use App\Http\Controllers\SendRequestToServer;

class AdminSetting extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admin_settings';
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $fillable = ['flagcsv'];

    public static function getInfo() {
        $objAdminSettings = AdminSetting::find(1);

        $companyLogoPath = 'storage/app/public/resizimg/' . $objAdminSettings->companylogo;
//        $companyLogoPath = config("filesystems.disks.public.root") . '/resizimg/' . $objAdminSettings->companylogo;
//        $companyLogoPath = config("filesystems.disks.public.url") . '/app/public/resizimg/' . $objAdminSettings->companylogo;


        $flagCompanyLogoExists = File::exists($companyLogoPath);

        if (!$flagCompanyLogoExists) {
            $companyLogoPath = 'img/no-img.png';
        }

        $companyLogoPath = asset($companyLogoPath);

        $objAdminSettings->companylogoFullPath = $companyLogoPath;

        return $objAdminSettings;
    }

    public static function getTimeZoneList() {
        return array(
            "12" => "12",
            "24" => "24"
        );
    }

    public static function getRecallTimes() {
        $arrReturn = array();

        for ($intI = 1; $intI <= 5; $intI++) {
            $arrReturn[$intI] = $intI;
        }

        return $arrReturn;
    }

    public static function getRecalIntervals() {
        return array(
            "5" => "5",
            "10" => "10",
            "15" => "15",
            "30" => "30",
            "60" => "60"
        );
    }

    public static function updateTimeZone($arrParams) {
        $lstTimeZoneDropDown = $arrParams['lstTimeZoneDropDown'];

        $objAdminSettings = AdminSetting::find(1);
        $objAdminSettings->systemtimezone = $lstTimeZoneDropDown;
        $objAdminSettings->isupdated = 1;
        $objAdminSettings->save();

        return true;
    }

    public static function updateRecallSettings($arrParams) {
        $recallEnable = $arrParams['recallEnable'];
        $howManyTimes = $arrParams['howManyTimes'];
        $timingInterval = $arrParams['timingInterval'];


        $objAdminSettings = AdminSetting::find(1);
        $objAdminSettings->flagrecall = $recallEnable;
        $objAdminSettings->recallinterval = $timingInterval;
        $objAdminSettings->recalltimes = $howManyTimes;
        $objAdminSettings->isupdated = 1;
        $objAdminSettings->save();

        return true;
    }

    public static function updateCompanyLogoAndEmailSignature($arrParams) {
        $fileName = $arrParams['fileName'];
        $taEmailSignature = $arrParams['taEmailSignature'];
        $taCompanyName = $arrParams['taCompanyName'];

        $objAdminSettings = AdminSetting::find(1);
        if (!empty($fileName)) {
            $objAdminSettings->companylogo = $fileName;
        }
        $objAdminSettings->emailsignature = $taEmailSignature;
        $objAdminSettings->companyname = $taCompanyName;
        $objAdminSettings->isupdated = 1;
        $objAdminSettings->save();

        return true;
    }

    public static function updatedata($request, $mastertemplates) {

        if (isset($request['emailfrom']) && !empty($request['emailfrom'])) {
            $mastertemplates->emailfrom = $request['emailfrom'];
        }if (isset($request['emailsubject']) && !empty($request['emailsubject'])) {
            $mastertemplates->emailsubject = $request['emailsubject'];
        }if (isset($request['twiliocallnumber']) && !empty($request['twiliocallnumber'])) {
            $mastertemplates->twiliocallnumber = $request['twiliocallnumber'];
        }if (isset($request['twiliosmsnumber']) && !empty($request['twiliosmsnumber'])) {
            $mastertemplates->twiliosmsnumber = $request['twiliosmsnumber'];
        }if (isset($request['twilioaccountsid']) && !empty($request['twilioaccountsid'])) {
            $mastertemplates->twilioaccountsid = $request['twilioaccountsid'];
        }if (isset($request['twilioauthtoken']) && !empty($request['twilioauthtoken'])) {
            $mastertemplates->twilioauthtoken = $request['twilioauthtoken'];
        }if (isset($request['clientkey']) && !empty($request['clientkey'])) {
            $mastertemplates->clientkey = $request['clientkey'];
        }

        $mastertemplates->isupdated = 1;

        $mastertemplates->save();
        return true;
    }

    public static function checkIfEmailAndAdminSettingsUpdated() {

        $arrResult = AdminSetting::find(1)->where('isupdated', 1)->get()->toArray();

        return $arrResult;
    }

    public static function updateAdminSettingOnCloud() {
        $objAdminSettings = AdminSetting::find(1);
        $objAdminSettings->isupdated = 0;
        $objAdminSettings->save();

        return true;
    }

    public static function updateCsvUpload($value) {

        $admin = AdminSetting::find(1);
        $admin->isupdated = 1;
        $admin->update($value);
        $admin->save();
    }

}
