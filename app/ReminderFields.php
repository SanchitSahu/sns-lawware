<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderFields extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_template_insert_fields';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function scopeActive($query) {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }

    public static function getfields() {
        $fields = ReminderFields::active()->pluck('name', 'name')->toArray();
        return $fields;
    }

}
