<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkApiReminder extends Model {
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bulk_api_reminder';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

   

    protected $fillable = ['account', 'first_name', 'last_name', 'home', 'work', 'appointment',
        'appt_date', 'appt_time', 'appt_doctor', 'appt_room_location','appt_dt_time','appt_room',
        ' 	appt_room_class', 'created_at', 'updated_at'];
    
    
     

}
