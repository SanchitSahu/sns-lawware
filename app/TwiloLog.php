<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class TwiloLog extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'twilio_log';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';
    const IS_DELETED = 1;
    const IS_NOT_DELETED = 0;
    const IS_ACTIVE = 'A';
    const IS_NOT_ACTIVE = 'I';

    
    public function twilioremindersync() {

        return $this->hasOne(
                        'App\ReminderSync', 'sid', 'sid'
        );
    }

    public function getStartdateAttribute($value) {
        return Carbon::parse($value)->format('m/d/Y H:i');
    }

    public static function getlogDateRangeArray() {
        return array(
            0 => 'Today',
            1 => 'Yesterday',
            2 => 'Past Week',
            3 => 'Specific Date',
            4 => 'Custom Range'
        );
    }

    public static function twiliolist($request) {
        $query = DB::table('twilio_log')
                ->leftjoin('reminder_sync', 'reminder_sync.sid', '=', 'twilio_log.sid')
                ->leftjoin('reminder', 'reminder_sync.reminderid', '=', 'reminder.recid')
                ->select(['twilio_log.recid', 'reminder.firstname as firstname', 'reminder.lastname as lastname', 'twilio_log.twiliostatus',
            'tonumber', 'duration', 'startdate', 'typex', 'reminder.recid as reminderid', 'reminder.caseno']);

        if (isset($request['daterangefilter']) && $request['daterangefilter'] == 0) {
            $query->where(\DB::raw('DATE_FORMAT(startdate,"%Y-%m-%d")'), date('Y-m-d'));
        } else if (isset($request['daterangefilter']) && $request['daterangefilter'] == 1) {
            $query->where(\DB::raw('DATE_FORMAT(startdate,"%Y-%m-%d")'), Carbon::now()->subDay()->format('Y-m-d'));
        } else if (isset($request['daterangefilter']) && $request['daterangefilter'] == 2) {
            $query->where(\DB::raw('DATE_FORMAT(startdate,"%Y-%m-%d")'), '<=', Carbon::now()->subDay()->format('Y-m-d'));
            $query->where(\DB::raw('DATE_FORMAT(startdate,"%Y-%m-%d")'), '>=', Carbon::now()->subDays(8)->format('Y-m-d'));
        } else if (isset($request['daterangefilter'])  && $request['daterangefilter'] == 3 && isset($request['startdate'])) {
            $query->where(\DB::raw('DATE_FORMAT(startdate,"%Y-%m-%d")'), '=', Carbon::parse($request['startdate'])->format('Y-m-d'));
        } else if (isset($request['daterangefilter'])  && $request['daterangefilter'] == 4 && isset($request['startdate']) && isset($request['enddate'])) {
            $query->where(\DB::raw('DATE_FORMAT(enddate,"%Y-%m-%d")'), '<=', Carbon::parse($request['enddate'])->format('Y-m-d'));
            $query->where(\DB::raw('DATE_FORMAT(startdate,"%Y-%m-%d")'), '>=', Carbon::parse($request['startdate'])->format('Y-m-d'));
        }


        return $query;
    }

    public static function updateTwilioLogsFromServer($data) {
 
        $twiliologarr = [];
    
        foreach ($data as $kdata => $vdata) {

            $twilioStatus = $vdata['twiliostatus'];
            if($twilioStatus == 'failed' || $twilioStatus == 'canceled' || $twilioStatus == 'Busy' || $twilioStatus == 'No-answer' ){
                $twilioStatus = 'no-answer';
            }

            
            $twiliologarr[$kdata]['typex'] = $vdata['typex'];
            $twiliologarr[$kdata]['fromnumber'] = $vdata['fromnumber'];
            $twiliologarr[$kdata]['tonumber'] = $vdata['tonumber'];
            $twiliologarr[$kdata]['direction'] = $vdata['direction'];
            $twiliologarr[$kdata]['duration'] = $vdata['duration'];
            $twiliologarr[$kdata]['startdate'] = $vdata['startdate'];
            $twiliologarr[$kdata]['enddate'] = $vdata['enddate'];
            $twiliologarr[$kdata]['twiliostatus'] = $twilioStatus;
            $twiliologarr[$kdata]['sid'] = $vdata['sid'];
            $twiliologarr[$kdata]['twiliotime'] = $vdata['twiliotime'];
            $twiliologarr[$kdata]['createddate'] = $vdata['createddate'];
            $twiliologarr[$kdata]['updateddate'] = $vdata['updateddate'];

          
            if ($kdata >= 0) {
                
                if($vdata['typex']=='CALL'){
                  
                        $remindersync=ReminderSync::where([
                            ['reminderid',$vdata['reminderid']],
                             ['recallcalltime',$vdata['recallcalltime']],
                            ['remindertimingid',$vdata['remindertimingid']]])
                            ->first();
                        
                }else{
                
                $remindersync=ReminderSync::where([
                            ['reminderid',$vdata['reminderid']],
                            ['remindertimingid',$vdata['remindertimingid']]])
                            ->first();
                }
           
                if(!empty($remindersync)){
                    $remindersync->sid=$vdata['sid'];
                    $remindersync->save();
                }
                
                $twiliosid = \App\TwiloLog::where('sid', $vdata['sid'])->first();
         
                if (!empty($twiliosid)) {
              
                    \App\TwiloLog::where('sid', $vdata['sid'])
                            ->update(['typex'=>$vdata['typex'],'startdate'=>$vdata['startdate'],
                                'twiliostatus'=>$twilioStatus]);
                    
             
                    unset($twiliologarr[$kdata]);
                    
                }

            }
              }

        $ins=TwiloLog::insert($twiliologarr);
         if($ins){
             return true;
         }
    return false;
    
    }

}
