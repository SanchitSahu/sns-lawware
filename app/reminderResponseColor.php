<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reminderResponseColor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'master_reminder_response_color';

    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    
}
