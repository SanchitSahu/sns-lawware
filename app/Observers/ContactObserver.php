<?php 

namespace App\Observers;
use Auth;

class EventObserver {

    public $userID;

    public function __construct(){
 
     //   $this->userID = Auth::id();
    }

    public function saving($model)
    {
        $model->updated_by = $this->userID;
    }

    public function saved($model)
    {
        $model->updated_by = $this->userID;
    }


    

    public function updated($model)
    {
        $model->case = $this->userID;
    }


   

    public function created($model)
    {
        $model->created_by = $this->userID;
        
    }


//    public function removing($model)
//    {
//        $model->purged_by = $this->userID;
//    }
//
//    public function removed($model)
//    {
//        $model->purged_by = $this->userID;
//    }
}