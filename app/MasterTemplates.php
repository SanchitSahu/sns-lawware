<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MasterTemplates extends Model
{
   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table='master_reminder_template';
    
      /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';
    const IS_DELETED = 1;
    const IS_NOT_DELETED = 0;
    const IS_ACTIVE = 'A';
    const IS_NOT_ACTIVE = 'I';
    
    public $fields;
    
    protected $fillable = ['title', 'purpose', 'bodyemail', 'bodysms', 'language', 'flagsms',
        'smstimingid', 'flagcall', 'calltimingid', 'flagemail', 'emailtimingid', 'created_at', 'updated_at', 'isreschedule' ,'isdisable','iscallback',
        'firmcode', 'cardcode', 'country_id'];

    

   
    
       protected static function setemptyvalue($request,$mastertemplates)
    {
         // echo '<pre>'; print_r($mastertemplates);exit;
       if(!isset($request['calltimingid']) && empty($request['calltimingid'])){
           $mastertemplates->calltimingid='';
       }if(!isset($request['smstimingid']) && empty($request['smstimingid'])){
           $mastertemplates->smstimingid='';
       }if(!isset($request['emailtimingid']) && empty($request['emailtimingid'])){
           $mastertemplates->emailtimingid='';
       }if(!isset($request['isreschedule']) && empty($request['isreschedule'])){
           $mastertemplates->isreschedule='N';
       }if(!isset($request['isdisable']) && empty($request['isdisable'])){
           $mastertemplates->isdisable='N';
       }if(!isset($request['iscallback']) && empty($request['iscallback'])){
           $mastertemplates->iscallback='N';
       }
       $mastertemplates->save();
     }
    
    
    
       public function scopeActive($query)
    {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }
    
    /**
     * Set the templates's sms timing id.
     *
     * @param  string  $value
     * @return void
     */
     public function setBodyemailAttribute($value) {
 
       $this->attributes['bodyemail'] =  strip_tags($value);
      
    }
    /**
     * Set the templates's sms timing id.
     *
     * @param  string  $value
     * @return void
     */
     public function setSmstimingidAttribute($value) {

       $this->attributes['smstimingid'] =  (!empty($value))?implode(',',$value):"";
         $this->attributes['flagsms']=(!empty($value))?'Y':'N';
    }
    
     /**
     * Set the templates's email timing id.
     *
     * @param  string  $value
     * @return void
     */
    
     public function setEmailtimingidAttribute($value) {
         
        $this->attributes['emailtimingid'] =(!empty($value))?implode(',',$value):"";
         $this->attributes['flagemail']=(!empty($value))?'Y':'N';
    }
    
      /**
     * Set the templates's call timing id.
     *
     * @param  string  $value
     * @return void
     */
    
    public function setCalltimingidAttribute($value) {
         
        $this->attributes['calltimingid'] = (!empty($value))?implode(',',$value):"";
         $this->attributes['flagcall']=(!empty($value))?'Y':'N';
    }
    
     /**
     * Get the templates's sms timing id.
     *
     * @param  string  $value
     * @return string
     */
    public function getSmstimingidAttribute($value)
    {
        return explode(',',$value);
    }
     /**
     * Get the templates's email timing id.
     *
     * @param  string  $value
     * @return string
     */
    public function getEmailtimingidAttribute($value)
    {
        return explode(',',$value);
    }
    
     /**
     *  Get the templates's call timing id.
     *
     * @param  string  $value
     * @return string
     */
    public function getCalltimingidAttribute($value)
    {
        return explode(',',$value);
    }
    
     
    
}
