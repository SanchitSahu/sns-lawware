<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table = 'co';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

}
