<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTemplateInsertFields extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_template_insert_fields';

      /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';


    public static function getAllInsertFields() {
        $insertFields = MasterTemplateInsertFields::all()->toArray();
        return $insertFields;
    }
}
