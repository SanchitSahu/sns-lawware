<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ReminderTiming extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reminder_timing';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function scopeActive($query) {
        return $query->where('reminder_timing.isactive', 'A')->where('reminder_timing.isdeleted', 0);
    }

    public function scopeNonDeleted($query) {
        return $query->where('reminder_timing.isdeleted', 0);
    }

    public static function insertReminderTimingForReminder($arrParams) {
        //echo '<pre>';print_r($arrParams);exit('123');
        $reminderId = $arrParams['reminderId'];
        $arrTimeSlot = $arrParams['arrTimeSlot'];
        $arrToPassParams = $arrParams['arrToPassParams'];
        $arrSlotsAllInformation = ReminderSlots::getSlotsAllInformation();
        if (!empty($arrTimeSlot)) {
            if (isset($arrTimeSlot['SMS']) && !empty($arrTimeSlot['SMS'])) {
                foreach ($arrTimeSlot['SMS'] AS $key => $reminderSlotIdForAppointmentSMS) {

                    $reminderSlotIdForAppointmentSMS = explode(',', $reminderSlotIdForAppointmentSMS);
                    if (!empty($arrSlotsAllInformation)) {
                        foreach ($arrSlotsAllInformation AS $allSlotsKey => $arrAllSlotsInfoSMS) {
                            if (in_array( $arrAllSlotsInfoSMS['recid'] ,$reminderSlotIdForAppointmentSMS )) {
                                $arrParamsToInsertIntoReminder = array();
                                $arrParamsToInsertIntoReminder['reminderId'] = $reminderId;
                                $arrParamsToInsertIntoReminder['value'] = $arrAllSlotsInfoSMS['value'];
                                $arrParamsToInsertIntoReminder['unit'] = $arrAllSlotsInfoSMS['unit'];
                                $arrParamsToInsertIntoReminder['typeX'] = 'SMS';
                                $arrParamsToInsertIntoReminder['arrToPassParams'] = $arrToPassParams;
                                ReminderTiming::insertIntoReminderTimig($arrParamsToInsertIntoReminder);
                            }
                        }
                    }
                }
            }
            
            
            if (isset($arrTimeSlot['CALL']) && !empty($arrTimeSlot['CALL'])) {
                foreach ($arrTimeSlot['CALL'] AS $key => $reminderSlotIdForAppointmentCALL) {

                    $reminderSlotIdForAppointmentCALL = explode(',', $reminderSlotIdForAppointmentCALL);
                    if (!empty($arrSlotsAllInformation)) {
                        foreach ($arrSlotsAllInformation AS $allSlotsKey => $arrAllSlotsInfoCALL) {
                            if (in_array( $arrAllSlotsInfoCALL['recid'] ,$reminderSlotIdForAppointmentCALL )) {

                                $arrParamsToInsertIntoReminder = array();
                                $arrParamsToInsertIntoReminder['reminderId'] = $reminderId;
                                $arrParamsToInsertIntoReminder['value'] = $arrAllSlotsInfoCALL['value'];
                                $arrParamsToInsertIntoReminder['unit'] = $arrAllSlotsInfoCALL['unit'];
                                $arrParamsToInsertIntoReminder['typeX'] = 'CALL';
                                $arrParamsToInsertIntoReminder['arrToPassParams'] = $arrToPassParams;

                                $reminder = ReminderTiming::insertIntoReminderTimig($arrParamsToInsertIntoReminder);
                                ReminderTiming::recallSettings($reminder);
                            }
                        }
                    }
                }
            }



            if (isset($arrTimeSlot['EMAIL']) && !empty($arrTimeSlot['EMAIL'])) {
                foreach ($arrTimeSlot['EMAIL'] AS $key => $reminderSlotIdForAppointmentEMAIL) {

                    $reminderSlotIdForAppointmentEMAIL = explode(',', $reminderSlotIdForAppointmentEMAIL);
                    if (!empty($arrSlotsAllInformation)) {
                        foreach ($arrSlotsAllInformation AS $allSlotsKey => $arrAllSlotsInfoEMAIL) {
                            if (in_array( $arrAllSlotsInfoEMAIL['recid'] ,$reminderSlotIdForAppointmentEMAIL )) {

                                $arrParamsToInsertIntoReminder = array();
                                $arrParamsToInsertIntoReminder['reminderId'] = $reminderId;
                                $arrParamsToInsertIntoReminder['value'] = $arrAllSlotsInfoEMAIL['value'];
                                $arrParamsToInsertIntoReminder['unit'] = $arrAllSlotsInfoEMAIL['unit'];
                                $arrParamsToInsertIntoReminder['typeX'] = 'EMAIL';
                                $arrParamsToInsertIntoReminder['arrToPassParams'] = $arrToPassParams;

                                ReminderTiming::insertIntoReminderTimig($arrParamsToInsertIntoReminder);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    public static function insertIntoReminderTimig($arrParams) {

        $arrToPassParams = $arrParams['arrToPassParams'];

        $reminderSlot = new ReminderTiming();

        $reminderSlot->reminderid = $arrParams['reminderId'];
        $reminderSlot->value = $arrParams['value'];
        $reminderSlot->unit = $arrParams['unit'];
        $reminderSlot->typex = $arrParams['typeX'];

        $reminderSlot->save();
        $reminderSlotId = $reminderSlot->recid;

        $arrToPassParams['reminderSlotId'] = $reminderSlotId;
        $arrToPassParams['typeX'] = $arrParams['typeX'];
        $arrToPassParams['unit'] = $arrParams['unit'];
        $arrToPassParams['value'] = $arrParams['value'];
        $recid = ReminderSync::insertIntoReminderSyncTableForReminder($arrToPassParams);
        return $recid;
    }

    public static function deleteReminderTimingForReminder($reminderId) {
        $deletedRows = ReminderTiming::where('reminderid', $reminderId)->delete();
        return $deletedRows;
    }

    public static function deleteReminderTimingsForReminder($arrReminderIds) {
        if (!empty($arrReminderIds)) {
            ReminderTiming::whereIn('reminderid', $arrReminderIds)->update(['isdeleted' => '1']);
        }
        return true;
    }

    public static function recallSettings($recid) {

        $arrAdminSettings = AdminSetting::getInfo();
        if ($arrAdminSettings->flagrecall == 1) {
            for ($i = 1; $i <= $arrAdminSettings->recalltimes; $i++) {
                $reminderid = ReminderSync::find($recid);
                $recallrecords = $reminderid->replicate();
                $reminderrecalltime = $i * $arrAdminSettings->recallinterval;
                $recallrecords->flagrecall = 'Y';
                $recallrecords->recalltime = $arrAdminSettings->recalltimes;
                $recallrecords->recallinterval = $arrAdminSettings->recallinterval;
                $recallrecords->reminderrecallid = $recid;
                $recallrecords->recallcalltime = $i * $arrAdminSettings->recallinterval;
                $recallrecords->remindereventdatetime = Carbon::parse($recallrecords->remindereventdatetime)->addMinutes($reminderrecalltime);
                $recallrecords->save();
            }
        }
    }

}
