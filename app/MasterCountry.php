<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterCountry extends Model
{
        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table='master_countries';
    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';
      /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }
    
    public static function getcountry(){
   
        $mastercountry= MasterCountry::active()->orderBy('countrycode')->pluck('countrycode', 'rec_id')->toArray();
     
        return $mastercountry;
    }
    
}
