<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use App\Http\Controllers\SendRequestToServer;

class Addressbook extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'address_book';
    protected $primaryKey = 'recid';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

}
