<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterReminderResponseColor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'master_reminder_response_color';

    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public $defaultResponseColor = "";


    // public function __construct()
    // {
        
    // }

public function scopeActive($query)
    {
        return $query->where('isactive', 'A')->where('isdeleted', 0);
    }
    
    public static function getResponseColors() {
        return MasterReminderResponseColor::where('typex', 'REMINDER')->where('isdeleted', '0')->orderBy('twilioresponse', 'asc')->get();
    }

    public function getDefaultResponseColors() {
        
        $defaultColor = "#FFFFFF";
        if($this->defaultResponseColor == "") {
            $result = MasterReminderResponseColor::where('typex', 'REMINDER')->where('isdeleted', '0')->where('recid', '6')->get();

            if(!empty($result)) {
                $objDefaultColorInfo = $result[0];
                $this->defaultResponseColor = $objDefaultColorInfo->color;
                $defaultColor = $objDefaultColorInfo->color;
            }
        }
        else {
            $defaultColor = $this->defaultResponseColor;
        }
        return $defaultColor;
    }

    public static function getTwilioLogColor() {
        return MasterReminderResponseColor::where('typex', 'LOG')->where('isdeleted', '0')->orderBy('twilioresponse', 'asc')->get();
    }


    public static function updateColours($arrParams) {
        $colorCode = $arrParams['colorCode'];
        $colorId = $arrParams['colorId'];

        $twilioResponseColor = MasterReminderResponseColor::find($colorId);
        $twilioResponseColor->color = $colorCode;
        $twilioResponseColor->save();

        return true;
    }
    
    
    public static function getTwilioColor() {
        return MasterReminderResponseColor::where('typex', 'LOG')->active()->pluck( 'color','statusvalue')->toArray();
    }

}
