<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalRoomLink extends Model
{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medical_room_link';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';
}
