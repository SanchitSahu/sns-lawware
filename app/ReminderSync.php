<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReminderSync extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reminder_sync';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function scopeActive($query) {
        return $query->where('reminder_sync.isactive', 'A')->where('reminder_sync.isdeleted', 0);
    }

    public function scopeNonDeleted($query) {
        return $query->where('reminder_sync.isdeleted', 0);
    }

    public function scopeNotSyncedReminders($query) {
        return $query->where('flagscyncserver', 'N')
                        ->where('isdeleted', 0)
                        ->where('isactive', 'A');
    }

    public static function insertIntoReminderSyncTableForReminder($arrParams) {
//echo "<pre>";print_r($arrParams);
        $typeX = $arrParams['typeX'];
        $arrParamsForTemplateBodyReplace = array();
        $arrParamsForTemplateBodyReplace['strTemplateBody'] = $arrParams['bodyemail'];
        $arrParamsForTemplateBodyReplace['reminderId'] = $arrParams['reminderId'];
        $arrParamsForTemplateBodyReplace['typeX'] = $typeX;

        $strTemplateBody = ReminderSync::replaceInsertFieldsFromTemplateBody($arrParamsForTemplateBodyReplace);

        $arrAdminSettings = AdminSetting::getInfo();

        $arrParamsToGetReminderEventDateTime = array();
        $arrParamsToGetReminderEventDateTime['unit'] = $arrParams['unit'];
        $arrParamsToGetReminderEventDateTime['value'] = $arrParams['value'];
        $arrParamsToGetReminderEventDateTime['referenceDate'] = $arrParams['reminderdatetime'];

        $strReminderEventDateTime = ReminderSync::getDateTimeValueWithRespectToSlotDiff($arrParamsToGetReminderEventDateTime);

        if (!isset($arrParams['isreschedule']) || empty($arrParams['isreschedule'])) {
            $arrParams['isreschedule'] = 'N';
        }
        
        $eventDateTime = Carbon::parse($arrParams['reminderdatetime'])->subMinutes(2)->format('Y-m-d H:i');
        $currerntTime = Carbon::now()->subMinutes(2)->format('Y-m-d H:i:s');
        $flagSync = 'N';
        if($currerntTime > $strReminderEventDateTime) {
            $flagSync = 'Y';
        }

        if ($arrParams['typeX'] == 'CALL' && $arrAdminSettings->flagrecall == 1) {
            $flagrecall = 'Y';
        } else {
            $flagrecall = 'N';
        }
        $reminderSync = new ReminderSync();
        $reminderSync->snsusersid = $arrParams['snsusersid'];
        $reminderSync->clientkey = $arrAdminSettings->clientkey;
        $reminderSync->reminderid = $arrParams['reminderId'];
        $reminderSync->remindertimingid = $arrParams['reminderSlotId'];
        $reminderSync->languageid = $arrParams['languageid'];
        $reminderSync->title = $arrParams['title'];
        $reminderSync->purpose = $arrParams['purpose'];
        $reminderSync->bodyemail = $strTemplateBody;
        $reminderSync->bodysms = $strTemplateBody;
        $reminderSync->userphone = $arrParams['phoneno'];
        $reminderSync->useremail = $arrParams['email'];
        $reminderSync->remindereventdatetime = $strReminderEventDateTime;
        $reminderSync->appointmentdatetime = $arrParams['reminderdatetime'];
        $reminderSync->remindertype = $arrParams['typeX'];
//        $reminderSync->flagscyncserver = "N";
        $reminderSync->flagscyncserver = $flagSync;
        $reminderSync->twiliostatus = "";
        $reminderSync->flagrecall = $flagrecall;
        $reminderSync->flagdisablecancelreschedule = $arrParams['isreschedule'];
        $reminderSync->flagcallback = $arrParams['iscallback'];
        $reminderSync->flagdisable = $arrParams['isdisable'];
        $reminderSync->flagpromotional = $arrParams['flagpromotional'];
        $reminderSync->save();

        return $reminderSync->recid;
    }

    public static function replaceInsertFieldsFromTemplateBody($arrParams) {
        $strTemplateBody = "";

        if (isset($arrParams['strTemplateBody']) && $arrParams['strTemplateBody'] != "" &&
                isset($arrParams['reminderId']) && is_numeric($arrParams['reminderId']) && $arrParams['reminderId'] > 0) {

            $typeX = $arrParams['typeX'];

            $strTemplateBody = $arrParams['strTemplateBody'];
            $arrAllInsertFields = MasterTemplateInsertFields::getAllInsertFields();

            $arrAdminSettings = AdminSetting::getInfo();

            $arrReminderDetail = Reminder::getPerticularReminderDetail(array('reminderId' => $arrParams['reminderId']));
            
            if (!empty($arrAllInsertFields)) {
                foreach ($arrAllInsertFields AS $insertFieldKey => $arrInsertFieldInfo) {
                    $insertFieldValue = $arrInsertFieldInfo['name'];
                    $valueToBeReplaceWith = '{' . $insertFieldValue . '}';
                    $valueToReplace = "";
                    switch ($insertFieldValue) {
                        case "company name":
                        case "companyname":
                            $valueToReplace = $arrAdminSettings->companyname ?? "";
                            break;
                        case "eventdatetime":
                            if (!empty($arrReminderDetail)) {
//                                $valueToReplace = $arrReminderDetail['reminderdatetime'];
                                $valueToReplace = Carbon::parse($arrReminderDetail['reminderdatetime'])->format("F d, Y h:i A");
                            }
                            break;
                        case "eventusername":
                            if (!empty($arrReminderDetail)) {
                                $valueToReplace = $arrReminderDetail['firstname'] . " " . $arrReminderDetail['lastname'];
                            }
                            break;
                        case "company logo":
                            if ($typeX != "SMS") {
                                // $valueToReplace = $arrAdminSettings->companylogo;
//                                $valueToReplace = '<div style="display: inline-block;"><img src="' . $arrAdminSettings->companylogoFullPath . '" style="width:250px; height:auto;" /></div>';
                                $valueToReplace = '';
                            }
                            break;
                        case "company signature":
                            $valueToReplace = $arrAdminSettings->emailsignature;
                            break;
                        case "location":
//                             $masterTemplates = MasterTemplates::where(['recid' => $arrReminderDetail['templateid']])->select(['purpose'])->active()->first();
// //                            $valueToReplace = $arrAdminSettings->emailsignature;
//                             $valueToReplace = $masterTemplates->purpose ?? "";
                            if($arrReminderDetail['location'] != ''){
                                $valueToReplace = $arrReminderDetail['location'];
                            }else{
                                $valueToReplace = 'location';
                            }
                            break;
                        case "doctor":
                            $masterTemplates = DB::select("select * from calaudit1 where srno = ".$arrReminderDetail['srno']);
                            if($arrReminderDetail['doctor'] != ''){
                                $valueToReplace = $arrReminderDetail['doctor'];
                            }else{
                                $valueToReplace = 'doctor';
                            }
                            break;
                    }
//                    $strTemplateBody = str_replace($valueToBeReplaceWith, $valueToReplace, $strTemplateBody);
                    $strTemplateBody = str_ireplace($valueToBeReplaceWith, $valueToReplace, $strTemplateBody);
                }
            }
        }
        return $strTemplateBody;
    }

    public static function getDateTimeValueWithRespectToSlotDiff($arrParams) {
        $unit = $arrParams['unit'];
        $value = $arrParams['value'];
        $referenceDate = $arrParams['referenceDate'];

        $objCarbonReferenceDate = Carbon::parse($referenceDate);

        switch ($unit) {
            case "Immed":
            case "Immediate":

                $currentDate = Carbon::now();
                $currentDate->addMinutes(2);
                return $currentDate->format('Y-m-d H:i:s');

                break;
            case "Day":
            case "Days":
                return $objCarbonReferenceDate->subDays($value)->format('Y-m-d H:i:s');
                break;
            case "Minut":
            case "Minute":
            case "Minutes":
                return $objCarbonReferenceDate->subMinutes($value)->format('Y-m-d H:i:s');
                break;
            case "Hours":
            case "Hour":
                return $objCarbonReferenceDate->subHours($value)->format('Y-m-d H:i:s');
                break;
            default:
                return $objCarbonReferenceDate->format('Y-m-d H:i:s');
                break;
        }
    }

    public static function deleteReminderSyncForReminder($reminderId) {
        $arrAdminInfo = AdminSetting::getInfo();
        $clientKey = $arrAdminInfo->clientkey;
        $deletedRows = ReminderSync::where('reminderid', $reminderId)->where('clientkey', $clientKey)->delete();
        return $deletedRows;
    }

    public static function deleteReminderSyncForReminders($arrReminderIds) {
        if (!empty($arrReminderIds)) {
            ReminderSync::whereIn('reminderid', $arrReminderIds)->update(['isdeleted' => '1']);
        }
        return true;
    }

    public static function getRemindersToUpdateOnServer() {

        $arrResultNotSynced = ReminderSync::notSyncedReminders()->limit(100)->get()->toArray();

        $arrResultNeedToUpdate = Reminder::needToUpdateOnCloudServer()->limit(100)->get()->toArray();

        $arrResultNeedToDelte = Reminder::needToDeleteOnCloudServer()->limit(100)->get()->toArray();


        $arrReturn = array();
        $arrReturn['arrResultNotSynced'] = $arrResultNotSynced;
        $arrReturn['arrResultNeedToUpdate'] = $arrResultNeedToUpdate;
        $arrReturn['arrResultNeedToDelte'] = $arrResultNeedToDelte;


        return $arrReturn;
    }

    public static function updateRemindersFlagServerSync($arrReminderIds) {

        if (!empty($arrReminderIds)) {
            ReminderSync::whereIn('recid', $arrReminderIds)->update(['flagscyncserver' => 'Y']);
        }
        return true;
    }

}
