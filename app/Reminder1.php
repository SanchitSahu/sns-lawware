<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\snsUsers;
use App\reminderResponseColor;
use App\AdminSetting;
use Auth;

class Reminder extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reminder';
    protected $primaryKey = 'recid';

    const CREATED_AT = 'createddate';
    const UPDATED_AT = 'updateddate';

    public function snsUser() {
        return $this->hasOne('App\snsUsers', 'recid', 'snsusersid');
    }

    public function twilioStatusInfo() {
        return $this->hasOne('App\reminderResponseColor', 'twilioresponse', 'twiliostatus')->where('typex', 'REMINDER');
    }

    public function scopeActive($query) {
        return $query->where('reminder.isactive', 'A')->where('reminder.isdeleted', 0);
    }

    public function scopeNonDeleted($query) {
        return $query->where('reminder.isdeleted', 0);
    }

    public function scopeNeedToUpdateOnCloudServer($query) {
        return $query->where('flagneedtosyncwithserveragain', 'Y')
                        ->where('isdeleted', '0');
    }

    public function scopeNeedToDeleteOnCloudServer($query) {
        return $query->where('flagneedtosyncwithserveragain', 'Y')
                        ->where('isdeleted', '1');
    }

    public static function addInReminder($arrParams) {
        $reminder = new Reminder;
        //echo '<pre>';print_r(Auth::all());exit;
        $reminder->snsusersid = $arrParams['snsusersid'];
        $reminder->cardcode = $arrParams['cardcode'];
        $reminder->caseno = $arrParams['caseno'];
        $reminder->contacttype = $arrParams['contacttype'];
        $reminder->srno = isset($arrParams['srno']) ? $arrParams['srno'] : 0;//added by nilay
        $reminder->salutation = isset($arrParams['salutation']) ? $arrParams['salutation'] : '';
        $reminder->firstname = $arrParams['firstname'];
        $reminder->lastname = $arrParams['lastname'];
        $reminder->fullname = $arrParams['fullname'];
        $reminder->reminderdatetime = $arrParams['reminderdatetime'];
        $reminder->title = $arrParams['title'];
        $reminder->purpose = $arrParams['purpose'];
        $reminder->templateid = $arrParams['templateid'];
        $reminder->email = $arrParams['email'];
        $reminder->phoneno = $arrParams['phoneno'];
        $reminder->bodyemail = $arrParams['bodyemail'];
        $reminder->bodysms = $arrParams['bodyemail'];
        $reminder->languageid = $arrParams['languageid'];
        $reminder->flagsms = $arrParams['flagsms'];
        $reminder->smstimingid = $arrParams['smstimingid'];
        $reminder->flagcall = $arrParams['flagcall'];
        $reminder->calltimingid = $arrParams['calltimingid'];
        $reminder->flagemail = $arrParams['flagemail'];
        $reminder->emailtimingid = $arrParams['emailtimingid'];
        $reminder->isreschedule = $arrParams['isreschedule'];
        $reminder->iscallback = $arrParams['iscallback'];
        $reminder->isdisable = $arrParams['isdisable'];
        $reminder->isprivate = $arrParams['isprivate'];
        $reminder->flagpromotional = $arrParams['flagpromotional'];
        // $reminder->flagsyncbacktoa1law = $arrParams['flagsyncbacktoa1law'] ?? 0;
        $reminder->calender_val = isset($arrParams['calender_val']) ? $arrParams['calender_val'] : "1";
        $reminder->save();

        $reminderId = $reminder->recid;
        $arrParams['reminderId'] = $reminderId;

        $arrReminderTimingInsertData = array();
        $arrReminderTimingInsertData['reminderId'] = $reminderId;
        $arrReminderTimingInsertData['arrTimeSlot'] = $arrParams['arrTimeSlot'];
        $arrReminderTimingInsertData['arrToPassParams'] = $arrParams;

        ReminderTiming::insertReminderTimingForReminder($arrReminderTimingInsertData);
    }

    public static function updateInReminder($arrParams) {

        $reminder = Reminder::find($arrParams['reminderId']);

        $reminder->snsusersid = Auth::id();
        $reminder->cardcode = $arrParams['cardcode'];
        $reminder->caseno = $arrParams['caseno'];
        $reminder->contacttype = $arrParams['contacttype'];
        $reminder->srno = $reminder->srno ? $reminder->srno : 0;//added by nilay
        $reminder->salutation = $arrParams['salutation'];
        $reminder->firstname = $arrParams['firstname'];
        $reminder->lastname = $arrParams['lastname'];
        $reminder->fullname = $arrParams['fullname'];
        $reminder->reminderdatetime = $arrParams['reminderdatetime'];
        $reminder->title = $arrParams['title'];
        $reminder->purpose = $arrParams['purpose'];
        $reminder->templateid = $arrParams['templateid'];
        $reminder->email = $arrParams['email'];
        $reminder->phoneno = $arrParams['phoneno'];
        $reminder->bodyemail = $arrParams['bodyemail'];
        $reminder->bodysms = $arrParams['bodyemail'];
        $reminder->languageid = $arrParams['languageid'];
        $reminder->flagsms = $arrParams['flagsms'];
        $reminder->smstimingid = $arrParams['smstimingid'];
        $reminder->flagcall = $arrParams['flagcall'];
        $reminder->calltimingid = $arrParams['calltimingid'];
        $reminder->flagemail = $arrParams['flagemail'];
        $reminder->emailtimingid = $arrParams['emailtimingid'];
        $reminder->isreschedule = $arrParams['isreschedule'];
        $reminder->iscallback = $arrParams['iscallback'];
        $reminder->isdisable = $arrParams['isdisable'];
        $reminder->isprivate = $arrParams['isprivate'];
        $reminder->flagneedtosyncwithserveragain = 'Y';
        // $reminder->flagsyncbacktoa1law = $arrParams['flagsyncbacktoa1law'] ?? 0;
        $reminder->calender_val = isset($arrParams['calender_val']) ? $arrParams['calender_val'] : "1";
        $reminder->twiliostatus = null;

        $reminder->save();

        $arrReminderTimingInsertData = array();
        $arrReminderTimingInsertData['reminderId'] = $arrParams['reminderId'];
        $arrReminderTimingInsertData['arrTimeSlot'] = $arrParams['arrTimeSlot'];
        $arrReminderTimingInsertData['arrToPassParams'] = $arrParams;

        ReminderTiming::deleteReminderTimingForReminder($arrParams['reminderId']);
        ReminderSync::deleteReminderSyncForReminder($arrParams['reminderId']);

        ReminderTiming::insertReminderTimingForReminder($arrReminderTimingInsertData);
    }

    public function setBodyEmailAttribute($value) {
        $this->attributes['bodyemail'] = addslashes($value);
    }

    public static function getPerticularReminderDetail($arrParams) {
        $arrReturn = array();

        if (isset($arrParams['reminderId']) && is_numeric($arrParams['reminderId']) && $arrParams['reminderId'] > 0) {
            $arrReturn = Reminder::find($arrParams['reminderId'])->toArray();
        }

        return $arrReturn;
    }

    public static function getReminderDetails($reminderId) {

        if (!is_numeric($reminderId) || $reminderId <= 0) {
            return array();
        }

        $arrReminderInfo = Reminder::find($reminderId)->toArray();

        $reminderDateTime = "";
        $reminderDate = "";
        $reminderTime = "";
        $fullName = "";
        if (!empty($arrReminderInfo) && isset($arrReminderInfo['reminderdatetime'])) {
            $reminderDateTime = $arrReminderInfo['reminderdatetime'];
            $arrReminderDateTimeComponent = explode(' ', $reminderDateTime);

            if (isset($arrReminderDateTimeComponent[0])) {
                $reminderDate = Carbon::parse($arrReminderDateTimeComponent[0])->format(config('app.APP_DEFAULT_VIEW_DATE_FORMAT'));
            }

            if (isset($arrReminderDateTimeComponent[1])) {
                $reminderTime = Carbon::parse($arrReminderDateTimeComponent[1])->format('H:i');
            }
        }

        $arrReminderInfo['reminderDate'] = $reminderDate;
        $arrReminderInfo['reminderTime'] = $reminderTime;
        $arrReminderInfo['fullName'] = $arrReminderInfo['fullname'];

        return $arrReminderInfo;
    }

    public static function getReminderDetailsForDashboard($arrParams) {
        $dateRangeFilter = "";
        if (isset($arrParams['dateRangeFilter'])) {
            $dateRangeFilter = $arrParams['dateRangeFilter'];
        }


        $startDate = "";
        if (isset($arrParams['startDate'])) {
            $startDate = $arrParams['startDate'];
        }

        $endDate = "";
        if (isset($arrParams['endDate'])) {
            $endDate = $arrParams['endDate'];
        }
        $isPrivate = "";
        if (isset($arrParams['isPrivate']) && $arrParams['isPrivate'] == 'true') {
            $isPrivate = 'Y';
        }

        $searchText = "";
        if (isset($arrParams['searchText'])) {
            $searchText = $arrParams['searchText'];
        }

        $arrAdminSettings = AdminSetting::getInfo();

        $arrReminders = Reminder::select(['reminder.recid',
                    \DB::raw('IF(' . $arrAdminSettings->systemtimezone . '!= 24,(DATE_FORMAT(sns_reminder.confirmdate,"%m/%d/%Y %h:%i %p")),(DATE_FORMAT(sns_reminder.confirmdate,"%m/%d/%Y %H:%i"))) as confirmdate'),
                    'reminder.caseno', \DB::raw('IF(' . $arrAdminSettings->systemtimezone . '!= 24,(DATE_FORMAT(sns_reminder.reminderdatetime,"%m/%d/%Y %h:%i %p")),(DATE_FORMAT(sns_reminder.reminderdatetime,"%m/%d/%Y %H:%i"))) as reminderdatetime'),
                    'reminder.purpose', 'reminder.fullname', 'reminder.isdone',
                    'master_reminder_response_color.color', 'users.firstname','users.snsusersunqid', 'reminder.twiliostatus', \DB::raw("(SELECT para_hand FROM snsweb.case WHERE `caseno` = `sns_reminder`.`caseno`) as para_hand")])
                ->leftJoin('master_reminder_response_color', function($join) {
                    $join->on('reminder.twiliostatus', '=', 'master_reminder_response_color.twilioresponse');
                    $join->where('master_reminder_response_color.typex', '=', 'REMINDER');
                })
                ->leftJoin('users', 'users.recid', '=', 'reminder.snsusersid');
//                        ->leftJoin('case', 'case.caseno', '=', 'sns_remidner.caseno');
//                        ->leftJoin(\DB::raw("SELECT ")'case', 'case.caseno', '=', 'sns_remidner.caseno');
        //fetch is private records 
        if (Auth::user()->role == 'users') {
            $arrReminders->where(function ($query) {
                $query->where('reminder.isprivate', 'N');
                $query->orwhere(function ($query) {
                    $query->where('reminder.isprivate', 'Y')
                            ->where('reminder.snsusersid', Auth::id());
                });
            });
        }
        if (!empty($isPrivate)) {
            $arrReminders->where('reminder.isPrivate', $isPrivate);
        }
//        if(!empty($searchText)) {
//            $arrReminders->where(function($query) use($searchText){
//                $query->where('users.firstname', 'like', '%'.$searchText.'%')
////                        ->orwhere('reminders.para_hand', 'like', '%'.$searchText.'%')
//                        ->orwhere('reminder.purpose', 'like', '%'.$searchText.'%')
//                        ->orwhere('reminder.fullname', 'like', '%'.$searchText.'%')
//                        ->orwhere('reminder.caseno', 'like', '%'.$searchText.'%')
//                        ->orwhere('reminder.confirmdate', 'like', '%'.$searchText.'%')
//                        ->orwhere('reminder.reminderdatetime', 'like', '%'.$searchText.'%');
//            });
////            $arrReminders->where('users.firstname', 'like', '%'.$searchText.'%');
//        }
        $arrReminders->where('reminder.flagpromotional', 0);
        $currentDateTime = Carbon::now()->format('Y-m-d H:i:s');

        switch ($dateRangeFilter) {
            case "UP":
                //$arrReminders->where('reminder.reminderdatetime', '>', $currentDateTime);
                $currentDate = Carbon::parse($currentDateTime)->format('Y-m-d H:i:s');
                $startDate = $currentDate;
                $endDate = Carbon::parse($currentDateTime)->addDays(7)->format('Y-m-d') . ' 23:59:59';
                $arrReminders->where('reminder.reminderdatetime', '>=', $startDate);
                $arrReminders->where('reminder.reminderdatetime', '<=', $endDate);
                break;
            case "TD":
                $currentDate = Carbon::parse($currentDateTime)->format('Y-m-d');
                $startDate = $currentDate . ' 00:00:00';
                $endDate = $currentDate . ' 23:59:59';
                $arrReminders->where('reminder.reminderdatetime', '>=', $startDate);
                $arrReminders->where('reminder.reminderdatetime', '<=', $endDate);
                break;
            case "TO":
                $nextDayDate = Carbon::parse($currentDateTime)->addDay()->format('Y-m-d');
                $startDate = $nextDayDate . ' 00:00:00';
                $endDate = $nextDayDate . ' 23:59:59';
                $arrReminders->where('reminder.reminderdatetime', '>=', $startDate);
                $arrReminders->where('reminder.reminderdatetime', '<=', $endDate);
                break;
            case "PW":
                $lastDayDateTime = Carbon::parse($currentDateTime)->subDay()->format('Y-m-d H:i:s');
                $firstDayOfWeekFromLastDay = Carbon::parse($lastDayDateTime)->subWeek()->format('Y-m-d H:i:s');


                $firstDay = Carbon::parse($firstDayOfWeekFromLastDay)->format('Y-m-d');
                $lastDay = Carbon::parse($lastDayDateTime)->format('Y-m-d');


                $startDate = $firstDay . ' 00:00:00';
                $endDate = $lastDay . ' 23:59:59';

                $arrReminders->where('reminder.reminderdatetime', '>=', $startDate);
                $arrReminders->where('reminder.reminderdatetime', '<=', $endDate);
                break;
            case "SD":
                if (!empty($startDate)) {
                    $startDateTime = $startDate . ' 00:00:00';
                    $currentDate = Carbon::parse($startDateTime)->format('Y-m-d');
                    $startDate = $currentDate . ' 00:00:00';
                    $endDate = $currentDate . ' 23:59:59';
                    $arrReminders->where('reminder.reminderdatetime', '>=', $startDate);
                    $arrReminders->where('reminder.reminderdatetime', '<=', $endDate);
                }
                break;
            case "CR":
                if (!empty($startDate) && !empty($endDate)) {

                    $startDateFormated = Carbon::parse($startDate)->format('Y-m-d');
                    $endDateFormated = Carbon::parse($endDate)->format('Y-m-d');

                    $startDateTime = $startDateFormated . ' 00:00:00';
                    $endDateTime = $endDateFormated . ' 23:59:59';

                    $arrReminders->where('reminder.reminderdatetime', '>=', $startDateTime);
                    $arrReminders->where('reminder.reminderdatetime', '<=', $endDateTime);
                }
                break;
            default :
                break;
        }

        $arrReminders->nonDeleted();
//\DB::enableQueryLog();
        $result = $arrReminders->get();
//$db = \DB::getQueryLog();
//echo "<pre>";print_r($db);exit;
        return $result;
    }

    public static function deleteReminders($arrReminderIds) {

        if (!empty($arrReminderIds)) {
            Reminder::whereIn('recid', $arrReminderIds)->update(['flagneedtosyncwithserveragain' => 'Y', 'isdeleted' => 1]);

            ReminderTiming::deleteReminderTimingsForReminder($arrReminderIds);

            ReminderSync::deleteReminderSyncForReminders($arrReminderIds);
        }
        return true;
    }

    public static function updateTwilioStatusFromTheServer($arrParams) {
        $reminderId = 0;
        $twilioStatus = 0;
print_r($arrParams);
        if (!isset($arrParams['reminderId']) || !isset($arrParams['reminderId'])) {
            return false;
        }

        if (isset($arrParams['reminderId']) && is_numeric($arrParams['reminderId']) && $arrParams['reminderId'] > 0) {
            $reminderId = $arrParams['reminderId'];
        }

        if (isset($arrParams['twilioStatus']) && is_numeric($arrParams['twilioStatus']) && $arrParams['twilioStatus'] > 0) {
            $twilioStatus = $arrParams['twilioStatus'];
        }

        if (isset($arrParams['confirmdate'])) {
            $confirmDate = $arrParams['confirmdate'];
        }

        if ($reminderId > 0 && $twilioStatus > 0) {
            $objReminder = Reminder::find($reminderId);
            if (!empty($objReminder)) {


                $status_reminder = (isset($objReminder) && !empty($objReminder)) ? $objReminder->twiliostatus : "";

//                $updateStatus = 1;
//                $reminderSync = ReminderSync::where('reminderid', $reminderId)->get();
//                if(count($reminderSync) > 0) {
//                    foreach ($reminderSync as $row) {
//                        if ($row->twiliostatus != 5 && $row->twiliostatus != 0) {
//                            $updateStatus = 0;
//                        }
//                    }
//                }
                if ((empty($status_reminder) && $status_reminder != $twilioStatus) || ((!empty($status_reminder) && $status_reminder != $twilioStatus && $twilioStatus != 5))) {
                    echo $objReminder->twiliostatus = $twilioStatus;
                }

//                if($updateStatus == 1) {
//                    $objReminder->twiliostatus = $twilioStatus;

//                $objReminder->confirmdate = $confirmDate ?? "";
                    //echo "<pre>";print_r($objReminder);exit;
                    //$objReminder->save();

                    if ($twilioStatus != 5) {
                        $objReminder->confirmdate = $confirmDate ?? date('Y-m-d H:i:s');
                    }
                }

//                }
                //echo $status_reminder . ' ***** '. $twilioStatus . ' ***** '. $objReminder->contacttype;exit;
//                if ((env('APP_ENV') == 'local') && $status_reminder != $twilioStatus && $objReminder->contacttype == 'A1Law' && $twilioStatus != 5) {
                if ((env('APP_ENV') == 'local') && $status_reminder != $twilioStatus && $objReminder->contacttype == 'A1Law' && $twilioStatus != 5 && ($objReminder->isactive == 'A' && $objReminder->isdeleted == '0')) {
                //if ((env('APP_ENV') == 'local') && $objReminder->contacttype == 'A1Law' && $twilioStatus != 5 && ($objReminder->isactive == 'A' && $objReminder->isdeleted == '0')) {

                    //echo 555;exit;
                    self::generatePrefillerFile($objReminder, $twilioStatus);
//                    $event_txt = '';
//                    $file = 'c:/lawlocal/' . $objReminder->caseno . '.txt';
//                    $batfile = 'c:/lawlocal/' . 'reminderbat-' . $objReminder->caseno . '.bat';
//
//                    $contents = $objReminder->caseno . PHP_EOL;
//                    if ($twilioStatus == 1) {
//                        $event_txt = 'Confirmed by client';
//                    } elseif ($twilioStatus == 2) {
//                        $event_txt = 'Cancelled by client';
//                    } elseif ($twilioStatus == 3) {
//                        $event_txt = 'Rescheduled by client';
//                    } elseif ($twilioStatus == 4) {
//                        $event_txt = 'Request Callback by client';
//                    } elseif ($twilioStatus == 5) {
//                        $event_txt = 'No Action by client';
//                    }
//                    $contents .= trim($objReminder->title) . ' : ' . trim($event_txt);
//                    file_put_contents($file, $contents);
//                    $bat_cmd = "c:" . PHP_EOL;
//                    $bat_cmd .= "cd c:\lawlocal" . PHP_EOL;
//                    $bat_cmd .= "Outlk1 c:\lawlocal perfiller $file";
//                    file_put_contents($batfile, $bat_cmd);
//                    exec($batfile);
//                    unlink($batfile);
               // }
                //echo 666;exit;
                $objReminder->save();
            }
            return true;
        }
    }

    public static function generateCalenderVal($calenderVal) {
        $cal = explode(':', $calenderVal);
        return $cal;
    }

    public static function generatePrefillerFile($objReminder, $twilioStatus) {
        try {
            $categoryData = self::generateCalenderVal($objReminder->calender_val);
            // get the list of data from the reminder quwery 
            $userDetails = User::find($objReminder->snsusersid);

            $Caseno = $objReminder->caseno;
            $EventT = trim($objReminder->title);
//            $Category =  $objReminder->calender_val ?? 1;
            $Category = (isset($categoryData) && isset($categoryData[0])) ? $categoryData[0] : 1;
            $Color = $objReminder->color_val ?? 1;
            $Staff = $userDetails->snsusersunqid ?? '';

            $event_txt = '';
            $file = 'c:/lawlocal/' . $Caseno . '.txt';
            $batfile = 'c:/lawlocal/' . 'reminderbat-' . $Caseno . '.bat';

            $contents = $Caseno . PHP_EOL;
            if ($twilioStatus == 1) {
                $event_txt = 'Confirmed by client';
            } elseif ($twilioStatus == 2) {
                $event_txt = 'Cancelled by client';
            } elseif ($twilioStatus == 3) {
                $event_txt = 'Rescheduled by client';
            } elseif ($twilioStatus == 4) {
                $event_txt = 'Request Callback by client';
            } elseif ($twilioStatus == 5) {
                $event_txt = 'No Action by client';
            }

            $contents .= $EventT . ' : ' . trim($event_txt) . PHP_EOL;
            $contents .= $Category . PHP_EOL;
            $contents .= $Color . PHP_EOL;
            $contents .= $Staff . PHP_EOL;

            file_put_contents($file, $contents);
            $bat_cmd = "c:" . PHP_EOL;
            $bat_cmd .= "cd c:\lawlocal" . PHP_EOL;
            $bat_cmd .= "Outlk1 c:\lawlocal perfiller $file";
            file_put_contents($batfile, $bat_cmd);
            exec($batfile);
            //unlink($batfile);
            if (file_exists($batfile)) {
                unlink($batfile);
            }
        } catch (Exception $ex) {
            // prefiller command failt to load due to code error. save log to the failure log table.
        }
    }

    public static function updateAllTwilioStatusFromTheServer($arrParams) {
        $twilioStatus = 0;
        if ($arrParams['remindertype'] == 'CALL') {
            $objReminder = ReminderSync::where('reminderid', $arrParams['reminderid'])
                    ->where('remindertimingid', $arrParams['remindertimingid'])
                    ->where('recallcalltime', $arrParams['recallcalltime'])
                    ->first();
        } else {
            $objReminder = ReminderSync::where('reminderid', $arrParams['reminderid'])
                    ->where('remindertimingid', $arrParams['remindertimingid'])
                    ->first();
        }

        if (isset($arrParams['twilioStatus']) && is_numeric($arrParams['twilioStatus']) && $arrParams['twilioStatus'] > 0) {
            $twilioStatus = $arrParams['twilioStatus'];
        }

        if (!empty($objReminder)) {
            // condition to skip the update with no response on second time. 
            $status_reminder = (isset($objReminder) && !empty($objReminder)) ? $objReminder->twiliostatus : "";
            if ((empty($status_reminder) && $status_reminder != $twilioStatus) || ((!empty($status_reminder) && $status_reminder != $twilioStatus && $twilioStatus != 5))) {
                $objReminder->twiliostatus = $arrParams['twiliostatus'];
                $objReminder->save();
            }

            return true;
        }
    }

    public static function updateFlagForDoNotSyncWithServerAgain($arrReminderIds) {
        if (!empty($arrReminderIds)) {
            Reminder::whereIn('recid', $arrReminderIds)->update(['flagneedtosyncwithserveragain' => 'N']);
        }
    }

}
