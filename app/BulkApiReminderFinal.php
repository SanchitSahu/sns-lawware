<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkApiReminderFinal extends Model
{
       /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bulk_api_reminder_final';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'recid';

   

    protected $fillable = ['account', 'first_name', 'last_name', 'home', 'work', 'appointment',
        'room_no','phone_number','appt_date', 'appt_time', 'appt_doctor', 
        'appt_room_location','appt_dt_time','appt_room',
        'appt_room_class', 'created_at', 'updated_at'];
}
