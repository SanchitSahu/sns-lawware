<?php

return [
    'failurelog' => [
        'template_mismatch' => [
            'title' => 'Template Mismatch',
            'short_description' => '#TEMPLATE_NAME# - template does not match with SNS\'s template',
            'id' => 1
        ],
        'calaudit_to_reminder_exception' => [
            'title' => 'Exception calaudit1 to reminder',
            'short_description' => 'Exception in migrating event from calaudit1 to reminder table',
            'id' => 2
        ],
        'dataloader_fail' => [
            'title' => 'DataLoader fail',
            'id' => 3
        ],
    ],
    'contact_migrate' => [
        'phonetype' => [
            'mobile' => 0,
            'home' => 1,
            'office' => 2,
            'personal' => 3,
            'other' => 4,
        ],
        'emailtype' => [
            'home' => 0,
            'work' => 1,
            'other' => 2,
        ]
    ]
        ]
?>