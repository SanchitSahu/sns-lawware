<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSnsAdminSettingsOnoffsynctoa1law extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_settings', function (Blueprint $table) {
//            $table->tinyInteger('onoffparalegal')->after('synca1law')->default('0');
			$table->tinyInteger('onoffsynca1law')->after('onoffparalegal')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_settings', function (Blueprint $table) {
			// $table->dropColumn('facebook_verified');	// single column remove
//            $table->dropColumn(['onoffparalegal', 'onoffsynca1law']);	// multi colulm remove 
            $table->dropColumn(['onoffsynca1law']);	// multi colulm remove 
        });
    }
}
