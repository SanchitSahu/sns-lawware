<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSnsReminderFlagsynca1law extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reminder', function (Blueprint $table) {
            $table->tinyInteger('flagsyncbacktoa1law')->after('contacttype')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reminder', function (Blueprint $table) {
			// $table->dropColumn('facebook_verified');	// single column remove
            $table->dropColumn(['flagsyncbacktoa1law']);	// multi colulm remove 
        });
    }
}
