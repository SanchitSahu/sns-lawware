<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = DB::connection('oldsnsdb')->table('tbl_rs_contactlist')->select('*')->get();
        foreach ($data as $data) {
            $user = \App\Contact::firstOrCreate([
                 'phonenumber' => $data->PhoneNo,
                            ], [
                        'middlename' => $data->MName,
                        'lastname' => $data->LName,
                        'salutation' => $data->Salutation,
                        'firstname' => $data->FName,
                        'phonenumber' => $data->PhoneNo,
                        'phonetype' => '',
                        'case' => $data->Case,
                        'notes' => $data->Notes,
                        'phonetype' => $data->MobileType,
                        'emailtype' => $data->EmailType,
                        'firmcode' => $data->Firmcode,
                        'cardcode' => $data->CardCode,
                        'createddate' => date('Y-m-d H:i:s'),
                        'updateddate' => date('Y-m-d H:i:s'),
                        'isactive' => 'A',
                        'isdeleted' => 0,
            ]);
            $user->save();
        }
    }
}   
    