<?php

use Illuminate\Database\Seeder;

class MasterTemplateSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = DB::connection('oldsnsdb')->table('tbl_apptoto_addsmsemail_reminderapi')->select('*')->get();
        foreach ($data as $data) {
            if($data->TwilioLang=='6'){
                $lang= 7; 
            }elseif($data->TwilioLang=='9') { 
                $lang= 10; 
             }else { 
                  $lang= 7; 
             }
          if($data->IsReschedule=='0') {
            $reschedule= 'N';
          }elseif($data->IsReschedule=='1') {
               $reschedule= 'Y';
          }else{
               $reschedule= 'N';
          }
         
            $user = \App\MasterTemplates::Create([
                       'title' => $data->Name,
                        'purpose' => $data->Purpose,
                        'bodyemail' => $data->EmailSubject,
                'bodysms'=>$data->EmailSubject,
                        'language' =>   $lang,             
                        'smstimingid' => (!empty($data->whenSMS))?explode(',',$data->whenSMS):'',
                        'calltimingid' => (!empty($data->whenCALL))?explode(',',$data->whenCALL):'',
                        'emailtimingid' =>(!empty($data->whenEMAIL))?explode(',',$data->whenEMAIL):'',
                        'isreschedule' =>$reschedule,
                        'createddate' => date('Y-m-d H:i:s'),
                        'updateddate' => date('Y-m-d H:i:s'),
                        'isactive' =>'A',
                        'isdeleted' => 0,
            ]);
            $user->save();
        }
    }

}
