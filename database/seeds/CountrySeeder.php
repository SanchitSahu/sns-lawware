<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data=DB::connection('oldsnsdb')->table('lst_countrycode')->select('CountryName','CountryCode')->get();  
     foreach($data as $data){
        $user= \App\MasterCountry::firstOrCreate([     
        'name'          =>  $data->CountryName,
        'countrycode'   =>  $data->CountryCode,
         ],[
         'name'          =>  $data->CountryName,
        'countrycode'   =>  $data->CountryCode,
        'createddate'   =>  date('Y-m-d H:i:s'),
        'updateddate'   =>  date('Y-m-d H:i:s'),
        'isactive'   => 'A',
        'isdeleted'   => 0,
             ]);
         $user->save();
    }
    }
}
