<?php

use Illuminate\Database\Seeder;

class TwilioLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=DB::connection('oldsnsdb')->table('lst_twiliolanguage')->select('Language','LanguageValue')->get();  
         foreach($data as $data){
        $user= \App\TwilioLanguage::firstOrCreate([     
        'name'          =>  $data->Language,
        'code'   =>  $data->LanguageValue,
         ],[
         'name'          =>  $data->Language,
        'code'   =>  $data->LanguageValue,
        'createddate'   =>  date('Y-m-d H:i:s'),
        'updateddate'   =>  date('Y-m-d H:i:s'),
        'isactive'   => ($data->Language=='Spanish' || $data->Language=='English')?'A':'I',
        'isdeleted'   => 0,
             ]);
         $user->save();
            }
    }
}
